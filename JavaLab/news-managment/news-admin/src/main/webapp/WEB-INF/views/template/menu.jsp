
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="btn-group-vertical">

	<a href="/news-admin/news/list/1" class="btn btn-default"><button>
			<spring:message code="newsList" />
		</button></a> <a href="/news-admin/news/add" class="btn btn-default"><button>
			<spring:message code="addNews" />
		</button></a> <a href="/news-admin/author/list" class="btn btn-default"><button>
			<spring:message code="addUpdateAuthors" />
		</button></a> <a href="/news-admin/tag/list" class="btn btn-default"><button>
			<spring:message code="addUpdateTags" />
		</button></a>

</div>
