<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<div class="panel panel-heading">

	<h1>Mews portal</h1>
	<div class="row">

		<div class="languages">

			<a href="?language=ru"><button class=" btn btn-default"
					id="language" name="language" value="ru">Рус</button></a> <a
				href="?language=en"><button class=" btn btn-default"
					id="language" name="language" value="en">En</button></a>

		</div>

		<div class="logout">

			<sec:authorize access="isAuthenticated()">
				<sec:authentication property="principal.username" />
				<core:url var="logoutUrl" value="/logout" />
				<form action="${logoutUrl}" method="post">
					<button type="submit" class="btn btn-info">
						<spring:message code="logout" />
					</button>
					<input type="hidden" name="${_csrf.parameterName}"
						value="${_csrf.token}" />
				</form>
			</sec:authorize>

		</div>

	</div>




</div>