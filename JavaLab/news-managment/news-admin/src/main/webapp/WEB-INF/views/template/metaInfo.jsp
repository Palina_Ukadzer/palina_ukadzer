<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
<fmt:message key="badAuthor" var="badAuthor" />
<fmt:message key="badTitle" var="badTitle" />
<fmt:message key="badShort" var="badShort" />
<fmt:message key="badFull" var="badFull" />
<fmt:message key="badComment" var="badComment" />
<fmt:message key="badTag" var="badTag" />
</html>