<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="language"
	value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
	scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="message" />
<head>


<title>News portal</title>
<link href="<c:url value="/resources/myClasses.css" />" type="text/css"
	rel="stylesheet">

<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="<c:url value="/resources/jquery.sumoselect.min.js" />"></script>
<link href="<c:url value="/resources/sumoselect.css" />"
	rel="stylesheet" />
<meta charset="utf-8">
<script>
	$(document).ready(function() {

		$('.selectpicker').SumoSelect({
			placeholder : '<fmt:message key="selectHere" />',
			captionFormat : '<fmt:message key="selected" />',
			csvDispCount : 3
		});

	});
</script>

</head>
<body>

	<div class="panel panel-default">
		<!-- Header -->
		<tiles:insertAttribute name="header" />
		<!-- Menu Page -->


		<div class="menu"><%-- 
			<tiles:insertAttribute name="meta" /> --%>
			<tiles:insertAttribute name="menu" />
		</div>
	</div>
	<!-- Body Page -->
	<div class="mainPart">
		<tiles:insertAttribute name="body" />


		<!-- Footer Page -->

	</div>
	<tiles:insertAttribute name="footer" />
</body>
</html>