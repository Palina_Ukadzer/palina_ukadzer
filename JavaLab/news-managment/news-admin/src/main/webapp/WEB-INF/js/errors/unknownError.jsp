<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="language"
	value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
	scope="session" />
<fmt:setLocale value="${language}" />
<!DOCTYPE html>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Error</title>
<link rel="stylesheet"
	href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
<link rel="stylesheet"
	href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
</head>
<body>
	<div class="panel panel-heading">
		<h1>Mews portal</h1>
		<form>
			<button type="submit" id="language" name="language" value="ru">Ru</button>
			<button type="submit" id="language" name="language" value="en">En</button>
		</form>
	</div>
	<div class="panel panel-body">
		<div align="center">
			<h2>333
				<spring:message code="unknownError" />
			</h2>
		</div>
	</div>
</body>
</html>
