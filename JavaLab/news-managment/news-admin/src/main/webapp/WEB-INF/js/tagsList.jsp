
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<script>	
		badTag = '<fmt:message key="badTag"/>';
</script> 
<script src="<c:url value="/resources/validation.js" />"></script>
<link href="<c:url value="/resources/editUpdate.css" />" type="text/css"
	rel="stylesheet">
<body>
	<div class="panel panel-default">
		<div class="panel-body">
			<c:choose>
				<c:when test="${empty tagForEditId}">
					<c:forEach items="${tags}" var="tagCurrent">
						<form action="edit">
							<div class="col-sm-12">
								<div class=" wrapper"><c:out value="${tagCurrent.name}"/></div>
								<input type="hidden" name="id" value="${tagCurrent.id}">
								<button type="submit" class="editButton">
									<spring:message code="edit" />
								</button>
							</div>
						</form>
					</c:forEach>
				</c:when>
				<c:otherwise>
					<c:forEach items="${tags}" var="tagCurrent">
						<c:choose>
							<c:when test="${tagForEditId == tagCurrent.id}">
								<sf:form action="save" modelAttribute="tag"
									onsubmit="return validate_tag ();">
									<sf:input path="name" value="${tagCurrent.name}" id="tag"
										class="wrapper" />
									<sf:errors path="name" cssClass="error" />
									<sf:hidden path="id" value="${tagCurrent.id}" />
									<button type="submit" class="editButton">
										<spring:message code="edit" />
									</button>
									<a href="/news-admin/tag/delete/${tagForEditId}"><div class="buttonLike">
											<spring:message code="delete" />
										</div></a>
									<a href="/news-admin/tag/cancel"><div class="buttonLike">
											<spring:message code="cancel" />
										</div></a>
								</sf:form>
							</c:when>
							<c:otherwise>
								<form action="edit" method="post">
									<div class="col-sm-12">
										<div class="wrapper "><c:out value="${tagCurrent.name}"/></div>
										<input type="hidden" name="id" value="${tagCurrent.id}">
										<button type="submit" class="editButton">
											<spring:message code="edit" />
										</button>
									</div>
								</form>

							</c:otherwise>
						</c:choose>
					</c:forEach>
				</c:otherwise>
			</c:choose>
			<sf:form action="add" modelAttribute="newTag" onsubmit="return validate_tag ();">
				<div class="addField">
					<sf:input class="addWrapper" id="tag" path="name" value="${newTag.name}" />
					<sf:errors path="name" cssClass="error" />
					<button type="submit" class="editButton">
						<spring:message code="add" />
					</button>
				</div>
			</sf:form>
		</div>
	</div>
</body>
</html>