
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
<link href="<c:url value="/resources/editNews.css" />" type="text/css"
	rel="stylesheet">
 <script>	
		badTitle = '<fmt:message key="badTitle"/>';
		badShort = '<fmt:message key="badShort"/>';
		badFull = '<fmt:message key="badFull"/>';
</script> 
<script src="<c:url value="/resources/validation.js" />"></script>
<body>

<fmt:message key="badShort" var="badShort" />
<fmt:message key="badFull" var="badFull" />
	<div class="panel panel-default">
		<div class="panel-body">
			<sf:form modelAttribute="complexNews" action="save" method="post"
				onsubmit="return validate_news('${badShort}','${badFull}');">
			<!-- onsubmit="return validate_news ( );"> -->
				<c:if test="${not empty complexNews.news.id }">
					<sf:hidden path="news.id" value="${complexNews.news.id}" />
				</c:if>
				<div class="col-sm-12">
					<div class="col-sm-2">
						<spring:message code="title" />
						:
					</div>
					<div class="col-sm-5">
						<sf:textarea class="form-control" id="news_title" 
							path="news.title" value="${complexNews.news.title}" rows="1"
							cols="40" />
					</div>
					<div class="col-sm-5">
						<sf:errors path="news.title" cssClass="error" />
					</div>
				</div>
				<div>
					<fmt:formatDate value="${complexNews.news.creationDate}"
						pattern="yyyy-MM-dd" var="creationDate" />
					<div class="col-sm-12">
						<div class="col-sm-2">
							<spring:message code="date" />
							:
						</div>
						<div class="col-sm-5">
							<sf:input id="creation_date" type="date" class="form-control"
								path="news.creationDate" value="${creationDate}" />
						</div>
						<div class="col-sm-5">
							<sf:errors path="news.creationDate" cssClass="error" />
						</div>
					</div>

				</div>
				<div class="col-sm-12">
					<div class="col-sm-2">

						<spring:message code="brief" />
						:
					</div>
					<div class="col-sm-5">
						<sf:textarea class="form-control" id="news_short"
							path="news.shortText" value="${complexNews.news.shortText}"
							rows="2" cols="40" />
					</div>
					<div class="col-sm-5">
						<sf:errors path="news.shortText" cssClass="error" />
					</div>
				</div>

				<div class="col-sm-12">
					<div class="col-sm-2">

						<spring:message code="content" />
						:
					</div>

					<div class="col-sm-5">
						<sf:textarea class="form-control" id="news_full"
							path="news.fullText" value="${complexNews.news.fullText}"
							rows="4" cols="40" />
					</div>
					<div class="col-sm-5">
						<sf:errors path="news.fullText" cssClass="error" />
					</div>
				</div>


				<div class="col-sm-6">


					<div class="author">
						<sf:select class="selectpicker" path="author.id">
							<c:forEach items="${authors}" var="authorNew">
								<c:choose>
									<c:when test="${authorNew.id == authorId}">
										<sf:option selected="true" value="${authorNew.id}">${authorNew.name}</sf:option>
									</c:when>
									<c:otherwise>
										<sf:option value="${authorNew.id}">${authorNew.name}</sf:option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</sf:select>
					</div>
					<div class="tags">
						<select name="tagsIds" id="tagsIds" class="selectpicker" multiple>
							<c:choose>
								<c:when test="${empty searchedTagsMap}">
									<c:forEach items="${tags}" var="tag">
										<option value="${tag.id}">${tag.name}</option>
									</c:forEach>
								</c:when>
								<c:otherwise>
									<c:forEach items="${tags}" var="tag">
										<option value="${tag.id}"
											${not empty searchedTagsMap[tag.id] ? 'selected' : ''}>${tag.name}</option>
									</c:forEach>

								</c:otherwise>
							</c:choose>
						</select>
					</div>

				</div>
				<button type="submit">
					<spring:message code="save" />
				</button>
			</sf:form>
		</div>

	</div>
</body>
</html>