<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%-- <link href="<c:url value="/resources/error.css" />" type="text/css"
	rel="stylesheet"> --%>
<html>

<body>

	<div class="dotted-border">


		<div class="panel panel-body">

			<div class="backButton">
				<a href="news/back"><button type="submit" class="btn btn-primary">
						<spring:message code="back" />
					</button></a>
			</div>

			<div class="col-sm-12">
				<div class="col-sm-10"><spring:message code="pageNotFound" /></div>
			</div>
		</div>
	</div>
</body>
</html>


