package com.epam.newsmanagment.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.ukadzer.entity.SearchCriteria;

@Controller
@PropertySource("classpath:constants.properties")
public class HomeController {

	@Value("${login}")
	private String login;

	@Value("${notFound}")
	private String notFound;

	@Value("${unknownServiceError}")
	private String unknownServiceError;

	@Value("${unknownError}")
	private String unknownError;

	@RequestMapping({ "/", "/hi" })
	public String showHomePage(Model model) {
		return login;
	}

	@RequestMapping("/errorNotFound")
	public String notFoundErrorPage(Model model) {
		return notFound;
	}

	@RequestMapping({ "/errorWithAuthorsService", "/errorWithNewsService",
			"/errorWithTagsService", "/errorWithCommentsService", })
	public String unknownServiceErrorPage(Model model) {
		return notFound;
	}

	@RequestMapping({ "/errorWithAuthors", "/errorWithNews", "/errorWithTags",
			"/errorWithComments", })
	public String unknownErrorPage(Model model) {
		return notFound;
	}

	@RequestMapping("/login")
	public ModelAndView logIn(
			@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout) {

		ModelAndView model = new ModelAndView();
		model.addObject("searchCriteria", new SearchCriteria());
		if (error != null) {
			model.addObject("error", "Invalid username and password!");
		}
		if (logout != null) {
			model.addObject("msg", "You've been logged out successfully.");
		}
		model.setViewName(login);

		return model;

	}
}