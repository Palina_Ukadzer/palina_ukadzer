package com.epam.newsmanagment.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.epam.ukadzer.entity.Comment;
import com.epam.ukadzer.service.CommentService;
import com.epam.ukadzer.service.exception.ServiceException;

@Controller
@RequestMapping("/comment")
public class CommentController {

	@Autowired
	private CommentService commentService;

	@ExceptionHandler(ServiceException.class)
	public String handleServiceException() {
		return "redirect:/errorWithCommentsService";
	}

	@ExceptionHandler(Exception.class)
	public String handleException() {
		return "redirect:/errorWithComments";
	}

	@RequestMapping("/postComment")
	public String postComment(
			@Valid @ModelAttribute("comment") Comment comment,
			BindingResult bindingResult, RedirectAttributes attr, Model model)
			throws ServiceException {

		if (bindingResult.hasErrors()) {
			attr.addFlashAttribute(
					"org.springframework.validation.BindingResult.comment",
					bindingResult);
			attr.addFlashAttribute("comment", comment);
			return "redirect:/news/" + comment.getNewsId();
		}
		commentService.add(comment);

		return "redirect:/news/" + comment.getNewsId();
	}

	@RequestMapping("/delete")
	public String deleteComment(@RequestParam Long newsId,
			@RequestParam Long commentId) throws ServiceException {
		commentService.delete(commentId);
		return "redirect:/news/" + newsId;
	}
}
