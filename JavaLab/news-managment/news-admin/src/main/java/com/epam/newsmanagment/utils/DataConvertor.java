package com.epam.newsmanagment.utils;

import java.util.HashSet;
import java.util.Set;


import com.epam.ukadzer.entity.Tag;
import com.epam.ukadzer.service.TagService;
import com.epam.ukadzer.service.exception.ServiceException;

public class DataConvertor {
	
	public Set<Tag> stringArrayToTags(String[] strings, TagService tagService)
		 	throws ServiceException {
		Set<Tag> tags = new HashSet<Tag>();
	
			for (int i = 0; i < strings.length; i++) {
				Long long1 = Long.parseLong(strings[i]);
				tags.add(tagService.getById(long1));
			}
		
		return tags;
	}

}
