package com.epam.newsmanagment.controller.exception;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="No such news")
public class NewsNotFoundException extends RuntimeException{

	private static final long serialVersionUID = 1L;

}
