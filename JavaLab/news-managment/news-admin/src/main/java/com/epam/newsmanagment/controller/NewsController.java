package com.epam.newsmanagment.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.context.annotation.PropertySource;

import com.epam.newsmanagment.controller.exception.NewsNotFoundException;
import com.epam.newsmanagment.utils.DataConvertor;
import com.epam.ukadzer.entity.Comment;
import com.epam.ukadzer.entity.ComplexNews;
import com.epam.ukadzer.entity.SearchCriteria;
import com.epam.ukadzer.entity.Tag;
import com.epam.ukadzer.service.AuthorService;
import com.epam.ukadzer.service.CommentService;
import com.epam.ukadzer.service.ComplexNewsService;
import com.epam.ukadzer.service.NewsService;
import com.epam.ukadzer.service.TagService;
import com.epam.ukadzer.service.exception.ServiceException;


@Controller
@RequestMapping("/news")
@PropertySource("classpath:constants.properties")
public class NewsController {

	@Autowired
	private AuthorService authorService;

	@Autowired
	private NewsService newsService;

	@Autowired
	private TagService tagService;

	@Autowired
	private CommentService commentService;

	@Autowired
	private ComplexNewsService complexNewsService;
	
	@Value( "${newsList}" )
	private String newsList;
	
	@Value( "${singleNews}" )
	private String singleNews;
	
	@Value( "${editNews}" )
	private String editNews;
	
	@Value( "${newsPerPage}" )
	private Integer newsOnPageNumber;

	private static final Integer initialNewsNumber = 0;

	private static final Long anyAuthorId = 0L;

	@InitBinder
	private void dateBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		CustomDateEditor editor = new CustomDateEditor(dateFormat, true);
		binder.registerCustomEditor(Date.class, editor);
	}

	@RequestMapping("/filter")
	public String filterNewsList(@ModelAttribute SearchCriteria searchCriteria,
			HttpSession session, Model model) {

		if (model.containsAttribute("searchCriteria")) {
			if (searchCriteria.getAuthorId() != null
					&& searchCriteria.getAuthorId() == anyAuthorId) {
				searchCriteria.setAuthorId(null);
			}
			session.setAttribute("searchCriteria", searchCriteria);
		}

		return "redirect:/news/list/1";
	}
	

	@ExceptionHandler(NewsNotFoundException.class)
	public String handleNewsNotFound(){
		return "redirect:/errorNotFound";
		
	}
	
	@ExceptionHandler(ServiceException.class)
	public String handleServiceException(){
		return "redirect:/errorWithNewsService";
	}
	
	
	
	@ExceptionHandler(Exception.class)
	public String handleException(){
		return "redirect:/errorWithNews";
	}
	

	@RequestMapping("/list/{page}")
	public String pagination(@ModelAttribute SearchCriteria searchCriteria,
			@PathVariable Integer page, HttpServletRequest request,
			HttpSession session, Model model) throws ServiceException {

		List<ComplexNews> news = new ArrayList<>();

		int newsNum = initialNewsNumber;
		searchCriteria = (SearchCriteria) session
				.getAttribute("searchCriteria");
		model.addAttribute("searchCriteria", searchCriteria);

		news = complexNewsService.getSomeWithCriteria(searchCriteria, page
				* newsOnPageNumber - newsOnPageNumber + 1, page
				* newsOnPageNumber);
		newsNum = newsService.countAllWithCriteria(searchCriteria);
		int pagesNum = (newsNum) / newsOnPageNumber;
		if (newsNum % newsOnPageNumber != 0) {
			pagesNum++;
		}
		model.addAttribute("pagesNum", pagesNum);
		model.addAttribute("news", news);
		model.addAttribute("tags", tagService.getAllTags());
		model.addAttribute("authors", authorService.getAllAuthors());

		return newsList;
	}

	@RequestMapping("/{newsId}")
	public String showSingleNews(@PathVariable Long newsId,
			HttpSession session, Model modelInitial) {
		try {
			ComplexNews complexNews = complexNewsService.get(newsId);
			modelInitial.addAttribute("news", complexNews);
			if(complexNews==null){
				throw new NewsNotFoundException();
			}
			SearchCriteria searchCriteria = (SearchCriteria) session
					.getAttribute("searchCriteria");
			modelInitial.addAttribute("nextNewsId",
					newsService.getNextNewsId(newsId, searchCriteria));
			modelInitial.addAttribute("previousNewsId",
					newsService.getPreviousNewsId(newsId, searchCriteria));
			if (!modelInitial.containsAttribute("comment")) {
				modelInitial.addAttribute("comment", new Comment());
			}

		} catch (ServiceException e) {
			throw new NewsNotFoundException();
		}
		return singleNews;
	}

	@RequestMapping("/previous")
	public String showPreviousNews(HttpServletRequest request) {
		if (request.getParameter("previousNewsId") != null) {
			Long previousNewsId = Long.parseLong(request
					.getParameter("previousNewsId"));
			return "redirect:/news/" + previousNewsId;
		}
		return "redirect:/errorWithNews";

	}

	@RequestMapping("/next")
	public String showNextNews(HttpServletRequest request) {
		if (request.getParameter("nextNewsId") != null) {
			Long nextNewsId = Long
					.parseLong(request.getParameter("nextNewsId"));
			return "redirect:/news/" + nextNewsId;
		}
		return "redirect:/errorWithNews";

	}

	@RequestMapping("/delete")
	public String deleteNews(HttpServletRequest request)
			throws NumberFormatException, ServiceException {

		String[] params = request.getParameterValues("deleteId");
		List<Long> newsIdsList = new ArrayList<Long>();
		if (params != null) {
			for (int i = 0; i < params.length; i++) {
				newsIdsList.add(Long.parseLong(params[i]));
			}
		}
		complexNewsService.deleteNewsList(newsIdsList);
		return "redirect:/news/list/1";

	}

	@RequestMapping("/edit/{newsId}")
	public String editNews(@PathVariable Long newsId, Model model,
			HttpSession session) throws ServiceException {

		ComplexNews complexNews = complexNewsService.get(newsId);
		Map<Long, Boolean> searchedTagsMap = new HashMap<Long, Boolean>();

		for (Tag tag : complexNews.getTags()) {
			searchedTagsMap.put(tag.getId(), true);
		}

		model.addAttribute("searchedTagsMap", searchedTagsMap);
		if (!model.containsAttribute("complexNews")) {
			model.addAttribute("complexNews", complexNews);
		}
		model.addAttribute("authors", authorService.getAllAuthors());
		model.addAttribute("authorId", complexNews.getAuthor().getId());
		model.addAttribute("tags", tagService.getAllTags());
		model.addAttribute("searchedTags", complexNews.getTags());
		return editNews;

	}

	@RequestMapping("/add")
	public String addNews(Model model, HttpSession session)
			throws ServiceException {

		if (!model.containsAttribute("complexNews")) {
			model.addAttribute("complexNews", new ComplexNews());
		}
		model.addAttribute("authors", authorService.getAllAuthors());
		model.addAttribute("tags", tagService.getAllTags());
		return editNews;

	}

	@RequestMapping("/edit/save")
	public String saveEditedNews(
			@Valid @ModelAttribute("complexNews") ComplexNews complexNews,
			BindingResult bindingResult, RedirectAttributes attr,
			HttpServletRequest request, HttpSession session)
			throws ServiceException {

		if (bindingResult.hasErrors()) {
			putTagsIdsInComplexNews(request, complexNews);
			addFlashForComplex(attr, bindingResult, complexNews);
			return "redirect:/news/edit/" + complexNews.getNews().getId();
		}
		putTagsIdsInComplexNews(request, complexNews);
		complexNewsService.update(complexNews);
		return "redirect:/news/list/1";

	}

	@RequestMapping("/save")
	public String saveAddedNews(
			@Valid @ModelAttribute("complexNews") ComplexNews complexNews,
			BindingResult bindingResult, RedirectAttributes attr,
			HttpServletRequest request, HttpSession session,
			BindingResult result) throws ServiceException {

		if (bindingResult.hasErrors()) {
			addFlashForComplex(attr, bindingResult, complexNews);
			return "redirect:/news/add";
		}
		putTagsIdsInComplexNews(request, complexNews);
		Long id = complexNewsService.add(complexNews);
		return "redirect:/news/" + id;

	}

	@RequestMapping("/reset")
	public String reset(HttpSession session, RedirectAttributes attr) {

		session.setAttribute("searchCriteria", new SearchCriteria());
		attr.addFlashAttribute("searchCriteria", new SearchCriteria());
		return "redirect:/news/list/1";
	}

	@RequestMapping("/back")
	public String back(HttpSession session, RedirectAttributes attr) {

		return "redirect:/news/list/1";
	}

	private void addFlashForComplex(RedirectAttributes attr,
			BindingResult bindingResult, ComplexNews complexNews) {
		attr.addFlashAttribute(
				"org.springframework.validation.BindingResult.complexNews",
				bindingResult);
		attr.addFlashAttribute("complexNews", complexNews);

	}

	private void putTagsIdsInComplexNews(HttpServletRequest request,
			ComplexNews complexNews) throws ServiceException {
		if (request.getParameterValues("tagsIds") != null) {

			Set<Tag> tags;
			tags = new DataConvertor().stringArrayToTags(
					request.getParameterValues("tagsIds"), tagService);
			complexNews.setTags(tags);

		}

	}
}
