package com.epam.newsmanagment.controller;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.epam.ukadzer.entity.Author;
import com.epam.ukadzer.service.AuthorService;
import com.epam.ukadzer.service.exception.ServiceException;

@Controller
@RequestMapping("/author")
@PropertySource("classpath:constants.properties")
public class AuthorController {

	@Autowired
	private AuthorService authorService;
	
	@Value( "${authorsList}" )
	private String authorsList;
	

	@ExceptionHandler(ServiceException.class)
	public String handleServiceException(){
		return "redirect:/errorWithAuthorsService";
	}
	
	@ExceptionHandler(Exception.class)
	public String handleException(){
		return "redirect:/errorWithAuthors";
	}
	
	@RequestMapping("/list")
	public String seeAllAuthors(Model model) throws ServiceException {
		model.addAttribute("authors", authorService.getAllAuthors());
		if (!model.containsAttribute("author")) {
			model.addAttribute("author", new Author());
		}
		if (!model.containsAttribute("newAuthor")) {
			model.addAttribute("newAuthor", new Author());
		}
		return authorsList;

	}

	@RequestMapping("/edit")
	public String allowEditingAuthor(@RequestParam Long id, Model model,
			RedirectAttributes attr) throws ServiceException {
		attr.addFlashAttribute("authorForEditId", id);
		return "redirect:/author/list";

	}
	
	@RequestMapping("/cancel")
	public String cancelEditing() throws ServiceException {
		return "redirect:/author/list";

	}

	@RequestMapping("/save")
	public String saveAuthor(@Valid @ModelAttribute("author") Author author,
			BindingResult bindingResult, Model model, RedirectAttributes attr)
			throws ServiceException {
		if (bindingResult.hasErrors()) {
			attr.addFlashAttribute(
					"org.springframework.validation.BindingResult.author",
					bindingResult);
			attr.addFlashAttribute("author", author);
			attr.addFlashAttribute("authorForEditId", author.getId());
			return "redirect:/author/list";
		}
		authorService.update(author);
		attr.addFlashAttribute("authorForEditId", null);
		return "redirect:/author/list";
	}

	@RequestMapping("/expire/{authorId}")
	public String expireAuthor(@PathVariable Long authorId, Model model,
			RedirectAttributes attr) throws ServiceException {
		authorService.expire(new Author(authorId, "", null));
		attr.addFlashAttribute("authorForEditId", null);
		return "redirect:/author/list";

	}

	@RequestMapping("/add")
	public String addAuthor(
			@Valid @ModelAttribute("newAuthor") Author newAuthor,
			BindingResult bindingResult, Model model, RedirectAttributes attr)
			throws ServiceException {
		if (bindingResult.hasErrors()) {
			attr.addFlashAttribute(
					"org.springframework.validation.BindingResult.newAuthor",
					bindingResult);
			attr.addFlashAttribute("newAuthor", newAuthor);
			return "redirect:/author/list";
		}
		authorService.add(newAuthor);
		return "redirect:/author/list";
	}

}
