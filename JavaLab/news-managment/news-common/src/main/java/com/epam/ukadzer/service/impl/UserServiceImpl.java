package com.epam.ukadzer.service.impl;

import org.apache.log4j.Logger;

import com.epam.ukadzer.dao.UserDAO;
import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.User;
import com.epam.ukadzer.service.UserService;
import com.epam.ukadzer.service.exception.ServiceException;

public class UserServiceImpl implements UserService {

	private static final Logger logger = Logger
			.getLogger(UserServiceImpl.class);

	private UserDAO userDAO;

	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	@Override
	public Long add(User user) throws ServiceException {
		try {
			return userDAO.add(user);
		} catch (DAOException e) {
			logger.error("can't add an user");
			throw new ServiceException(e);
		}
	}

	@Override
	public User getById(Long id) throws ServiceException {
		try {
			return userDAO.getById(id);
		} catch (DAOException e) {
			logger.error("can't add an user");
			throw new ServiceException(e);
		}
	}

	@Override
	public boolean findIfExists(String name, String password)
			throws ServiceException {
		try {
			return userDAO.findIfExists(name, password);
		} catch (DAOException e) {
			logger.error("can't add an user");
			throw new ServiceException(e);
		}
	}

}
