package com.epam.ukadzer.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.transaction.annotation.Transactional;

import com.epam.ukadzer.entity.Comment;
import com.epam.ukadzer.entity.ComplexNews;
import com.epam.ukadzer.entity.News;
import com.epam.ukadzer.entity.SearchCriteria;
import com.epam.ukadzer.entity.Tag;
import com.epam.ukadzer.service.AuthorService;
import com.epam.ukadzer.service.CommentService;
import com.epam.ukadzer.service.ComplexNewsService;
import com.epam.ukadzer.service.NewsService;
import com.epam.ukadzer.service.TagService;
import com.epam.ukadzer.service.exception.ServiceException;

@Transactional(rollbackFor = Exception.class)
public class ComplexNewsServiceImpl implements ComplexNewsService {

	NewsService newsService;

	TagService tagService;

	CommentService commentService;

	AuthorService authorService;

	public void setNewsService(NewsService newsService) {
		this.newsService = newsService;
	}

	public void setTagService(TagService tagService) {
		this.tagService = tagService;
	}

	public void setAuthorService(AuthorService authorService) {
		this.authorService = authorService;
	}

	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}

	@Override
	public Long add(ComplexNews complexNews) throws ServiceException {
		Long newsId = newsService.addNews(complexNews.getNews());
		newsService.bindAuthor(newsId, complexNews.getAuthor().getId());
		Set<Long> set = new HashSet<Long>();
		if (complexNews.getTags() != null) {
			for (Tag tag : complexNews.getTags()) {
				set.add(tag.getId());
			}
		}
		newsService.bindTags(newsId, set);
		if (complexNews.getComments() != null) {
			for (Comment comment : complexNews.getComments()) {
				commentService.add(comment);
			}
		}
		return newsId;
	}

	@Override
	public void delete(Long newsId) throws ServiceException {
		newsService.deleteLinkToAuthor(newsId);
		newsService.deleteLinkToTags(newsId);
		commentService.deleteAllForNews(newsId);
		newsService.delete(newsId);
	}

	@Override
	public ComplexNews get(Long newsId) throws ServiceException {
		ComplexNews complexNews = null;
		News news = newsService.get(newsId);
		if (news != null) {
			complexNews = new ComplexNews();
			complexNews.setNews(news);
			complexNews.setAuthor(authorService.getById(authorService
					.getForNews(newsId)));
			Set<Tag> tags = new HashSet<Tag>();
			tags = tagService.getTagsForNews(tags, newsId);
			complexNews.setTags(tags);
			Set<Comment> comments;
			comments = commentService.getAllForNews(newsId);
			complexNews.setComments(comments);
		}
		return complexNews;
	}

	@Override
	public void update(ComplexNews complexNews) throws ServiceException {
		News news = complexNews.getNews();
		newsService.edit(news);
		newsService.deleteLinkToAuthor(news.getId());
		newsService.bindAuthor(news.getId(), complexNews.getAuthor().getId());
		newsService.deleteLinkToTags(news.getId());
		Set<Long> set = new HashSet<Long>();
		if (complexNews.getTags() != null) {
			for (Tag tag : complexNews.getTags()) {
				set.add(tag.getId());
			}
			newsService.bindTags(news.getId(), set);
		}
		Set<Comment> comments = new HashSet<Comment>();
		comments = commentService.getAllForNews(news.getId());
		complexNews.setComments(comments);
	}

	@Override
	public List<ComplexNews> getSomeWithCriteria(SearchCriteria searchCriteria,
			int first, int last) throws ServiceException {
		List<ComplexNews> complexNews = new ArrayList<ComplexNews>();
		List<News> news = new ArrayList<News>();
		news = newsService.getSomeWithCriteria(news, searchCriteria, first,
				last);
		for (int i = 0; i < news.size(); i++) {
			complexNews.add(get(news.get(i).getId()));
		}
		return complexNews;
	}

	@Transactional
	public void deleteNewsList(List<Long> newsIds) throws ServiceException {
		newsService.deleteLinkToAuthorForAllNews(newsIds);
		newsService.deleteLinkToTagsForAllNews(newsIds);
		commentService.deleteAllForAllNews(newsIds);
		newsService.deleteNewsList(newsIds);
	}

}
