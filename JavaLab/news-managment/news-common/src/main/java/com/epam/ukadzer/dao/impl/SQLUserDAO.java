package com.epam.ukadzer.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.ukadzer.dao.UserDAO;
import com.epam.ukadzer.dao.UtilsService;
import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.User;

public class SQLUserDAO implements UserDAO {

	private static final String SQL_GET_BY_ID = "select u.user_id, u.user_name, u.login, u.password from Users u where user_id=?";
	private static final String SQL_ADD = "insert into Users (user_id, user_name, login, password) values (user_sequence.nextval,?,?)";
	private static final String SQL_UPDATE = "update users set USER_NAME=?, LOGIN=?, PASSWORD=? where user_id=?";
	private static final String SQL_DELETE = "delete from users where user_id=?";
	private static final String SQL_GET_BY_NAME_PASS = "select u.user_id, u.user_name, u.login, u.password from Users u where login=? and password=?";
	private static final String SQL_USER_ID = "user_ID";

	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public User getById(Long entityId) throws DAOException {
		User user = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_GET_BY_ID);
			preparedStatement.setLong(1, entityId);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				user = new User(resultSet.getLong(1), resultSet.getString(2),
						resultSet.getString(3), resultSet.getString(4));
			}
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			UtilsService.closeConnection(connection, preparedStatement, null,
					dataSource);
		}
		return user;
	}

	public Long add(User entity) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		String generatedColumns[] = { SQL_USER_ID };
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_ADD,
					generatedColumns);
			preparedStatement.setString(1, entity.getName());
			preparedStatement.setString(2, entity.getLogin());
			preparedStatement.setString(3, entity.getPassword());
			preparedStatement.executeUpdate();
			try (ResultSet resultSet = preparedStatement.getGeneratedKeys();) {
				if (resultSet.next()) {
					return resultSet.getLong(1);
				} else {
					throw new SQLException();
				}
			}

		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			UtilsService.closeConnection(connection, preparedStatement, null,
					dataSource);
		}

	}

	public void update(User entity) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_UPDATE);
			preparedStatement.setString(1, entity.getName());
			preparedStatement.setString(2, entity.getLogin());
			preparedStatement.setString(3, entity.getPassword());
			preparedStatement.setLong(4, entity.getId());
			preparedStatement.executeUpdate();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			UtilsService.closeConnection(connection, preparedStatement, null,
					dataSource);
		}

	}

	public void delete(Long entityId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_DELETE);
			preparedStatement.setLong(1, entityId);
			preparedStatement.executeUpdate();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			UtilsService.closeConnection(connection, preparedStatement, null,
					dataSource);
		}

	}

	@Override
	public boolean findIfExists(String name, String password)
			throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection
					.prepareStatement(SQL_GET_BY_NAME_PASS);
			preparedStatement.setString(1, name);
			preparedStatement.setString(2, password);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				return true;
			}
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			UtilsService.closeConnection(connection, preparedStatement, null,
					dataSource);
		}
		return false;
	}

}
