package com.epam.ukadzer.entity;

import java.util.Set;

import javax.validation.Valid;

public class ComplexNews{
	
	@Valid
	private News news;
	
	@Valid
	private Author author;
	
	private Set<Tag> tags;
	
	private Set<Comment> comments;
	
	public ComplexNews() {}

	public ComplexNews(News news, Author author, Set<Tag> tags, Set<Comment> comments) {
		super();
		this.news = news;
		this.author = author;
		this.tags = tags;
		this.comments = comments;
	}
	

	@Override
	public String toString() {
		return "ComplexNews [" + (news != null ? "news=" + news + ", " : "")
				+ (author != null ? "author=" + author + ", " : "")
				+ (tags != null ? "tags=" + tags + ", " : "")
				+ (comments != null ? "comments=" + comments : "") + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result
				+ ((comments == null) ? 0 : comments.hashCode());
		result = prime * result + ((news == null) ? 0 : news.hashCode());
		result = prime * result + ((tags == null) ? 0 : tags.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ComplexNews other = (ComplexNews) obj;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (comments == null) {
			if (other.comments != null)
				return false;
		} else if (!comments.equals(other.comments))
			return false;
		if (news == null) {
			if (other.news != null)
				return false;
		} else if (!news.equals(other.news))
			return false;
		if (tags == null) {
			if (other.tags != null)
				return false;
		} else if (!tags.equals(other.tags))
			return false;
		return true;
	}

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public Set<Tag> getTags() {
		return tags;
	}

	public void setTags(Set<Tag> tags) {
		this.tags = tags;
	}

	public Set<Comment> getComments() {
		return comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}


	
	
	

}
