package com.epam.ukadzer.dao;

import java.util.List;

import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.Author;

public interface AuthorDAO extends CRUD<Author>{
	
	public Long getForNews(Long id) throws DAOException;
	
	/**
     * Method gets all the authors from db
     * @param all collection to get all object
     * @return collection with all objects
     */
    List<Author> getAllAuthors() throws DAOException;
	
}
