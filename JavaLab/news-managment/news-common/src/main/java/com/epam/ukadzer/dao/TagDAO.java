package com.epam.ukadzer.dao;

import java.util.List;
import java.util.Set;

import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.Tag;

public interface TagDAO extends CRUD<Tag>{
	

	/**
     * Method deletes all links of given tag
     * @param tagId the id of tag
	 * @throws DAOException
     */
	public void deleteLinkToNews(Long tagId) throws DAOException;
	
	/**
     * Method deletes all links of given tag
     * @param tagId the id of tag
	 * @throws DAOException
     */
	public Set<Tag> getAllForNews(Set<Tag> tagsIds, Long newsId) throws DAOException;
	
	/**
     * Method gets all the tags from db
     * @param all collection to get all object
     * @return collection with all objects
     */
    List<Tag> getAllTags() throws DAOException;
    
    
}