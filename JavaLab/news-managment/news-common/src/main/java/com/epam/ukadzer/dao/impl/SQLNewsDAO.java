package com.epam.ukadzer.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.ukadzer.dao.NewsDAO;
import com.epam.ukadzer.dao.UtilsService;
import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.News;
import com.epam.ukadzer.entity.SearchCriteria;

public class SQLNewsDAO implements NewsDAO {

	private static final String SQL_GET_BY_ID = "select n.news_id, n.TITLE, n.SHORT_TEXT, "
			+ "n.FULL_TEXT, n.CREATION_DATE, n.MODIFICATION_DATE from news n where news_id=?";
	private static final String SQL_ADD = "insert into news (news_id,title,short_text, full_text, creation_date, modification_date) "
			+ "values (news_sequence.nextval,?,?,?,?,?)";
	private static final String SQL_UPDATE = "update news set title=?, short_text=?, full_text=?, "
			+ "creation_date=?, modification_date=? where news_id=?";
	private static final String SQL_DELETE_FROM_NEWS = "delete from news where news_id=?";
	private static final String SQL_GET_ALL = "select n.TITLE, n.SHORT_TEXT, "
			+ "n.FULL_TEXT, n.CREATION_DATE, n.MODIFICATION_DATE from news";
	private static final String SQL_ADD_TAG = "insert into news_tags (news_tag_id, news_id, tag_id) "
			+ "values (news_tags_sequence.nextval,?,?)";
	private static final String SQL_COUNT_ALL = "select count(*) from news";
	private static final String SQL_GET_ALL_SORTED_BY_COMM = "select distinct n.News_id, n.TITLE, n.SHORT_TEXT, "
			+ "n.FULL_TEXT, n.CREATION_DATE, n.MODIFICATION_DATE, count(c.comment_id)"
			+ "from news n LEFT OUTER JOIN comments c on n.news_id=c.news_id "
			+ "group by n.News_id, n.TITLE, n.SHORT_TEXT, n.FULL_TEXT, n.CREATION_DATE, n.MODIFICATION_DATE"
			+ " order by count(c.comment_id)desc , n.news_id";
	private static final String SQL_ADD_TAGS = "insert into news_tags (news_tag_id, news_id, tag_id) "
			+ "values (news_tags_sequence.nextval,?,?) ";
	private static final String SQL_ADD_AUTHOR = "insert into news_authors (news_author_id, news_id, author_id) "
			+ "values (news_ath_sequence.nextval,?,?) ";
	private static final String SQL_FIND_SOME_BEGINING = "select distinct news_id, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE from ( select a.*, ROWNUM rnum from ( "
			+ "SELECT distinct n.news_id, n.title, n.SHORT_TEXT, n.FULL_TEXT, n.CREATION_DATE, n.MODIFICATION_DATE FROM  news n LEFT OUTER JOIN NEWS_AUTHORS na on n.NEWS_ID=na.NEWS_ID "
			+ "LEFT outer join NEWS_TAGS nt on n.NEWS_ID=nt.NEWS_ID LEFT OUTER JOIN comments c  ON n.news_id = c.news_id ";
	private static final String SQL_COUNT_CRITERIA_BEGINING = "select count(distinct n.news_id) from news n LEFT OUTER JOIN NEWS_AUTHORS na on n.NEWS_ID=na.NEWS_ID "
			+ "LEFT outer join NEWS_TAGS nt on n.NEWS_ID=nt.NEWS_ID ";
	private static final String SQL_FIND_ALL_BEGINING = "select n.news_id, n.TITLE, n.SHORT_TEXT, "
			+ "n.FULL_TEXT, n.CREATION_DATE, n.MODIFICATION_DATE from news n LEFT OUTER JOIN NEWS_AUTHORS na on n.NEWS_ID=na.NEWS_ID "
			+ "LEFT outer join NEWS_TAGS nt on n.NEWS_ID=nt.NEWS_ID ";
	private static final String SQL_DELETE_LINK_TO_AUTHOR = "delete from news_authors where news_id=?";
	private static final String SQL_DELETE_LINK_TO_TAGS = "delete from news_tags where news_id=?";
	private static final String SQL_GET_NEXT_NEWS = "select prev_news_id from (select News_id, MODIFICATION_DATE, Lead(news_id, 1) over(order by modification_date desc) as prev_news_id from ( select distinct n.news_id, n.MODIFICATION_DATE, count(c.comment_id) as comm_num from news n  LEFT OUTER JOIN NEWS_AUTHORS na on n.NEWS_ID=na.NEWS_ID  LEFT outer join NEWS_TAGS nt on n.NEWS_ID=nt.NEWS_ID  left OUTER JOIN comments c ON n.news_id = c.news_id ";
	private static final String SQL_GET_PREVIOUS_NEWS = "select prev_news_id from (select News_id, MODIFICATION_DATE, Lag(news_id, 1) over(order by modification_date desc) as prev_news_id from ( select distinct n.news_id, n.MODIFICATION_DATE, count(c.comment_id) as comm_num from news n  LEFT OUTER JOIN NEWS_AUTHORS na on n.NEWS_ID=na.NEWS_ID  LEFT outer join NEWS_TAGS nt on n.NEWS_ID=nt.NEWS_ID  left OUTER JOIN comments c ON n.news_id = c.news_id ";
	private static final String SQL_NEWS_ID = "news_ID";
	private static final String SQL_GET_SOME_SECOND_PART = " order by  n.modification_date desc) a where rownum <=  ";
	private static final String SQL_GET_SOME_THIRD_PART = ") where rnum >= ";
	private static final String SQL_GET_SOME_LAST_PART = " order by modification_date DESC";
	private static final String SQL_GET_PREV_NEXT_LAST_PART = " group by n.news_id, n.MODIFICATION_DATE)) where news_id=";
	private static final String SQL_WHERE_AUTHOR_ID = "where author_id=";
	private static final String SQL_AND_OPENIONG_BRACKET = "AND ( ";
	private static final String SQL_WHERE = " where ";
	private static final String SQL_TAG_ID_EQUALS = "tag_id=";
	private static final String SQL_OR_TAG_ID = "or tag_id=";
	private static final String SQL_CLOSING_BRACKET = ")";

	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public News getById(Long entityId) throws DAOException {

		News news = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_GET_BY_ID);
			preparedStatement.setLong(1, entityId);
			try (ResultSet resultSet = preparedStatement.executeQuery();) {
				if (resultSet.next()) {
					news = new News(resultSet.getLong(1),
							resultSet.getString(2), resultSet.getString(3),
							resultSet.getString(4), resultSet.getTimestamp(5, cal),
							resultSet.getDate(6, cal));
				}
			}
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			UtilsService.closeConnection(connection, preparedStatement, null,
					dataSource);
		}
		return news;
	}

	public Long add(News entity) throws DAOException {
		String generatedColumns[] = { SQL_NEWS_ID };
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		try {
			SimpleDateFormat dateFormatGmt = new SimpleDateFormat(
					"yyyy-MMM-dd HH:mm:ss");
			dateFormatGmt.setTimeZone(TimeZone.getTimeZone("UTC"));
			SimpleDateFormat dateFormatLocal = new SimpleDateFormat(
					"yyyy-MMM-dd HH:mm:ss");
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_ADD,
					generatedColumns);
			preparedStatement.setString(1, entity.getTitle());
			preparedStatement.setString(2, entity.getShortText());
			preparedStatement.setString(3, entity.getFullText());
			preparedStatement.setTimestamp(4,
					utilDatetoTimestamp(dateFormatLocal.parse(dateFormatGmt
							.format(entity.getCreationDate()))));
			preparedStatement.setTimestamp(5,
					utilDatetoTimestamp(dateFormatLocal.parse(dateFormatGmt
							.format(new Date()))));
			preparedStatement.executeUpdate();
			try (ResultSet resultSet = preparedStatement.getGeneratedKeys();) {
				if (resultSet.next()) {
					return resultSet.getLong(1);
				} else {
					throw new SQLException();
				}
			}
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			UtilsService.closeConnection(connection, preparedStatement, null,
					dataSource);
		}
	}

	public void update(News entity) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			SimpleDateFormat dateFormatGmt = new SimpleDateFormat(
					"yyyy-MMM-dd HH:mm:ss");
			dateFormatGmt.setTimeZone(TimeZone.getTimeZone("UTC"));
			SimpleDateFormat dateFormatLocal = new SimpleDateFormat(
					"yyyy-MMM-dd HH:mm:ss");
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_UPDATE);
			preparedStatement.setString(1, entity.getTitle());
			preparedStatement.setString(2, entity.getShortText());
			preparedStatement.setString(3, entity.getFullText());
			preparedStatement.setTimestamp(4,
					utilDatetoTimestamp(dateFormatLocal.parse(dateFormatGmt
							.format(entity.getCreationDate()))));
			preparedStatement.setTimestamp(5,
					utilDatetoTimestamp(dateFormatLocal.parse(dateFormatGmt
							.format(new Date()))));
			preparedStatement.setLong(6, entity.getId());
			preparedStatement.executeUpdate();

		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			UtilsService.closeConnection(connection, preparedStatement, null,
					dataSource);
		}
	}

	public void delete(Long entityId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection
					.prepareStatement(SQL_DELETE_FROM_NEWS);
			preparedStatement.setLong(1, entityId);
			preparedStatement.executeUpdate();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			UtilsService.closeConnection(connection, preparedStatement, null,
					dataSource);
		}
	}

	public List<News> getAllNews() throws DAOException {
		List<News> news = new ArrayList<News>();
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
			connection = DataSourceUtils.getConnection(dataSource);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(SQL_GET_ALL);
			while (resultSet.next()) {
				news.add(new News(resultSet.getLong(1), resultSet.getString(2),
						resultSet.getString(3), resultSet.getString(4),
						resultSet.getTimestamp(5, cal), resultSet.getDate(6,cal)));
			}

		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			UtilsService.closeConnection(connection, statement, resultSet,
					dataSource);
		}
		return news;
	}

	public void bindTag(Long newsId, Long tagId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_ADD_TAG);
			preparedStatement.setLong(1, newsId);
			preparedStatement.setLong(2, tagId);
			preparedStatement.executeUpdate();

		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			UtilsService.closeConnection(connection, preparedStatement, null,
					dataSource);
		}

	}

	public int countAll() throws DAOException {
		String query = SQL_COUNT_ALL;
		int result = -1;
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);
			if (resultSet.next()) {
				result = resultSet.getInt(1);
			}
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			UtilsService.closeConnection(connection, statement, resultSet,
					dataSource);
		}
		return result;
	}

	public List<News> getAllSortedByComments(List<News> all)
			throws DAOException {
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
			connection = DataSourceUtils.getConnection(dataSource);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(SQL_GET_ALL_SORTED_BY_COMM);
			while (resultSet.next()) {
				all.add(new News(resultSet.getLong(1), resultSet.getString(2),
						resultSet.getString(3), resultSet.getString(4),
						resultSet.getTimestamp(5, cal), resultSet.getDate(6, cal)));
			}

		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			UtilsService.closeConnection(connection, statement, resultSet,
					dataSource);
		}
		return all;
	}

	public void bindTags(Long NewsId, Set<Long> tagsIds) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_ADD_TAGS);

			Iterator<Long> itr = tagsIds.iterator();
			while (itr.hasNext()) {
				preparedStatement.setLong(1, NewsId);
				preparedStatement.setLong(2, itr.next());
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();

		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			UtilsService.closeConnection(connection, preparedStatement, null,
					dataSource);
		}
	}

	public List<News> findAllWithCriteria(List<News> all,
			SearchCriteria searchCriteria) throws DAOException {
		String query = SQL_FIND_ALL_BEGINING;
		Connection connection = null;
		Statement statement = null;
		try {
			Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
			connection = DataSourceUtils.getConnection(dataSource);
			statement = connection.createStatement();
			query += sqlSearchCriteriaLina(searchCriteria);
			ResultSet resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				all.add(new News(resultSet.getLong(1), resultSet.getString(2),
						resultSet.getString(3), resultSet.getString(4),
						resultSet.getTimestamp(5, cal), resultSet.getDate(6, cal)));
			}

		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			UtilsService.closeConnection(connection, statement, null,
					dataSource);
		}
		return all;
	}

	public int countAllWithCriteria(SearchCriteria searchCriteria)
			throws DAOException {
		Connection connection = null;
		Statement statement = null;
		String query = SQL_COUNT_CRITERIA_BEGINING;
		int result = -1;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			statement = connection.createStatement();
			query += sqlSearchCriteriaLina(searchCriteria);
			try (ResultSet resultSet = statement.executeQuery(query);) {
				while (resultSet.next()) {
					result = resultSet.getInt(1);
				}
			}
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			UtilsService.closeConnection(connection, statement, null,
					dataSource);
		}
		return result;
	}

	public List<News> findSomeWithCriteria(List<News> all,
			SearchCriteria searchCriteria, int start, int finish)
			throws DAOException {
		Connection connection = null;
		Statement statement = null;
		String query = SQL_FIND_SOME_BEGINING;
		try {
			Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
			connection = DataSourceUtils.getConnection(dataSource);
			statement = connection.createStatement();
			query += sqlSearchCriteriaLina(searchCriteria);
			query += SQL_GET_SOME_SECOND_PART + finish
					+ SQL_GET_SOME_THIRD_PART + start + SQL_GET_SOME_LAST_PART;
			try (ResultSet resultSet = statement.executeQuery(query);) {
				while (resultSet.next()) {
					all.add(new News(resultSet.getLong(1), resultSet
							.getString(2), resultSet.getString(3), resultSet
							.getString(4), resultSet.getTimestamp(5, cal), resultSet
							.getDate(6, cal)));
				}
			}

		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			UtilsService.closeConnection(connection, statement, null,
					dataSource);
		}
		return all;
	}

	@Override
	public void bindAuthor(Long newsId, Long authorId) throws DAOException {

		String query = SQL_ADD_AUTHOR;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(query);
			preparedStatement.setLong(1, newsId);
			preparedStatement.setLong(2, authorId);
			preparedStatement.executeUpdate();

		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			UtilsService.closeConnection(connection, preparedStatement, null,
					dataSource);
		}

	}

	@Override
	public void deleteLinkToAuthor(Long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection
					.prepareStatement(SQL_DELETE_LINK_TO_AUTHOR);
			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			UtilsService.closeConnection(connection, preparedStatement, null,
					dataSource);
		}

	}

	@Override
	public void deleteLinkToTags(Long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection
					.prepareStatement(SQL_DELETE_LINK_TO_TAGS);
			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			UtilsService.closeConnection(connection, preparedStatement, null,
					dataSource);
		}

	}

	@Override
	public Long getNextNewsId(Long newsId, SearchCriteria searchCriteria)
			throws DAOException {
		Long nextNewsId = null;
		String query = SQL_GET_NEXT_NEWS;
		Connection connection = null;
		Statement statement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			statement = connection.createStatement();
			query += sqlSearchCriteriaLina(searchCriteria);
			query = query + SQL_GET_PREV_NEXT_LAST_PART + newsId;
			try (ResultSet resultSet = statement.executeQuery(query);) {
				if (resultSet.next()) {
					nextNewsId = resultSet.getLong(1);
				}
			}
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			UtilsService.closeConnection(connection, statement, null,
					dataSource);
		}
		return nextNewsId;
	}

	@Override
	public Long getPreviousNewsId(Long newsId, SearchCriteria searchCriteria)
			throws DAOException {
		Long nextNewsId = null;
		String query = SQL_GET_PREVIOUS_NEWS;
		Connection connection = null;
		Statement statement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			statement = connection.createStatement();
			query += sqlSearchCriteriaLina(searchCriteria);
			query = query + SQL_GET_PREV_NEXT_LAST_PART + newsId;
			try (ResultSet resultSet = statement.executeQuery(query);) {
				if (resultSet.next()) {
					nextNewsId = resultSet.getLong(1);
				}
			}
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			UtilsService.closeConnection(connection, statement, null,
					dataSource);
		}
		return nextNewsId;
	}

	public void deleteNewsList(List<Long> newsIds) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection
					.prepareStatement(SQL_DELETE_FROM_NEWS);
			for (Long id : newsIds) {
				preparedStatement.setLong(1, id);
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			UtilsService.closeConnection(connection, preparedStatement, null,
					dataSource);
		}
	}

	public void deleteLinkToAuthorForAllNews(List<Long> newsIds)
			throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection
					.prepareStatement(SQL_DELETE_LINK_TO_AUTHOR);
			for (Long id : newsIds) {
				preparedStatement.setLong(1, id);
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			UtilsService.closeConnection(connection, preparedStatement, null,
					dataSource);
		}
	}

	public void deleteLinkToTagsForAllNews(List<Long> newsIds)
			throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection
					.prepareStatement(SQL_DELETE_LINK_TO_TAGS);
			for (Long id : newsIds) {
				preparedStatement.setLong(1, id);
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			UtilsService.closeConnection(connection, preparedStatement, null,
					dataSource);
		}

	}

	private String sqlSearchCriteriaLina(SearchCriteria searchCriteria) {
		String query = " ";
		boolean authorExists = false;
		if (searchCriteria.getAuthorId() != null) {
			authorExists = true;
			query += SQL_WHERE_AUTHOR_ID + searchCriteria.getAuthorId() + " ";
		}
		if (searchCriteria.getTagsIds() != null
				&& searchCriteria.getTagsIds().size() != 0) {
			Iterator<Long> itr = searchCriteria.getTagsIds().iterator();
			if (authorExists) {
				query += SQL_AND_OPENIONG_BRACKET;
			} else {
				query += SQL_WHERE;
			}
			int i = 0;
			while (itr.hasNext()) {
				if (i == 0) {
					query += SQL_TAG_ID_EQUALS + itr.next() + " ";
					i++;
				} else {
					query += SQL_OR_TAG_ID + itr.next() + " ";
				}
			}
			if (authorExists) {
				query += SQL_CLOSING_BRACKET;
			}
		}
		return query;
	}

	private Timestamp utilDatetoTimestamp(Date date) {
		if (date == null) {
			return null;
		} else {
			return new Timestamp(date.getTime());
		}
	}

}
