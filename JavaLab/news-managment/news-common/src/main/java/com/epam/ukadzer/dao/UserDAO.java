package com.epam.ukadzer.dao;

import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.User;

public interface UserDAO extends CRUD<User>{
	
	/**
     * Method finds an author with given name and id if exists
     * @param name name of user
     * @param password password of user
	 * @throws DAOException
     */
	public boolean findIfExists(String name, String password) throws DAOException;

}
