package com.epam.ukadzer.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.ukadzer.dao.AuthorDAO;
import com.epam.ukadzer.dao.UtilsService;
import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.Author;

public class SQLAuthorDAO implements AuthorDAO {

	private static final String SQL_GET_BY_ID = "select a.author_id, a.author_name, a.expired from Authors a where author_id=?";
	private static final String SQL_ADD = "insert into Authors (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (ath_sequence.nextval,?,?)";
	private static final String SQL_UPDATE = "update authors set AUTHOR_NAME=? where author_id=?";
	private static final String SQL_GET_ALL = "select a.author_id, a.author_name, a.expired from authors a";
	private static final String SQL_GET_FOR_NEWS = "select author_id from news_authors a where news_id=?";
	private static final String SQL_AUTHOR_ID = "author_ID";

	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public Author getById(Long entityId) throws DAOException {
		Author author = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_GET_BY_ID);
			preparedStatement.setLong(1, entityId);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				author = new Author(resultSet.getLong(1),
						resultSet.getString(2), resultSet.getTimestamp(3, cal));
			}
		} catch (Exception e) {

			throw new DAOException(e);
		} finally {
			UtilsService.closeConnection(connection, preparedStatement, null,
					dataSource);
		}
		return author;
	}

	public Long add(Author entity) throws DAOException {

		String generatedColumns[] = { SQL_AUTHOR_ID };
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_ADD,
					generatedColumns);
			String nameString = entity.getName();
			preparedStatement.setString(1, nameString);
			preparedStatement.setTimestamp(2,
					utilDatetoTimestamp(entity.getExpired()));

			preparedStatement.executeUpdate();
			try (ResultSet resultSet = preparedStatement.getGeneratedKeys();) {
				if (resultSet.next()) {
					return resultSet.getLong(1);
				} else {
					throw new SQLException();
				}
			}

		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			UtilsService.closeConnection(connection, preparedStatement, null,
					dataSource);
		}

	}

	public void update(Author entity) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_UPDATE);
			preparedStatement.setString(1, entity.getName());
			preparedStatement.setLong(2, entity.getId());
			preparedStatement.executeUpdate();
		} catch (Exception e) {

			throw new DAOException(e);
		} finally {
			UtilsService.closeConnection(connection, preparedStatement, null,
					dataSource);
		}

	}

	public void delete(Long entityId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			SimpleDateFormat dateFormatGmt = new SimpleDateFormat(
					"yyyy-MMM-dd HH:mm:ss");
			dateFormatGmt.setTimeZone(TimeZone.getTimeZone("UTC"));
			SimpleDateFormat dateFormatLocal = new SimpleDateFormat(
					"yyyy-MMM-dd HH:mm:ss");
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_UPDATE);
			preparedStatement.setTimestamp(1,
					utilDatetoTimestamp(dateFormatLocal.parse(dateFormatGmt
							.format(new Date()))));
			preparedStatement.setLong(2, entityId);
			preparedStatement.executeUpdate();
		} catch (Exception e) {

			throw new DAOException(e);
		} finally {
			UtilsService.closeConnection(connection, preparedStatement, null,
					dataSource);
		}

	}

	public List<Author> getAllAuthors() throws DAOException {

		List<Author> authors = new ArrayList<Author>();
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
			connection = DataSourceUtils.getConnection(dataSource);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(SQL_GET_ALL);
			while (resultSet.next()) {
				authors.add(new Author(resultSet.getLong(1), resultSet
						.getString(2), resultSet.getTimestamp(3, cal)));
			}

		} catch (Exception e) {

			throw new DAOException(e);
		} finally {
			UtilsService.closeConnection(connection, statement, null,
					dataSource);
		}
		return authors;
	}

	public Long getForNews(Long id) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_GET_FOR_NEWS);
			preparedStatement.setLong(1, id);
			ResultSet resultSet = preparedStatement.executeQuery();
			resultSet.next();
			return resultSet.getLong(1);
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			UtilsService.closeConnection(connection, preparedStatement, null,
					dataSource);
		}
	}

	private Timestamp utilDatetoTimestamp(Date date) {
		if (date == null) {
			return null;
		} else {
			return new Timestamp(date.getTime());
		}
	}

}
