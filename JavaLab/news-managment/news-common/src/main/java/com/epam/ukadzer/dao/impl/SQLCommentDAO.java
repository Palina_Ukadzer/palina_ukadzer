package com.epam.ukadzer.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.ukadzer.dao.CommentDAO;
import com.epam.ukadzer.dao.UtilsService;
import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.Comment;

public class SQLCommentDAO implements CommentDAO {

	private static final String SQL_GET_BY_ID = "select c.comment_id, c.news_id, c.comment_text, c.creation_date from comments c where comment_id=?";
	private static final String SQL_ADD = "insert into comments (comment_id,news_id,comment_text, creation_date) "
			+ "values (comm_sequence.nextval,?,?,?)";
	private static final String SQL_UPDATE = "update comments set comment_text=? where comment_id=?";
	private static final String SQL_DELETE = "delete from comments where comment_id=?";
	private static final String SQL_GEL_ALL = "select c.comment_id, c.news_id, c.comment_text, c.creation_date from comments c";
	private static final String SQL_DELETE_FOR_NEWS = "delete from comments where news_id=?";
	private static final String SQL_GEL_ALL_FOR_NEWS = "select c.comment_id, c.news_id, c.comment_text, c.creation_date from comments c where c.news_id=? order by c.comment_id ";
	private static final String SQL_COMMENT_ID = "comment_ID";

	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public Comment getById(Long entityId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		Comment comment = new Comment();
		try {
			Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Europe/Moscow"));
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_GET_BY_ID);
			preparedStatement.setLong(1, entityId);
			try (ResultSet resultSet = preparedStatement.executeQuery();) {
				if (resultSet.next()) {
					comment = new Comment(resultSet.getLong(1),
							resultSet.getLong(2), resultSet.getString(3),
							resultSet.getDate(4,cal));
				}
			}
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {

			UtilsService.closeConnection(connection, preparedStatement, null,
					dataSource);

		}
		return comment;
	}

	public Long add(Comment entity) throws DAOException {
		String generatedColumns[] = { SQL_COMMENT_ID };
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
			SimpleDateFormat dateFormatGmt = new SimpleDateFormat(
					"yyyy-MMM-dd HH:mm:ss ");
			dateFormatGmt.setTimeZone(TimeZone.getTimeZone("UTC"));
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_ADD,
					generatedColumns);
			preparedStatement.setLong(1, entity.getNewsId());
			preparedStatement.setString(2, entity.getText());
			if (entity.getCreationDate() != null) {
				preparedStatement.setTimestamp(3,
						utilDatetoTimestamp(dateFormatGmt.parse(dateFormatGmt
								.format(entity.getCreationDate()))), cal);
			} else {
				preparedStatement.setTimestamp(3,
						utilDatetoTimestamp(dateFormatGmt.parse(dateFormatGmt
								.format(new Date()))), cal);
			}

			preparedStatement.executeUpdate();
			try (ResultSet resultSet = preparedStatement.getGeneratedKeys();) {
				if (resultSet.next()) {
					return resultSet.getLong(1);
				} else {
					throw new SQLException();
				}
			}
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {

			UtilsService.closeConnection(connection, preparedStatement, null,
					dataSource);

		}
	}

	public void update(Comment entity) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_UPDATE);
			preparedStatement.setString(1, entity.getText());
			preparedStatement.setLong(2, entity.getId());
			preparedStatement.executeUpdate();

		} catch (Exception e) {
			throw new DAOException(e);
		} finally {

			UtilsService.closeConnection(connection, preparedStatement, null,
					dataSource);
		}

	}

	public void delete(Long entityId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_DELETE);
			preparedStatement.setLong(1, entityId);
			preparedStatement.executeUpdate();

		} catch (Exception e) {
			throw new DAOException(e);
		} finally {

			UtilsService.closeConnection(connection, preparedStatement, null,
					dataSource);

		}
	}

	public List<Comment> getAllComments() throws DAOException {
		List<Comment> comments = new ArrayList<Comment>();
		Connection connection = null;
		Statement statement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(SQL_GEL_ALL);
			while (resultSet.next()) {
				comments.add(new Comment(resultSet.getLong(1), resultSet
						.getLong(2), resultSet.getString(3), resultSet
						.getTimestamp(4)));
			}

		} catch (Exception e) {
			throw new DAOException(e);
		} finally {

			UtilsService.closeConnection(connection, statement, null,
					dataSource);

		}
		return comments;
	}

	private Timestamp utilDatetoTimestamp(Date date) {
		if (date == null) {
			return null;
		} else {
			return new Timestamp(date.getTime());
		}
	}

	@Override
	public void deleteAllForNews(Long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection
					.prepareStatement(SQL_DELETE_FOR_NEWS);
			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();

		} catch (Exception e) {
			throw new DAOException(e);
		} finally {

			UtilsService.closeConnection(connection, preparedStatement, null,
					dataSource);

		}

	}

	@Override
	public Set<Comment> getAllForNews(Long newsId) throws DAOException {
		Set<Comment> comments = new LinkedHashSet<Comment>();
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
			connection = DataSourceUtils.getConnection(dataSource);
			statement = connection.prepareStatement(SQL_GEL_ALL_FOR_NEWS);
			statement.setLong(1, newsId);
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				comments.add(new Comment(resultSet.getLong(1), resultSet
						.getLong(2), resultSet.getString(3), resultSet
						.getTimestamp(4, cal)));
			}

		} catch (Exception e) {
			throw new DAOException(e);
		} finally {

			UtilsService.closeConnection(connection, statement, null,
					dataSource);

		}
		return comments;
	}

	public void deleteAllForAllNews(List<Long> newsIds) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection
					.prepareStatement(SQL_DELETE_FOR_NEWS);
			for (Long id : newsIds) {
				preparedStatement.setLong(1, id);
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();

		} catch (Exception e) {
			throw new DAOException(e);
		} finally {

			UtilsService.closeConnection(connection, preparedStatement, null,
					dataSource);

		}
	}

}
