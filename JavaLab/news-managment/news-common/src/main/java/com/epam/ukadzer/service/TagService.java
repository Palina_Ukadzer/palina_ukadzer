package com.epam.ukadzer.service;

import java.util.List;
import java.util.Set;

import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.Tag;
import com.epam.ukadzer.service.exception.ServiceException;


public interface TagService {
	/**
     * Method creates an new tag (saves it to db)
     * @param tag tag to be saved
     * @return id of saved tag
	 * @throws DAOException
     */
	public Long add(Tag tag) throws ServiceException;
	
	/**
     * Method updates a tag
     * @param tag tag to be updated
	 * @throws DAOException
     */
	public void update(Tag tag) throws ServiceException;
	
	/**
     * Method deletes a tag
     * @param id id of tag to be deleted
	 * @throws DAOException
     */
	public void delete(Long id) throws ServiceException;
	
	/**
     * Method gets a tag with given id
     * @param id id of tag to get 
     * @return found tag
	 * @throws DAOException
     */
	public Tag getById(Long id) throws ServiceException;
	
	/**
     * Method deletes links of tag with news
     * @param tagId id of tag
	 * @throws DAOException
     */
	public void deleteLinkToNews(Long tagId) throws ServiceException;

	
	/**
     * Method gets all tags
     * @param tags collection of tags where new will be put
     * @returns collection of found tags 
	 * @throws ServiceException
     */
	public List<Tag> getAllTags() throws ServiceException;
	
	/**
     * Method gets all tags for news
     * @param tags collection of tags where new will be put
     * @returns collection of found tags 
	 * @throws ServiceException
     */
	public Set<Tag> getTagsForNews(Set<Tag> tagsIds, Long newsId) throws ServiceException;
	
	
}
