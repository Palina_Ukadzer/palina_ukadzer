package com.epam.ukadzer.entity;

import java.util.Date;

import javax.validation.constraints.Size;


public class Comment{
	
	private Long newsId;
	
	@Size(min = 1 , max=100)
	private String text;
	
	
	private Date creationDate;
	private Long id;
	
	public Comment() {}
	
	
	public Comment(Long id, Long newsId, String text, Date creationDate) {
		this.id = id;
		this.newsId = newsId;
		this.text = text;
		this.creationDate = creationDate;
	}

	public Long getNewsId() {
		return newsId;
	}
	
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public Date getCreationDate() {
		return creationDate;
	}
	
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	@Override
    public String toString() {
        return "Comment whith id=" + id + "; newsId=" + newsId +
                "; text=" + text + "; creation date=" + creationDate;
    }


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + (int) (newsId ^ (newsId >>> 32));
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comment other = (Comment) obj;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (newsId != other.newsId)
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		return true;
	}

    
}
