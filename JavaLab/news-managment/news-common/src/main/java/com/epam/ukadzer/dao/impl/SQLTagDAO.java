package com.epam.ukadzer.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.ukadzer.dao.TagDAO;
import com.epam.ukadzer.dao.UtilsService;
import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.Tag;

public class SQLTagDAO implements TagDAO {

	private static final String SQL_GET_BY_ID = "select t.tag_id, t.tag_name from tags t where tag_id=?";
	private static final String SQL_ADD = "insert into tags (tag_id,tag_name) "
			+ "values (tag_sequence.nextval,?)";
	private static final String SQL_UPDATE = "update tags set tag_name=? where tag_id=?";
	private static final String SQL_DELETE = "delete from tags where tag_id=?";
	private static final String SQL_GET_ALL = "select t.tag_id, t.tag_name  from tags t";
	private static final String SQL_DELETE_LINK_TO_NEWS = "delete from news_tags where tag_id=?";
	private static final String SQL_GET_ALL_FOR_NEWS = "select distinct t.tag_id, t.tag_name, nt.news_id  from tags t left outer join news_tags nt on t.tag_id=nt.tag_id where news_id=?";
	private static final String SQL_TAG_ID = "tag_ID";

	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public Tag getById(Long entityId) throws DAOException {
		Tag tag = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_GET_BY_ID);
			preparedStatement.setLong(1, entityId);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				tag = new Tag(resultSet.getLong(1), resultSet.getString(2));
			}

		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			UtilsService.closeConnection(connection, preparedStatement,
					resultSet, dataSource);
		}
		return tag;
	}

	public Long add(Tag entity) throws DAOException {
		String generatedColumns[] = { SQL_TAG_ID };
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_ADD,
					generatedColumns);
			preparedStatement.setString(1, entity.getName());
			preparedStatement.executeUpdate();
			ResultSet resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next()) {
				return resultSet.getLong(1);
			} else {
				throw new SQLException();
			}
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			UtilsService.closeConnection(connection, preparedStatement, null,
					dataSource);
		}
	}

	public void update(Tag entity) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_UPDATE);
			preparedStatement.setString(1, entity.getName());
			preparedStatement.setLong(2, entity.getId());
			preparedStatement.executeUpdate();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			UtilsService.closeConnection(connection, preparedStatement, null,
					dataSource);
		}
	}

	public void delete(Long entityId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_DELETE);
			preparedStatement.setLong(1, entityId);
			preparedStatement.executeUpdate();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			UtilsService.closeConnection(connection, preparedStatement, null,
					dataSource);
		}
	}

	public List<Tag> getAllTags() throws DAOException {
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		List<Tag> tags = new ArrayList<Tag>();
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(SQL_GET_ALL);
			while (resultSet.next()) {
				tags.add(new Tag(resultSet.getLong(1), resultSet.getString(2)));
			}

		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			UtilsService.closeConnection(connection, statement, resultSet,
					dataSource);
		}
		return tags;
	}

	@Override
	public void deleteLinkToNews(Long tagId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection
					.prepareStatement(SQL_DELETE_LINK_TO_NEWS);
			preparedStatement.setLong(1, tagId);
			preparedStatement.executeUpdate();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			UtilsService.closeConnection(connection, preparedStatement, null,
					dataSource);
		}

	}

	public Set<Tag> getAllForNews(Set<Tag> tagsIds, Long newsId)
			throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection
					.prepareStatement(SQL_GET_ALL_FOR_NEWS);
			preparedStatement.setLong(1, newsId);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				tagsIds.add(new Tag(resultSet.getLong(1), resultSet
						.getString(2)));
			}
			return tagsIds;

		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			UtilsService.closeConnection(connection, preparedStatement, null,
					dataSource);
		}

	}

}
