package com.epam.ukadzer.dao;

import java.util.List;
import java.util.Set;

import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.Comment;

public interface CommentDAO extends CRUD<Comment>{
	

	/**
     * Method deletes all comment of news with given Id
     * @param newsId id of news with comments are to be deleted
	 * @throws DAOException
     */
	public void deleteAllForNews(Long newsId) throws DAOException;
	
	/**
     * Method get all comment of news with given Id
     * @param newsId id of news with comments are to get
	 * @throws DAOException
     */
	public Set<Comment> getAllForNews(Long newsId) throws DAOException;
	
	/**
     * Method gets all the comments from db
     * @param all collection to get all object
     * @return collection with all objects
     */
    List<Comment> getAllComments() throws DAOException;
	
    
    /**
     * Method deletes all comment of news with given Id
     * @param newsId id of news with comments are to be deleted
	 * @throws DAOException
     */
	public void deleteAllForAllNews(List<Long> newsIds) throws DAOException;
	
}
