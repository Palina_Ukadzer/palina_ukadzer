package com.epam.ukadzer.service.exception;

public class ServiceException extends Exception{


	private static final long serialVersionUID = 1L;

	public ServiceException(){
		super();
	}
	
	public ServiceException(Exception e){
		super(e);
	}
	
	public ServiceException(String string){
		super(string);
	}
	
	public ServiceException(String s,Exception e){
		super(s, e);
	}
}
