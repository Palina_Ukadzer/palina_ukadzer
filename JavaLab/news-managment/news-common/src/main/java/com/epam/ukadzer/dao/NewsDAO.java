package com.epam.ukadzer.dao;

import java.util.List;
import java.util.Set;

import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.News;
import com.epam.ukadzer.entity.SearchCriteria;

public interface NewsDAO extends CRUD<News>{
	

	/**
     * Method bind tag to news by creating a new row in news_tags table
     * @param newsId the id of news
     * @param tagId the id of tag
	 * @throws DAOException
     */
	public void bindTag(Long newsId,Long tagId) throws DAOException;
	
	/**
     * Method bind set of tags to news by creating new rows in news_tags table
     * @param newsId the id of news
     * @param tagsIds set of ids of tags
	 * @throws DAOException
     */
	public void bindTags(Long newsId,Set<Long> tagsIds) throws DAOException;
	
	/**
     * Method bind author to news by creating a new row in news_authors table
     * @param newsId the id of news
     * @param authorId the id of tag
	 * @throws DAOException
     */
	public void bindAuthor(Long newsId,Long authorId) throws DAOException;
	
	/**
     * Method deletes news and author bond by deleting specific row in news_authors table
     * @param newsId the id of news
	 * @throws DAOException
     */
	public void deleteLinkToAuthor(Long newsId) throws DAOException;
	
	/**
     * Method deletes news and tags bond by deleting specific rows in news_authors table
     * @param newsId the id of news
	 * @throws DAOException
     */
	public void deleteLinkToTags(Long newsId) throws DAOException;
	
	/**
     * Method counts all existing news
     * @returns number of news
	 * @throws DAOException
     */
	public int countAll() throws DAOException;
	
	/**
     * Method counts all existing news which characteristics match given criteria
     * @param searchCriteria the search criteria
     * @returns number of news
	 * @throws DAOException
     */
	public int countAllWithCriteria(SearchCriteria searchCriteria) throws DAOException;
	
	/**
     * Method finds all existing news which characteristics match given criteria
     * @param all list where news should be put
     * @param searchCriteria the search criteria
     * @returns list of found news
	 * @throws DAOException
     */
	public List<News> findAllWithCriteria(List<News> all,SearchCriteria searchCriteria) throws DAOException;
	
	/**
     * Method finds a number of existing news which characteristics match given criteria from start to finish
     * @param all list where news should be put
     * @param searchCriteria the search criteria
     * @param start the first row to get
     * @param finish the last row to get
     * @returns list of found news
	 * @throws DAOException
     */
	public List<News> findSomeWithCriteria(List<News> all,SearchCriteria searchCriteria, int start, int finish) throws DAOException;
	
	/**
     * Method gets all news sorted by the number of comments
     * @param all list where news should be put
     * @returns list of found news
	 * @throws DAOException
     */
	public List<News> getAllSortedByComments(List<News> all) throws DAOException;
	
	/**
     * Method gets a next news
     * @param newsId current news id
     * @returns next news
	 * @throws DAOException
     */
	public Long getNextNewsId(Long newsId, SearchCriteria searchCriteria) throws DAOException;
	
	/**
     * Method gets a previous news id
     * @param newsId current news id
     * @returns next news
	 * @throws DAOException
     */
	public Long getPreviousNewsId(Long newsId, SearchCriteria searchCriteria) throws DAOException;
	
	/**
     * Method gets all the news from db
     * @param all collection to get all object
     * @return collection with all objects
     */
    public List<News> getAllNews() throws DAOException;
    
    /**
     * Method deletes all news with given id's from db
     * @param entityId id of entity to be deleted
     */    
    public void deleteNewsList(List<Long> newsIds) throws DAOException;
    
    
    /**
     * Method deletes news and author bond by deleting specific row in news_authors table
     * @param newsId the id of news
	 * @throws DAOException
     */
	public void deleteLinkToAuthorForAllNews(List<Long> newsId) throws DAOException;
	
	/**
     * Method deletes news and tags bond by deleting specific rows in news_authors table
     * @param newsId the id of news
	 * @throws DAOException
     */
	public void deleteLinkToTagsForAllNews(List<Long> newsIds) throws DAOException;
	

}
