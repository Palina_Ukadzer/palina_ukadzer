package com.epam.ukadzer.entity;

import javax.validation.constraints.Size;


public class Tag {
	
	@Size(min=1, max=30, message="")
	private String name;
	private Long id;
	
	public Tag() {}
	
	public Tag(Long id, String name) {
		this.id=id;
		this.name=name;
	}

	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tag other = (Tag) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	public String getName() {
		return name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
    public String toString() {
        return "Tag whith id=" + id + "; name=" + name;
    }
}

    
