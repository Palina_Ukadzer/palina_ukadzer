package com.epam.ukadzer.dao.impl;

import java.sql.Timestamp;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.dao.impl.SQLCommentDAO;
import com.epam.ukadzer.entity.Comment;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring-test.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, })
public class CommentDAOTest {

	private Long myMagicalDate = 1420059600000L;

	@Autowired
	SQLCommentDAO commentDAO;

	@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/comment/dataset.xml")
	@Test
	public void testGetById() throws DAOException {
		Comment comment = null;
		comment = commentDAO.getById(1L);
		Assert.assertEquals(null, comment.getText());
	}

	@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/comment/dataset.xml")
	@Test
	public void testGetByIdNull() throws DAOException {
		Comment comment = null;
		comment = commentDAO.getById(6L);
		Assert.assertNull(comment.getText());
	}

	@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/comment/dataset.xml")
	@ExpectedDatabase(value = "/com/epam/ukadzer/dao/impl/comment/expected-database-update.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	@Test
	public void testUpdate() throws DAOException {
		Comment comment = new Comment(1L, 1L, "comment_update", new Timestamp(
				myMagicalDate));
		commentDAO.update(comment);

	}

	@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/comment/dataset.xml")
	@ExpectedDatabase(value = "/com/epam/ukadzer/dao/impl/comment/expected-database-delete.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	@Test
	public void testDelete() throws DAOException {

		commentDAO.delete(1L);

	}

	@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/comment/dataset.xml")
	@ExpectedDatabase(value = "/com/epam/ukadzer/dao/impl/comment/expected-database-delete-all.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	@Test
	public void testDeleteAllForNews() throws DAOException {

		commentDAO.deleteAllForNews(1L);

	}

	@Test(expected = DAOException.class)
	public void testExceptionOnTooLongText() throws DAOException {
		Comment comment = new Comment(1L, 1L, "111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111", new Timestamp(
				myMagicalDate));
		commentDAO.add(comment);
	}

}
