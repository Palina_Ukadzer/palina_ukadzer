package com.epam.ukadzer.dao.impl;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.dao.impl.SQLNewsDAO;
import com.epam.ukadzer.entity.News;
import com.epam.ukadzer.entity.SearchCriteria;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring-test.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class })
public class NewsDAOTest {

	private Long myMagicalDate = 1420059600000L;

	@Autowired
	SQLNewsDAO newsDAO;

	@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/news/dataset.xml")
	@Test
	public void testGetById() throws DAOException {
		News news = null;
		news = newsDAO.getById(1L);
		Assert.assertEquals(news, null);
	}

	@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/news/dataset.xml")
	@Test
	public void testGetByIdNull() throws DAOException {
		News news = new News();
		news = newsDAO.getById(17L);
		Assert.assertNull(news);

	}

	@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/news/dataset.xml")
	@ExpectedDatabase(value = "/com/epam/ukadzer/dao/impl/news/expected-database-save.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	@Test
	public void testSave() throws DAOException {
		News news = new News(1L, "title_test", "shortText_test",
				"fullText_test", new Timestamp(myMagicalDate), new Date(
						myMagicalDate));
		newsDAO.add(news);

	}

	@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/news/dataset.xml")
	@ExpectedDatabase(value = "/com/epam/ukadzer/dao/impl/news/expected-database-update.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	@Test
	public void testUpdate() throws DAOException {
		News news = new News(1L, "title_update", "shortText_update",
				"fullText_update", new Timestamp(myMagicalDate), new Date(
						myMagicalDate));
		newsDAO.update(news);

	}

	@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/news/dataset.xml")
	@ExpectedDatabase(value = "/com/epam/ukadzer/dao/impl/news/expected-database-delete.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	@Test
	public void testDelete() throws DAOException {
		newsDAO.delete(1L);
	}

	@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/news/dataset.xml")
	@Test
	public void testFindAllWithCriteria() throws DAOException {
		List<News> list = new ArrayList<News>();
		Set<Long> tags = new HashSet<>();
		tags.add(2L);
		SearchCriteria searchCriteria = new SearchCriteria(1L, tags);
		list = newsDAO.findAllWithCriteria(list, searchCriteria);
		Assert.assertEquals(0, list.size());
	}

	@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/news/dataset.xml")
	@Test
	public void testCountAllWithCriteria() throws DAOException {
		Set<Long> tags = new HashSet<>();
		tags.add(2L);
		SearchCriteria searchCriteria = new SearchCriteria(1L, tags);
		int result = 0;
		result = newsDAO.countAllWithCriteria(searchCriteria);
		Assert.assertEquals(0, result);
	}

	@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/news/dataset.xml")
	@Test
	public void testgetSomeWithCriteria() throws DAOException {
		List<News> list = new ArrayList<News>();
		Set<Long> tags = new HashSet<>();
		tags.add(2L);
		tags.add(3L);
		tags.add(4L);
		SearchCriteria searchCriteria = new SearchCriteria(1L, tags);
		list = newsDAO.findSomeWithCriteria(list, searchCriteria, 2, 3);
		Assert.assertEquals(0, list.size());
	}

	@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/news/dataset.xml")
	@ExpectedDatabase(value = "/com/epam/ukadzer/dao/impl/news/expected-database-delete-link-author.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	@Test
	public void testDeleteLinkToAuthor() throws DAOException {
		newsDAO.deleteLinkToAuthor(1L);
	}

	@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/news/dataset.xml")
	@ExpectedDatabase(value = "/com/epam/ukadzer/dao/impl/news/expected-database-delete-link-tags.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	@Test
	public void testDeleteLinkToTags() throws DAOException {
		newsDAO.deleteLinkToTags(1L);
	}
	
	@Test(expected = DAOException.class)
	public void testExceptionOnTooLongTitle() throws DAOException {
		News news = new News(1L, "11111111111111111111111111111111", "shortText_test",
				"fullText_test", new Timestamp(myMagicalDate), new Date(
						myMagicalDate));
		newsDAO.add(news);
	}
	
	@Test(expected = DAOException.class)
	public void testExceptionOnTooLongShort() throws DAOException {
		News news = new News(1L, "1", "111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111",
				"fullText_test", new Timestamp(myMagicalDate), new Date(
						myMagicalDate));
		newsDAO.add(news);
	}
	
}
