package com.epam.ukadzer.service.impl;

import static org.mockito.Mockito.verify;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.ukadzer.dao.TagDAO;
import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.Tag;
import com.epam.ukadzer.service.exception.ServiceException;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceTest {

	private Long myMagicalId = 3L;

	@Mock
	private TagDAO tagDAO;

	@InjectMocks
	private TagServiceImpl tagService;

	@Test
	public void shouldReturnAddedTagId() throws DAOException, ServiceException {
		Tag tag = new Tag(1L, "tag1");
		Mockito.when(tagDAO.add(tag)).thenReturn(myMagicalId);
		Long result = tagService.add(tag);
		verify(tagDAO).add(tag);
		Assert.assertEquals(myMagicalId, result);

	}

}
