package com.epam.ukadzer.service.impl;

import static org.mockito.Mockito.*;

import java.sql.Timestamp;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.ukadzer.dao.CommentDAO;
import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.Comment;
import com.epam.ukadzer.service.exception.ServiceException;
import com.epam.ukadzer.service.impl.CommentServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class CommentServiceTest {

	private Long myMagicalId = 12L;
	private Long myMagicalTime = 1420070400L;

	@Mock
	private CommentDAO commentDAO;

	@InjectMocks
	private CommentServiceImpl commentService;

	@Test
	public void shouldReturnAddedAuthorId() throws DAOException, ServiceException {
		Comment comment = new Comment(myMagicalId, myMagicalId, "comment1",
				new Timestamp( myMagicalTime));
		Mockito.when(commentDAO.add(comment)).thenReturn(myMagicalId);
		Long result = commentService.add(comment);
		verify(commentDAO, times(1)).add(comment);
		Assert.assertEquals(myMagicalId, result);

	}

	@Test
	public void shouldCallDelete() throws ServiceException, DAOException {
		commentService.delete(myMagicalId);
		verify(commentDAO, times(1)).delete(myMagicalId);
		verifyNoMoreInteractions(commentDAO);

	}
	
	@Test
	public void shouldCallDeleteAllForNews() throws ServiceException, DAOException{
		commentService.deleteAllForNews(myMagicalId);
		verify(commentDAO, times(1)).deleteAllForNews(myMagicalId);;
		verifyNoMoreInteractions(commentDAO);
	}

}
