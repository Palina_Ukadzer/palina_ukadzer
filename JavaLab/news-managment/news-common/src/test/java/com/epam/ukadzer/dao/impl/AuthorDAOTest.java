package com.epam.ukadzer.dao.impl;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.dao.impl.SQLAuthorDAO;
import com.epam.ukadzer.entity.Author;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring-test.xml" })
public class AuthorDAOTest {

	@Autowired
	SQLAuthorDAO authorDAO;

	@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/author/dataset.xml")
	@Test
	public void testGetById() {
		Author author = null;
		try {
			author = authorDAO.getById(1L);
		} catch (DAOException e) {
			Assert.fail();
		}
		Assert.assertEquals("Palina Ukadzer", author.getName());
	}

	@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/author/dataset.xml")
	@Test
	public void testGetByIdNull() {
		Author author = null;
		try {
			author = authorDAO.getById(6L);
		} catch (DAOException e) {
			Assert.fail();
		}
		Assert.assertNull(author);
		;
	}

	@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/author/dataset.xml")
	@ExpectedDatabase(value = "/com/epam/ukadzer/dao/impl/author/expected-database-save.xml")
	@Test
	public void testSave() throws DAOException {
		Author author = new Author(1L, "qqq", null);
		authorDAO.add(author);

	}

	@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/author/dataset.xml")
	@ExpectedDatabase(value = "/com/epam/ukadzer/dao/impl/author/expected-database-update.xml")
	@Test
	public void testUpdate() throws DAOException {
		Author author = new Author(1L, "Palina Ukadzer", null);
		authorDAO.update(author);

	}
	
	@Test(expected=DAOException.class)
	public void testExceptionOnTooLongText() throws DAOException {
		Author author = new Author(1L, "1234567890123456789012345678901234567890", null);
		authorDAO.add(author);

	}

}
