package com.epam.newsmanagment.controller;

import java.util.HashMap;

import javax.servlet.http.HttpSession;

import com.epam.newsmanagment.controller.command.Command;
import com.epam.newsmanagment.controller.command.GoBackToListCommand;
import com.epam.newsmanagment.controller.command.PostCommentCommand;
import com.epam.newsmanagment.controller.command.ResetSearchCriteriaCommand;
import com.epam.newsmanagment.controller.command.SeeNewsListCommand;
import com.epam.newsmanagment.controller.command.SeeNextNewsCommand;
import com.epam.newsmanagment.controller.command.SeePreviousNewsCommand;
import com.epam.newsmanagment.controller.command.SeeSingleNewsCommand;


public class RequestHelper {
	 private static RequestHelper instance = null;
	    private HashMap<String, Command> commands = new HashMap<String, Command>();


	    /**
	     * Fills 'commands' HashMap with name of uri as a 'key' parameter
	     * and command which should be executed for given uri as 'value' parameter
	     */
	    public RequestHelper() {
	        super();
	        commands.put("SeeNewsList", new SeeNewsListCommand());
	        commands.put("SeeSingleNews", new SeeSingleNewsCommand());
	        commands.put("SeeNextNews", new SeeNextNewsCommand());
	        commands.put("GoBackToList", new GoBackToListCommand());
	        commands.put("SeePreviousNews", new SeePreviousNewsCommand());
	        commands.put("PostComment", new PostCommentCommand());
	        commands.put("ResetSearchCriteria", new ResetSearchCriteriaCommand());
	    }

	    /**
	     * Returns instance
	     */
	    public static RequestHelper getInstance() {
	        if (instance == null) {
	            instance = new RequestHelper();
	        }
	        return instance;
	    }

	    /**
	     * Checks if access for current user is allowed and if so
	     * returns the command, that is to be executed for incoming uri
	     */
	    public Command getCommand(String commandName, HttpSession session) {
	        Command command = commands.get(commandName);
	        return command;
	    }

}
