package com.epam.newsmanagment.controller.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.epam.newsmanagment.controller.exception.ControllerException;

public interface Command {
	
	    public String execute(HttpServletRequest request, HttpSession session) throws ControllerException;
	    
	    public boolean isRedirected();
	    


}
