package com.epam.newsmanagment.utilits;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class ServiceManager {

	private static ApplicationContext applicationContext;
	
	public static void init() {
		applicationContext =  new ClassPathXmlApplicationContext("spring1.xml");
	}
	
	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}

}
