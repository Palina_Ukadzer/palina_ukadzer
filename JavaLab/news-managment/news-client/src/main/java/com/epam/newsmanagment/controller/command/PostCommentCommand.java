package com.epam.newsmanagment.controller.command;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.epam.newsmanagment.controller.exception.ControllerException;
import com.epam.newsmanagment.utilits.ResourceManager;
import com.epam.newsmanagment.utilits.ServiceManager;
import com.epam.newsmanagment.utilits.DataForRequestBuilder;
import com.epam.ukadzer.entity.Comment;
import com.epam.ukadzer.entity.ComplexNews;
import com.epam.ukadzer.service.CommentService;
import com.epam.ukadzer.service.ComplexNewsService;
import com.epam.ukadzer.service.NewsService;
import com.epam.ukadzer.service.exception.ServiceException;

public class PostCommentCommand implements Command {
	
	private ResourceManager resourceManager=ResourceManager.INSTANCE; 

	@Override
	public String execute(HttpServletRequest request, HttpSession session)
			throws ControllerException {
		NewsService newsService = (NewsService) ServiceManager
				.getApplicationContext().getBean("newsService");
		CommentService commentService = (CommentService) ServiceManager
				.getApplicationContext().getBean("commentService");
		ComplexNewsService complexNewsService = (ComplexNewsService) ServiceManager
				.getApplicationContext().getBean("complexNewsService");
		try {
			Long newsId = Long.parseLong(request.getParameter("newsId"));
			if (request.getParameter("commentText") != null && request.getParameter("commentText").length()<100) {
				Comment comment = new Comment(1L, newsId,
						request.getParameter("commentText"), new Date());
				commentService.add(comment);
			}
			ComplexNews news = complexNewsService.get(newsId);
			request.setAttribute("news", news);
			request.setAttribute("nextNewsId", DataForRequestBuilder.setNextNewsId(newsId, newsService, session));
			request.setAttribute("previousNewsId", DataForRequestBuilder.setPrevNewsId(newsId, newsService, session));	
		} catch (ServiceException e) {
			throw new ControllerException();
		}
		return resourceManager.getString("fullNameForRedirectToSinglrNews")+request.getParameter("newsId");
	}
	
	public boolean isRedirected(){
		return true;
	}
}
