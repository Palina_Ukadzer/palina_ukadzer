package com.epam.newsmanagment.utilits;

import java.util.Locale;
import java.util.ResourceBundle;

public enum ResourceManager {
    INSTANCE;
    private ResourceBundle resourceBundle;
    private final String resourceName = "constants";

    /**
     * The default constructor
     */
    private ResourceManager() {
        resourceBundle = ResourceBundle.getBundle(resourceName, Locale.getDefault());
    }

    /**
     * Changes locale of ResourceBundle
     */
    public void changeResource(Locale locale) {
        resourceBundle = ResourceBundle.getBundle(resourceName, locale);
    }

    /**
     * Gets the string that is mapped to the given key
     * @param key the key of wanted string
     * @return the string mapped to the key
     */
    public String getString(String key) {
        return resourceBundle.getString(key);
    }
}