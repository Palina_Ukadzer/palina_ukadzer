package com.epam.newsmanagment.controller.command;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.epam.newsmanagment.controller.exception.ControllerException;
import com.epam.newsmanagment.utilits.ResourceManager;
import com.epam.newsmanagment.utilits.ServiceManager;
import com.epam.newsmanagment.utilits.DataForRequestBuilder;
import com.epam.ukadzer.entity.Comment;
import com.epam.ukadzer.entity.ComplexNews;
import com.epam.ukadzer.service.ComplexNewsService;
import com.epam.ukadzer.service.NewsService;
import com.epam.ukadzer.service.exception.ServiceException;

public class SeePreviousNewsCommand implements Command{
	
	private ResourceManager resourceManager=ResourceManager.INSTANCE; 

	@Override
	public String execute(HttpServletRequest request, HttpSession session)
			throws ControllerException {
		NewsService newsService = (NewsService) ServiceManager.getApplicationContext().getBean("newsService");
		ComplexNewsService complexNewsService = (ComplexNewsService) ServiceManager.getApplicationContext().getBean("complexNewsService");
		try {
			if (request.getParameter("previousNewsId") != null) {
				Long previousNewsId = Long.parseLong(request.getParameter("previousNewsId"));
				ComplexNews news = complexNewsService.get(previousNewsId);
				if (news==null){
					request.setAttribute("newsId", previousNewsId);
					return resourceManager.getString("newsNotFound");
				}
				request.setAttribute("news", news);
				Set<Comment> comments = new HashSet<>();
				comments = news.getComments();
				request.setAttribute("comments", comments);

				request.setAttribute("nextNewsId", DataForRequestBuilder.setNextNewsId(previousNewsId, newsService, session));
				request.setAttribute("previousNewsId", DataForRequestBuilder.setPrevNewsId(previousNewsId, newsService, session));
			}

		} catch (ServiceException e) {
			throw new ControllerException();
		}

		return resourceManager.getString("singleNews");
	}
	
	public boolean isRedirected(){
		return false;
	}

}
