package com.epam.newsmanagment.controller.command;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;



import com.epam.newsmanagment.controller.exception.ControllerException;
import com.epam.newsmanagment.utilits.ServiceManager;
import com.epam.newsmanagment.utilits.DataForRequestBuilder;
import com.epam.newsmanagment.utilits.ResourceManager;
import com.epam.ukadzer.entity.ComplexNews;
import com.epam.ukadzer.entity.SearchCriteria;
import com.epam.ukadzer.service.AuthorService;
import com.epam.ukadzer.service.ComplexNewsService;
import com.epam.ukadzer.service.NewsService;
import com.epam.ukadzer.service.TagService;
import com.epam.ukadzer.service.exception.ServiceException;

public class GoBackToListCommand implements Command {
	
	 private ResourceManager resourceManager=ResourceManager.INSTANCE; 


	@SuppressWarnings("unchecked")
	@Override
	public String execute(HttpServletRequest request, HttpSession session)
			throws ControllerException {
		AuthorService authorService = (AuthorService) ServiceManager.getApplicationContext().getBean("authorService");
		TagService tagService = (TagService) ServiceManager.getApplicationContext().getBean("tagService");
		NewsService newsService = (NewsService) ServiceManager.getApplicationContext().getBean("newsService");
		ComplexNewsService complexNewsService = (ComplexNewsService) ServiceManager.getApplicationContext().getBean("complexNewsService");
		try {
			int page = 1;
			
			request.setAttribute("authors", authorService.getAllAuthors());
			request.setAttribute("tags", tagService.getAllTags());
			
			List<ComplexNews> news = new ArrayList<>();
			SearchCriteria searchCriteria = new SearchCriteria();
			Set<Long> searchedTags = new HashSet<>();
			if (session.getAttribute("authorId") != null && !session.getAttribute("authorId").equals("0")) {
				searchCriteria.setAuthorId((Long) session.getAttribute("authorId"));
				request.setAttribute("authorId",(Long) session.getAttribute("authorId"));
			}
			if (session.getAttribute("searchedTagsForCriteria") != null) {
				searchedTags = (Set<Long>) session.getAttribute("searchedTagsForCriteria");
			}
			request.setAttribute("searchedTags", DataForRequestBuilder.mapFromSet(searchedTags));
			
			searchCriteria.setTagsIds(searchedTags);
			news = complexNewsService.getSomeWithCriteria( searchCriteria,
					page * 3 - 2, page * 3);
			request.setAttribute("news", news);
			
			request.setAttribute("pagesNum", DataForRequestBuilder.pageNum(newsService, searchCriteria));
			

		} catch (ServiceException e) {
			throw new ControllerException();
		}
		return resourceManager.getString("newsList");
	}
	
	public boolean isRedirected(){
		return false;
	}

}
