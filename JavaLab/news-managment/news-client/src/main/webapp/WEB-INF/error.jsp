<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<c:set var="language"
	value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
	scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="message" />
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>News</title>

<link href="<c:url value="/resources/myClasses.css" />" type="text/css"
	rel="stylesheet">
<script type="text/javascript">
	function validate_form() {
		if (document.getElementById("text").value.length < 1
				|| document.getElementById("text").value.length > 30) {
			alert("WRONG");
			return false;
		}
		return true;
	}
</script>
</head>
<body>
	<div class="panel panel-default">

		<div class="panel panel-heading">
			<h1>Mews portal</h1>
			<form action="/news-client">
				<input type="hidden" name="command" value="SeeSingleNews"> <input
					type="hidden" name="newsId" value="${newsId}">

				<button type="submit" id="language" name="language" value="ru">Рус</button>
				<button type="submit" id="language" name="language" value="en">En</button>
			</form>
		</div>
		<div class="dotted-border">
			<div class="col-sm-9">
				<form action="Controller">
					<input type="hidden" name="from" value="SingleNews"> <input
						type="hidden" name="command" value="GoBackToList">
					<div class="backButton">
						<button type="submit" class="btn btn-primary">
							<fmt:message key="back" />
						</button>
					</div>
				</form>
				<div class="col-sm-12">A weird mistake</div>
			</div>
		</div>
	</div>
</body>
</html>