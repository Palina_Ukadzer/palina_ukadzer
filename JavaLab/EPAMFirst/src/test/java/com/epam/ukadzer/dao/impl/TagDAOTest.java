package com.epam.ukadzer.dao.impl;


import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.dao.impl.SQLTagDAO;
import com.epam.ukadzer.entity.Tag;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring-test.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
public class TagDAOTest{
	@Autowired
	SQLTagDAO tagDAO;
	
	@DatabaseSetup(value="/com/epam/ukadzer/dao/impl/tag/dataset.xml")
	@Test
	public void testGetById(){
		Tag tag = null;
		try {
			tag = tagDAO.getById(1L);
		} catch (DAOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Assert.assertEquals("Tag1",tag.getName());
	}
	
	
	@DatabaseSetup(value="/com/epam/ukadzer/dao/impl/tag/dataset.xml")
	@Test
	public void testGetByIdNull(){
		Tag tag = null;
		try {
			tag = tagDAO.getById(6L);
		} catch (DAOException e) {
			Assert.fail();
		}
		Assert.assertNull(tag);
	}
	
	@DatabaseSetup(value="/com/epam/ukadzer/dao/impl/tag/dataset.xml")
	@ExpectedDatabase(value="/com/epam/ukadzer/dao/impl/tag/expected-database-save.xml",
	assertionMode=DatabaseAssertionMode.NON_STRICT)
	@Test
	public void testSave(){
		Tag tag = new Tag(1L, "tag_new");
		try {
			tagDAO.add(tag);
		} catch (DAOException e) {
			Assert.fail();
		}
	}
	
	@DatabaseSetup(value="/com/epam/ukadzer/dao/impl/tag/dataset.xml")
	@ExpectedDatabase(value="/com/epam/ukadzer/dao/impl/tag/expected-database-update.xml",
	assertionMode=DatabaseAssertionMode.NON_STRICT)
	@Test
	public void testUpdate(){
		Tag tag = new Tag(1L, "tag_update");
		try {
			tagDAO.update(tag);
		} catch (DAOException e) {
			Assert.fail();
		}
		
	}
	
	@DatabaseSetup(value="/com/epam/ukadzer/dao/impl/tag/dataset.xml")
	@ExpectedDatabase(value="/com/epam/ukadzer/dao/impl/tag/expected-database-delete.xml",
	assertionMode=DatabaseAssertionMode.NON_STRICT)
	@Test
	public void testDelete(){
		try {
			tagDAO.delete(1L);
		} catch (DAOException e) {
			Assert.fail();
		}
		
	}
	
	@DatabaseSetup(value="/com/epam/ukadzer/dao/impl/tag/dataset.xml")
	@ExpectedDatabase(value="/com/epam/ukadzer/dao/impl/tag/expected-database-delete-link-news.xml",
	assertionMode=DatabaseAssertionMode.NON_STRICT)
	@Test
	public void testDeleteLinkToNews(){
		try {
			tagDAO.deleteLinkToNews(2L);
		} catch (DAOException e) {
			Assert.fail();
		}
		
	}

}
