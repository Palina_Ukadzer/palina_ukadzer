package com.epam.ukadzer.service.impl;

import static org.mockito.Mockito.verify;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.ukadzer.entity.Author;
import com.epam.ukadzer.entity.ComplexNews;
import com.epam.ukadzer.entity.News;
import com.epam.ukadzer.entity.Tag;
import com.epam.ukadzer.service.CommentService;
import com.epam.ukadzer.service.NewsService;
import com.epam.ukadzer.service.exception.ServiceException;
import com.epam.ukadzer.service.impl.ComplexNewsServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class ComplexNewsServiceTest {

	private Long myMagicalId = 3L;
	private Long myMagicalTime = 1420070400L;

	@Mock
	private NewsService newsService;
	
	@Mock
	private CommentService commentService;

	@InjectMocks
	private ComplexNewsServiceImpl complexNewsService;

	@Test
	public void shouldCallTwoMethods() throws Exception {
		Mockito.when(newsService.addNews(new News(myMagicalId,"title_test", "shortText_test", "fullText_test", new Timestamp(myMagicalTime), 
				new Date(myMagicalTime)))).thenReturn(myMagicalId);
		Set<Tag> tags = new HashSet<Tag>();
		ComplexNews complexNews = new ComplexNews(new News(myMagicalId,
				"title_test", "shortText_test", "fullText_test", new Timestamp(myMagicalTime), 
				new Date(myMagicalTime)), new Author(myMagicalId, "palina", null), tags);
		complexNewsService.add(complexNews);
		verify(newsService).addNews(complexNews.getNews());
		verify(newsService).bindAuthor(myMagicalId,
				complexNews.getAuthor().getId());

	}
	
	@Test
	public void shouldCallThreeMethods() throws Exception {
		Mockito.when(newsService.addNews(new News(myMagicalId,"title_test", "shortText_test", "fullText_test", new Timestamp(myMagicalTime), 
				new Date(myMagicalTime)))).thenReturn(myMagicalId);
		Set<Tag> tags = new HashSet<Tag>();
		tags.add(new Tag(1L, "tag1"));
		ComplexNews complexNews = new ComplexNews(new News(myMagicalId,
				"title_test", "shortText_test", "fullText_test", new Timestamp(
						1420070400), new Date(1420070400)), new Author(
				myMagicalId, "palina", null), tags);
		complexNewsService.add(complexNews);
		verify(newsService).addNews(complexNews.getNews());
		verify(newsService).bindAuthor(complexNews.getNews().getId(),
				complexNews.getAuthor().getId());
		Set <Long> list = new HashSet<Long>();
		list.add(1L);
		verify(newsService).bindTags(myMagicalId, list);
	}
	
	@Test
	public void shouldCallFourDifferentDeleteMethods() throws ServiceException{
		Set<Tag> tags = new HashSet<Tag>();
		ComplexNews complexNews = new ComplexNews(new News(myMagicalId,
				"title_test", "shortText_test", "fullText_test", new Timestamp(myMagicalTime), 
				new Date(myMagicalTime)), new Author(myMagicalId, "palina", null), tags);
		complexNewsService.delete(complexNews);
		verify(newsService).deleteLinkToAuthor(myMagicalId);
		verify(newsService).deleteLinkToTags(myMagicalId);
		verify(commentService).deleteAllForNews(myMagicalId);
		verify(newsService).delete(myMagicalId);
		
		
	}
}
