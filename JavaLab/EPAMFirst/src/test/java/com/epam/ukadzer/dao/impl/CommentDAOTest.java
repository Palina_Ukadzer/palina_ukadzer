package com.epam.ukadzer.dao.impl;

import java.sql.Timestamp;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.dao.impl.SQLCommentDAO;
import com.epam.ukadzer.entity.Comment;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring-test.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
public class CommentDAOTest{
	
	private Long myMagicalDate = 1420059600000L;
	
	@Autowired
	SQLCommentDAO commentDAO;
	
	@DatabaseSetup(value="/com/epam/ukadzer/dao/impl/comment/dataset.xml")
	@Test
	public void testGetById(){
		Comment comment = null;
		try {
			comment = commentDAO.getById(1L);
		} catch (DAOException e) {
			Assert.fail();
		}
		Assert.assertEquals("comment1",comment.getText());
	}
	
	
	@DatabaseSetup(value="/com/epam/ukadzer/dao/impl/comment/dataset.xml")
	@Test
	public void testGetByIdNull(){
		Comment comment = null;
		try {
			comment = commentDAO.getById(6L);
		} catch (DAOException e) {
			Assert.fail();
		}
		Assert.assertNull(comment.getText());
	}
	
	@DatabaseSetup(value="/com/epam/ukadzer/dao/impl/comment/dataset.xml")
	@ExpectedDatabase(value="/com/epam/ukadzer/dao/impl/comment/expected-database-save.xml",
	assertionMode=DatabaseAssertionMode.NON_STRICT)
	@Test
	public void testSave(){
		Comment comment = new Comment(1L,1L,"comment_save", new Timestamp(myMagicalDate));
		try {
			commentDAO.add(comment);
		} catch (DAOException e) {
			Assert.fail();
		}
	}
	
	@DatabaseSetup(value="/com/epam/ukadzer/dao/impl/comment/dataset.xml")
	@ExpectedDatabase(value="/com/epam/ukadzer/dao/impl/comment/expected-database-update.xml",
	assertionMode=DatabaseAssertionMode.NON_STRICT)
	@Test
	public void testUpdate(){
		Comment comment = new Comment(1L,1L,"comment_update", new Timestamp(myMagicalDate));
		try {
			commentDAO.update(comment);
		} catch (DAOException e) {
			Assert.fail();
		}
		
	}
	
	@DatabaseSetup(value="/com/epam/ukadzer/dao/impl/comment/dataset.xml")
	@ExpectedDatabase(value="/com/epam/ukadzer/dao/impl/comment/expected-database-delete.xml",
	assertionMode=DatabaseAssertionMode.NON_STRICT)
	@Test
	public void testDelete(){
		try {
			commentDAO.delete(1L);
		} catch (DAOException e) {
			Assert.fail();
		}
		
	}
	
	@DatabaseSetup(value="/com/epam/ukadzer/dao/impl/comment/dataset.xml")
	@ExpectedDatabase(value="/com/epam/ukadzer/dao/impl/comment/expected-database-delete-all.xml",
	assertionMode=DatabaseAssertionMode.NON_STRICT)
	@Test
	public void testDeleteAllForNews(){
		try {
			commentDAO.deleteAllForNews(1L);
		} catch (DAOException e) {
			Assert.fail();
		}
		
	}
	

}
