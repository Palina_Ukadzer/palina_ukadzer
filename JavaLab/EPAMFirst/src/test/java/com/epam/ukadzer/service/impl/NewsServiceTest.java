package com.epam.ukadzer.service.impl;

import static org.mockito.Mockito.*;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.ukadzer.dao.NewsDAO;
import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.News;
import com.epam.ukadzer.entity.SearchCriteria;
import com.epam.ukadzer.entity.Tag;
import com.epam.ukadzer.service.exception.ServiceException;
import com.epam.ukadzer.service.impl.NewsServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class NewsServiceTest {

	private Long myMagicalId = 1L;
	private Long myMagicalTime = 1420070400L;

	@Mock
	private NewsDAO newsDAO;

	@InjectMocks
	private NewsServiceImpl newsService;

	@Test
	public void shouldCallEdit() throws ServiceException, DAOException {
		News news = new News(myMagicalId, "title_test", "shortText_test",
				"fullText_test", new Timestamp(myMagicalTime),
				new Date(myMagicalTime));
		newsService.edit(news);
		verify(newsDAO).update(news);
		verifyNoMoreInteractions(newsDAO);

	}

	@Test
	public void testGetNewsList() throws ServiceException, DAOException {
		Collection<News> news = new ArrayList<News>();
		news.add(new News(myMagicalId, "title_test", "shortText_test",
				"fullText_test", new Timestamp(myMagicalTime),
				new Date(myMagicalTime)));
		Collection<News> newNews = new ArrayList<>();
		when(newsDAO.getAll(newNews)).thenReturn(news);
		newNews = newsDAO.getAll(newNews);
		Assert.assertEquals(news, newNews);
		 newNews = new ArrayList<News>();
		verify(newsDAO, times(1)).getAll(newNews);
		verifyNoMoreInteractions(newsDAO);

	}

	@Test
	public void shouldCallAddTagToNews() throws ServiceException, DAOException {
		News news = new News(myMagicalId, "title_test", "shortText_test",
				"fullText_test", new Timestamp(myMagicalTime),
				new Date(myMagicalTime));
		Tag tag = new Tag(myMagicalId, "tag1");
		newsService.addTagToNews(news, tag);
		verify(newsDAO, times(1)).bindTag(news.getId(), tag.getId());
		verifyNoMoreInteractions(newsDAO);

	}

	@Test
	public void shouldCallAddTagsToNews() throws ServiceException, DAOException {
		News news = new News(myMagicalId, "title_test", "shortText_test",
				"fullText_test", new Timestamp(myMagicalTime),
				new Date(myMagicalTime));
		Set<Long> tags = new HashSet<>();
		tags.add(myMagicalId);
		newsService.bindTags(news.getId(), tags);
		verify(newsDAO).bindTags(news.getId(), tags);

	}

	@Test
	public void shouldCallGetCommSortedNewsList() throws ServiceException,
			DAOException {
		News news = new News(myMagicalId, "title_test", "shortText_test",
				"fullText_test", new Timestamp(myMagicalTime),
				new Date(myMagicalTime));
		List<News> newsList = new ArrayList<News>();
		newsList.add(news);
		newsService.getCommSortedNewsList(newsList);
		verify(newsDAO).getAllSortedByComments(newsList);

	}

	@Test
	public void shouldReturnNumberOfNews() throws ServiceException, DAOException {
		Mockito.when(newsDAO.countAll()).thenReturn(11);
		int result = newsService.countAll();
		verify(newsDAO).countAll();
		Assert.assertEquals(11, result);

	}

	@Test
	public void shouldCallCountAllWithCriteria() throws ServiceException,
			DAOException {
		Set<Long> tags = new HashSet<>();
		tags.add(myMagicalId);
		SearchCriteria searchCriteria = new SearchCriteria(myMagicalId, tags);
		Mockito.when(newsDAO.countAllWithCriteria(searchCriteria)).thenReturn(
				11);
		int result = newsService.countAllWithCriteria(searchCriteria);
		verify(newsDAO).countAllWithCriteria(searchCriteria);
		Assert.assertEquals(11, result);

	}

	@Test
	public void shouldCallGetAllWithCriteria() throws ServiceException, DAOException {
		List<News> list = new ArrayList<News>();
		Set<Long> tags = new HashSet<>();
		tags.add(myMagicalId);
		SearchCriteria searchCriteria = new SearchCriteria(myMagicalId, tags);
		newsService.gettAllWithCriteria(list, searchCriteria);
		verify(newsDAO).findAllWithCriteria(list, searchCriteria);

	}

	@Test
	public void shouldCallGetSomeWithCriteria() throws DAOException, ServiceException {
		List<News> list = new ArrayList<News>();
		Set<Long> tags = new HashSet<>();
		tags.add(myMagicalId);
		SearchCriteria searchCriteria = new SearchCriteria(myMagicalId, tags);
		newsService.getSomeWithCriteria(list, searchCriteria, 1, 2);
		verify(newsDAO).findSomeWithCriteria(list, searchCriteria, 1, 2);

	}
	
	@Test
	public void shouldCallDeleteLinkToAuthor() throws ServiceException, DAOException {
		newsService.deleteLinkToAuthor(myMagicalId);
		verify(newsDAO).deleteLinkToAuthor(myMagicalId);
		verifyNoMoreInteractions(newsDAO);

	}
	
	@Test
	public void shouldCallDeleteLinkToTags() throws ServiceException, DAOException {
		newsService.deleteLinkToTags(myMagicalId);
		verify(newsDAO).deleteLinkToTags(myMagicalId);
		verifyNoMoreInteractions(newsDAO);

	}
	
	@Test
	public void shouldCallBindAuthor() throws ServiceException, DAOException {
		newsService.bindAuthor(myMagicalId, myMagicalId);
		verify(newsDAO).bindAuthor(myMagicalId, myMagicalId);
		verifyNoMoreInteractions(newsDAO);

	}
	
	@Test
	public void shouldCallBindTags() throws ServiceException, DAOException {
		Set<Long> ids = new HashSet<Long>();
		newsService.bindTags(myMagicalId, ids);
		verify(newsDAO).bindTags(myMagicalId, ids);
		verifyNoMoreInteractions(newsDAO);

	}
	
	@Test
	public void shouldCallAddNews() throws ServiceException, DAOException {
		News news = new News(myMagicalId, "title_test", "shortText_test",
				"fullText_test", new Timestamp(myMagicalTime),
				new Date(myMagicalTime));
		newsService.addNews(news);
		verify(newsDAO).add(news);
		verifyNoMoreInteractions(newsDAO);

	}
	
	@Test
	public void shouldCallDelete() throws ServiceException, DAOException {
		newsService.delete(myMagicalId);
		verify(newsDAO).delete(myMagicalId);
		verifyNoMoreInteractions(newsDAO);

	}
	
	

}
