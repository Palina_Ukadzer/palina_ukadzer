package com.epam.ukadzer.dao.impl;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.dao.impl.SQLNewsDAO;
import com.epam.ukadzer.entity.News;
import com.epam.ukadzer.entity.SearchCriteria;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring-test.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
public class NewsDAOTest{
	
	private Long myMagicalDate = 1420059600000L;

	@Autowired
	SQLNewsDAO newsDAO;
	
	@DatabaseSetup(value="/com/epam/ukadzer/dao/impl/news/dataset.xml")
	@Test
	public void testGetById(){
		News news = new News();;
		try {
			news = newsDAO.getById(1L);
		} catch (DAOException e) {
			Assert.fail();
		}
		Assert.assertEquals(news.getTitle(), "title1");
	}
	
	
	@DatabaseSetup(value="/com/epam/ukadzer/dao/impl/news/dataset.xml")
	@Test
	public void testGetByIdNull(){
		News news = new News();
		try {
			news = newsDAO.getById(17L);
		} catch (DAOException e) {
			Assert.fail();
		}
		Assert.assertNull(news);;
	}
	
	@DatabaseSetup(value="/com/epam/ukadzer/dao/impl/news/dataset.xml")
	@ExpectedDatabase(value="/com/epam/ukadzer/dao/impl/news/expected-database-save.xml",
	assertionMode=DatabaseAssertionMode.NON_STRICT)
	@Test
	public void testSave(){
		News news = new News(1L, "title_test", "shortText_test", "fullText_test",
				new Timestamp(myMagicalDate), new Date(myMagicalDate));
		try {
			newsDAO.add(news);
		} catch (DAOException e) {
			Assert.fail();
		} 
	}
	
	
	@DatabaseSetup(value="/com/epam/ukadzer/dao/impl/news/dataset.xml")
	@ExpectedDatabase(value="/com/epam/ukadzer/dao/impl/news/expected-database-update.xml",
	assertionMode=DatabaseAssertionMode.NON_STRICT)
	@Test
	public void testUpdate(){
		News news = new News(1L, "title_update", "shortText_update", "fullText_update",
				new Timestamp(myMagicalDate), new Date(myMagicalDate));
		try {
			newsDAO.update(news);
		} catch (DAOException e) {
			Assert.fail();
		}
		
	}
	
	@DatabaseSetup(value="/com/epam/ukadzer/dao/impl/news/dataset.xml")
	@ExpectedDatabase(value="/com/epam/ukadzer/dao/impl/news/expected-database-delete.xml",
	assertionMode=DatabaseAssertionMode.NON_STRICT)
	@Test
	public void testDelete(){
		try {
			newsDAO.delete(1L);
		} catch (DAOException e) {
			Assert.fail();
		}
		
	}
	
	
	@DatabaseSetup(value="/com/epam/ukadzer/dao/impl/news/dataset.xml")
	@Test
	public void testCountAll(){
		int count = 0;
		try {
			count = newsDAO.countAll();
		} catch (DAOException e) {
			Assert.fail();
		}
		Assert.assertEquals(6, count);
	}
	
	@DatabaseSetup(value="/com/epam/ukadzer/dao/impl/news/dataset.xml")
	@ExpectedDatabase(value="/com/epam/ukadzer/dao/impl/news/expected-database-addtag.xml",
	assertionMode=DatabaseAssertionMode.NON_STRICT)
	@Test
	public void testAddTag(){
		try {
			newsDAO.bindTag(1L, 1L);
		} catch (DAOException e) {
			Assert.fail();
		}
		
	}
	
	@DatabaseSetup(value="/com/epam/ukadzer/dao/impl/news/dataset.xml")
	@ExpectedDatabase(value="/com/epam/ukadzer/dao/impl/news/expected-database-addtags.xml",
	assertionMode=DatabaseAssertionMode.NON_STRICT)
	@Test
	public void testAddTags(){
		Set<Long> list = new HashSet<Long>();
		list.add(1L);
		list.add(3L);
		list.add(4L);
		try {
			newsDAO.bindTags(1L, list);
		} catch (DAOException e) {
			Assert.fail();
		}		
	}
	
	@DatabaseSetup(value="/com/epam/ukadzer/dao/impl/news/dataset.xml")
	@Test
	public void testGetSortedByComments(){
		List<News> list = new ArrayList<News>();
		try {
			list = newsDAO.getAllSortedByComments(list);
		} catch (DAOException e) {
			Assert.fail();
		}
		Assert.assertEquals(6, list.size());
		Iterator<News> itr=list.iterator();
		Assert.assertEquals("title3", itr.next().getTitle());
		Assert.assertEquals("title1", itr.next().getTitle());
		
	}
	
	@DatabaseSetup(value="/com/epam/ukadzer/dao/impl/news/dataset.xml")
	@Test
	public void testGetAll(){
		List<News> list = new ArrayList<News>();
		try {
			list = newsDAO.getAllSortedByComments(list);
		} catch (DAOException e) {
			Assert.fail();
		}
		Assert.assertEquals(6, list.size());
	}
	
	@DatabaseSetup(value="/com/epam/ukadzer/dao/impl/news/dataset.xml")
	@Test
	public void testFindAllWithCriteria(){
		List<News> list = new ArrayList<News>();
		Set<Long> tags = new HashSet<>();
		tags.add(2L);
		SearchCriteria searchCriteria = new SearchCriteria(1L, tags );  
		try {
			list = newsDAO.findAllWithCriteria(list, searchCriteria);
		} catch (DAOException e) {
			Assert.fail();
		}
		Assert.assertEquals(2, list.size());
	}
	
	@DatabaseSetup(value="/com/epam/ukadzer/dao/impl/news/dataset.xml")
	@Test
	public void testCountAllWithCriteria(){
		Set<Long> tags = new HashSet<>();
		tags.add(2L);
		SearchCriteria searchCriteria = new SearchCriteria(1L, tags );  
		int result = 0;
		try {
			result = newsDAO.countAllWithCriteria(searchCriteria);
		} catch (DAOException e) {
			Assert.fail();
		}
		Assert.assertEquals(2, result);
	}	
	
	@DatabaseSetup(value="/com/epam/ukadzer/dao/impl/news/dataset.xml")
	@Test
	public void testgetSomeWithCriteria(){
		List<News> list = new ArrayList<News>();
		Set<Long> tags = new HashSet<>();
		tags.add(2L);
		tags.add(3L);
		tags.add(4L);
		SearchCriteria searchCriteria = new SearchCriteria(1L, tags );  
		try {
			list = newsDAO.findSomeWithCriteria(list, searchCriteria,2,3);
		} catch (DAOException e) {
			Assert.fail();
		}
		Assert.assertEquals(2, list.size());
	}
	
	@DatabaseSetup(value="/com/epam/ukadzer/dao/impl/news/dataset-tobeadded.xml")
	@ExpectedDatabase(value="/com/epam/ukadzer/dao/impl/news/expected-database-addauthor.xml",
	assertionMode=DatabaseAssertionMode.NON_STRICT)
	@Test
	public void testAddAuthor(){
		try {
			newsDAO.bindAuthor(6L,1L);
		} catch (DAOException e) {
			Assert.fail();
		}
	}
	
	@DatabaseSetup(value="/com/epam/ukadzer/dao/impl/news/dataset.xml")
	@ExpectedDatabase(value="/com/epam/ukadzer/dao/impl/news/expected-database-delete-link-author.xml",
	assertionMode=DatabaseAssertionMode.NON_STRICT)
	@Test
	public void testDeleteLinkToAuthor(){
		try {
			newsDAO.deleteLinkToAuthor(1L);
		} catch (DAOException e) {
			Assert.fail();
		}
	}
	
	@DatabaseSetup(value="/com/epam/ukadzer/dao/impl/news/dataset.xml")
	@ExpectedDatabase(value="/com/epam/ukadzer/dao/impl/news/expected-database-delete-link-tags.xml",
	assertionMode=DatabaseAssertionMode.NON_STRICT)
	@Test
	public void testDeleteLinkToTags(){
		try {
			newsDAO.deleteLinkToTags(1L);
		} catch (DAOException e) {
			Assert.fail();
		}
	}
}
