package com.epam.ukadzer.service.impl;

import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.ukadzer.dao.AuthorDAO;
import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.Author;
import com.epam.ukadzer.service.exception.ServiceException;

@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceTest {

	private Long myMagicalId = 3L;

	@Mock
	private AuthorDAO authorDAO;

	@InjectMocks
	private AuthorServiceImpl authorService;

	@Test
	public void shouldReturnAuthorWithDivenId() throws DAOException, ServiceException {
		Author author = new Author(myMagicalId, "palina", null);
		Mockito.when(authorDAO.getById(myMagicalId)).thenReturn(author);
		Author newAuthor = authorService.getById(myMagicalId);
		verify(authorDAO).getById(myMagicalId);
		Assert.assertEquals(author, newAuthor);
	}

	@Test
	public void shouldReturnIdOfAddedAuthor() throws DAOException, ServiceException {
		Author author = new Author(myMagicalId, "palina", null);
		Mockito.when(authorDAO.add(author)).thenReturn(myMagicalId);
		Long result = authorService.add(author);
		verify(authorDAO).add(author);
		Assert.assertEquals(myMagicalId, result);

	}

	@Test
	public void shouldCallExpireMetodOnly() throws ServiceException, DAOException {
		Author author = new Author(myMagicalId, "palina", null);
		authorService.expire(author);
		verify(authorDAO,times(1)).delete(author.getId());
		verifyNoMoreInteractions(authorDAO);
	}
	
	@Test
	public void shoulReturnCollectionOfAllAuthors() throws DAOException{
		Collection<Author> authors = new ArrayList<Author>();
		authors.add(new Author(1L, "palina", null));
		authors.add(new Author(2L, "anton", null));
		Collection<Author> newAuthors = new ArrayList<Author>();
		when(authorDAO.getAll(newAuthors)).thenReturn(authors);
		newAuthors = authorDAO.getAll(newAuthors);
		Assert.assertEquals(authors, newAuthors);
		 newAuthors = new ArrayList<Author>();
		verify(authorDAO, times(1)).getAll(newAuthors);
		verifyNoMoreInteractions(authorDAO);
	}
	
	
	

}
