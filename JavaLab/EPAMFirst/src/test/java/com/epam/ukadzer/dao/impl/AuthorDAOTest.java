package com.epam.ukadzer.dao.impl;



import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.dao.impl.SQLAuthorDAO;
import com.epam.ukadzer.entity.Author;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring-test.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
public class AuthorDAOTest {
	
	@Autowired
	SQLAuthorDAO authorDAO;
	
	@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/author/dataset.xml")
	@Test
	public void testGetById() {
		Author author = null;
		try {
			author = authorDAO.getById(1L);
		} catch (DAOException e) {
			Assert.fail();
		}
		Assert.assertEquals(author.getName(), "palina");
	}

	@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/author/dataset.xml")
	@Test
	public void testGetByIdNull() {
		Author author = null;
		try {
			author = authorDAO.getById(6L);
		} catch (DAOException e) {
			Assert.fail();
		}
		Assert.assertNull(author);
		;
	}

	@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/author/dataset.xml")
	@ExpectedDatabase(value = "/com/epam/ukadzer/dao/impl/author/expected-database.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	@Test
	public void testSave() {
		Author author = new Author(1L, "qqq", null);
		try {
			authorDAO.add(author);
		} catch (DAOException e) {
			Assert.fail();
		}
	}

	@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/author/dataset.xml")
	@ExpectedDatabase(value = "/com/epam/ukadzer/dao/impl/author/expected-database-update.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	@Test
	public void testUpdate() {
		Author author = new Author(1L, "Palina Ukadzer", null);
		try {
			authorDAO.update(author);
		} catch (DAOException e) {
			Assert.fail();
		}

	}

	@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/author/dataset.xml")
	@ExpectedDatabase(value = "/com/epam/ukadzer/dao/impl/author/expected-database-delete.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	@Test
	public void testDelete() {
		try {
			authorDAO.delete(1L);
		} catch (DAOException e) {
			Assert.fail();
		}

	}
	
}
