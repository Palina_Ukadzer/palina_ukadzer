package com.epam.ukadzer.dao;

import java.util.Collection;

import com.epam.ukadzer.dao.exception.DAOException;


public interface EntityDAO<T> {
	
	/**
     * Method gets an entity that is mapped to given id
     * @param entityId the id of entity
     * @return found entity
	 * @throws DAOException 
     */
    T getById(Long entityId) throws DAOException;

    /**
     * Method saves the entity to db
     * @param entity to be saved entity
     */
    Long add(T entity) throws DAOException;

    /**
     * Method updates the entity to db
     * @param entity entity with new parameters
     */
    void update(T entity) throws DAOException;

    /**
     * Method deletes the entity to db
     * @param entityId id of entity to be deleted
     */
    
    void delete(Long entityId) throws DAOException;

    /**
     * Method gets all the objects from db
     * @param all collection to get all object
     * @return collection with all objects
     */
    Collection<T> getAll(Collection<T> all) throws DAOException;


}
