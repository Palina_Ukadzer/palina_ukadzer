package com.epam.ukadzer.service.impl;

import java.util.Collection;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import com.epam.ukadzer.dao.AuthorDAO;
import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.Author;
import com.epam.ukadzer.service.AuthorService;
import com.epam.ukadzer.service.exception.ServiceException;



public class AuthorServiceImpl implements AuthorService{


	private static final Logger logger = Logger.getLogger(AuthorServiceImpl.class);
	
	private AuthorDAO authorDAO; 
	
	
	public void setAuthorDAO(AuthorDAO authorDAO) {
		this.authorDAO = authorDAO;
	}
	
	@Transactional
	public Long add(Author author) throws ServiceException {
		 try {
			return authorDAO.add(author);
		} catch (DAOException e) {
			logger.error("can't add an author");
			throw new ServiceException(e);
		}
	}

	

	@Transactional
	public void expire(Author author) throws ServiceException {
		try {
			authorDAO.delete(author.getId());
		} catch (DAOException e) {
			logger.error("can't expire an author");
			throw new ServiceException(e);
		} 
		
	}
	
	@Transactional
	public Author getById(Long id) throws ServiceException {
		 try {
			return authorDAO.getById(id);
		} catch (DAOException e) {
			logger.error("can't add an author");
			throw new ServiceException(e);
		}
	}
	
	@Transactional
	public Collection<Author> getAllAuthors(Collection<Author> authors) throws ServiceException{
		try {
			return authorDAO.getAll(authors);
		} catch (DAOException e) {
			logger.error("can't get all authors");
			throw new ServiceException(e);
		}
	}

}
