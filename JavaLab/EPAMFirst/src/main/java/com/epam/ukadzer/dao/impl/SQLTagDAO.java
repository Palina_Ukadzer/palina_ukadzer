package com.epam.ukadzer.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

import javax.sql.DataSource;


import com.epam.ukadzer.dao.TagDAO;
import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.Tag;


public class SQLTagDAO implements TagDAO {

	private static final String SQL_GET_BY_ID = "select t.tag_id, t.tag_name from tags t where tag_id=?";
	private static final String SQL_ADD = "insert into tags (tag_id,tag_name) "
			+ "values (tag_sequence.nextval,?)";
	private static final String SQL_UPDATE = "update tags set tag_name=? where tag_id=?";
	private static final String SQL_DELETE = "delete from tags where tag_id=?";
	private static final String SQL_GET_ALL = "select t.tag_id, t.tag_name  from tags t";
	private static final String SQL_DELETE_LINK_TO_NEWS = "delete from news_tags where tag_id=?";
	
	
	private DataSource dataSource;
	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public Tag getById(Long entityId) throws DAOException {
		Tag tag = null;
		try (Connection connection = dataSource.getConnection();
				PreparedStatement preparedStatement = connection
						.prepareStatement(SQL_GET_BY_ID);) {
			preparedStatement.setLong(1, entityId);
			try (ResultSet resultSet = preparedStatement.executeQuery();) {
				if (resultSet.next()) {
					tag = new Tag(resultSet.getLong(1), resultSet.getString(2));
				}
			}
		} catch (Exception e) {
			throw new DAOException(e);
		}
		return tag;
	}

	public Long add(Tag entity) throws DAOException {
		String generatedColumns[] = { "tag_ID" };
		try (Connection connection = dataSource.getConnection();
				PreparedStatement preparedStatement = connection
						.prepareStatement(SQL_ADD, generatedColumns);) {
			preparedStatement.setString(1, entity.getName());
			preparedStatement.executeUpdate();
			ResultSet resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next()) {
				return resultSet.getLong(1);
			} else {
				throw new SQLException();
			}
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	public void update(Tag entity) throws DAOException {

		try (Connection connection = dataSource.getConnection();
				PreparedStatement preparedStatement = connection
						.prepareStatement(SQL_UPDATE);) {
			preparedStatement.setString(1, entity.getName());
			preparedStatement.setLong(2, entity.getId());
			preparedStatement.executeUpdate();
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	public void delete(Long entityId) throws DAOException {
		
		try (Connection connection = dataSource.getConnection();
				PreparedStatement preparedStatement = connection
						.prepareStatement(SQL_DELETE);) {
			preparedStatement.setLong(1, entityId);
			preparedStatement.executeUpdate();
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	public Collection<Tag> getAll(Collection<Tag> all) throws DAOException {
		
		Collection<Tag> tags = new ArrayList<Tag>();
		try (Connection connection = dataSource.getConnection();
				Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(SQL_GET_ALL);){			
			while (resultSet.next()) {
				tags.add(new Tag(resultSet.getLong(1), resultSet.getString(2)));
			}

		} catch (Exception e) {
			throw new DAOException(e);
		}
		return tags;
	}

	@Override
	public void deleteLinkToNews(Long tagId) throws DAOException {
		try (Connection connection = dataSource.getConnection();
				PreparedStatement preparedStatement = connection
						.prepareStatement(SQL_DELETE_LINK_TO_NEWS);) {
			preparedStatement.setLong(1, tagId);
			preparedStatement.executeUpdate();
		} catch (Exception e) {
			throw new DAOException(e);
		}
		
	}

}
