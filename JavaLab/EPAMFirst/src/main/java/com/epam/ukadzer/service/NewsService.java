package com.epam.ukadzer.service;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.epam.ukadzer.entity.News;
import com.epam.ukadzer.entity.SearchCriteria;
import com.epam.ukadzer.entity.Tag;
import com.epam.ukadzer.service.exception.ServiceException;


public interface NewsService {

	/**
     * Method edits a news with id same as of news given for it
     * to be same as news given
     * @param news the news with correct parameters
	 * @throws ServiceException
     */
	public void edit(News news) throws ServiceException;
	
	/**
     * Method deletes a news with given id 
     * @param newsId the id of news to be deleted
	 * @throws ServiceException
     */
	public void delete(Long newsId) throws ServiceException;
	
	/**
     * Method gets a list of all news 
     * @param news collection of news
     * @return collection of news
	 * @throws ServiceException
     */
	public Collection<News> getNewsList(List<News> news) throws ServiceException;
	
	/**
     * Method adds tag to news 
     * @param news the news to which tag should be added
     * @param tag the tag to be bind to the news
	 * @throws ServiceException
     */
	public void addTagToNews(News news, Tag tag) throws ServiceException;
	
	/**
     * Method adds set of tags to news 
     * @param news the news to which tags should be added
     * @param tags the set of tags to be bind to the news
	 * @throws ServiceException
     */
	public void bindTags(Long newsId, Set<Long> tags) throws ServiceException;
	
	/**
     * Method gets all news sorted by comments 
     * @param news list of news
     * @return list of news
	 * @throws ServiceException
     */
	public List<News> getCommSortedNewsList(List<News> news) throws ServiceException;
	
	/**
     * Method counts all news
     * @return number of news 
     * @throws ServiceException
     */
	public int countAll() throws ServiceException;
	
	/**
     * Method counts all news that suits given search criteria
     * @param searchCriteria the search criteria
     * @return number of news that suits given search criteria
     * @throws ServiceException
     */
	public int countAllWithCriteria(SearchCriteria searchCriteria) throws ServiceException;
	
	/**
     * Method gets all news that suits given search criteria
     * @param searchCriteria the search criteria
     * @param news list of news
     * @return list of news that suits given search criteria
     * @throws ServiceException
     */
	public List<News> gettAllWithCriteria (List<News> news, 
			SearchCriteria criteria) throws ServiceException;
	
	/**
     * Method gets a specific number of news that suits given search criteria
     * @param searchCriteria the search criteria
     * @param news list of news
     * @param start the first row to be taken
     * @param finish the last row to be taken 
     * @return list of news that suits given search criteria
     * @throws ServiceException
     */
	public List<News> getSomeWithCriteria (List<News> news, SearchCriteria criteria, 
			int start, int finish) throws ServiceException;
	/**
     * Method deletes a link of given news to any author 
     * @param newsId id of news which link is to be deleted
	 * @throws ServiceException
     */
	public void deleteLinkToAuthor(Long newsId) throws ServiceException;
	
	/**
     * Method deletes a link of given news to any tag(s) 
     * @param newsId id of news which link is to be deleted
	 * @throws ServiceException
     */
	public void deleteLinkToTags(Long newsId) throws ServiceException;
	
	/**
     * Method add a news to news table 
     * @param news the news to be added
     * @return id of found news
	 * @throws ServiceException
     */
	public Long addNews(News news) throws ServiceException;
	
	/**
     * Method binds an author to news 
     * @param newsId id of new to be bonded
     * @return authorId id of author to be bonded
	 * @throws ServiceException
     */
	public void bindAuthor(Long newsId, Long authorId) throws ServiceException;
	
	
}
