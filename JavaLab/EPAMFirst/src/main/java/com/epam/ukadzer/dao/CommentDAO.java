package com.epam.ukadzer.dao;

import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.Comment;

public interface CommentDAO extends EntityDAO<Comment>{
	

	/**
     * Method deletes all comment of news with given Id
     * @param newsId id of news with comments are to be deleted
	 * @throws DAOException
     */
	public void deleteAllForNews(Long newsId) throws DAOException;
	
	
}
