package com.epam.ukadzer.service.impl;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.ukadzer.entity.ComplexNews;
import com.epam.ukadzer.entity.Tag;
import com.epam.ukadzer.service.CommentService;
import com.epam.ukadzer.service.ComplexNewsService;
import com.epam.ukadzer.service.NewsService;
import com.epam.ukadzer.service.TagService;
import com.epam.ukadzer.service.exception.ServiceException;

@Transactional(rollbackFor=Exception.class)
public class ComplexNewsServiceImpl implements ComplexNewsService {

	
	NewsService newsService;
	
	TagService tagService;
	
	CommentService commentService;
	

	public void setNewsService(NewsService newsService) {
		this.newsService = newsService;
	}



	public void setTagService(TagService tagService) {
		this.tagService = tagService;
	}



	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}



	@Override
	public void add(ComplexNews complexNews) throws ServiceException {
			Long newsId = newsService.addNews(complexNews.getNews());
			newsService.bindAuthor(newsId, complexNews.getAuthor().getId());
			Set<Long> set = new HashSet<Long>();
			for(Tag tag: complexNews.getTags()){
				set.add(tag.getId());
			}
			newsService.bindTags(complexNews.getNews().getId(), set);
	}



	@Override
	public void delete(ComplexNews complexNews) throws ServiceException {
		Long newsId = complexNews.getNews().getId();
		newsService.deleteLinkToAuthor(newsId);
		newsService.deleteLinkToTags(newsId);
		commentService.deleteAllForNews(newsId);
		newsService.delete(newsId);
	}

}
