package com.epam.ukadzer.service;


import java.util.Collection;

import com.epam.ukadzer.entity.Author;
import com.epam.ukadzer.service.exception.ServiceException;


public interface AuthorService {

	/**
     * Method creates a news author (saves it to db)
     * @param author the author to be saved
     * @returns id of created (saved) author 
	 * @throws ServiceException
     */
	public Long add(Author author) throws ServiceException;
	
	/**
     * Method expires given author
     * @param author the author to become expired
	 * @throws ServiceException
     */
	public void expire(Author author) throws ServiceException;

	/**
     * Method get an Author with given id
     * @param id id of author to be found
     * @returns found author 
	 * @throws ServiceException
     */
	public Author getById(Long id) throws ServiceException;
	
	/**
     * Method gets all authors
     * @param authors collection of authors where new will be put
     * @returns collection of found authors 
	 * @throws ServiceException
     */
	public Collection<Author> getAllAuthors(Collection<Author> authors) throws ServiceException;
}
