package com.epam.ukadzer.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.sql.DataSource;





import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.ukadzer.dao.NewsDAO;
import com.epam.ukadzer.dao.UtilsService;
import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.News;
import com.epam.ukadzer.entity.SearchCriteria;


public class SQLNewsDAO implements NewsDAO {

	private static final String SQL_GET_BY_ID = "select n.news_id, n.TITLE, n.SHORT_TEXT, "
			+ "n.FULL_TEXT, n.CREATION_DATE, n.MODIFICATION_DATE from news n where news_id=?";
	private static final String SQL_ADD = "insert into news (news_id,title,short_text, full_text, creation_date, modification_date) "
			+ "values (news_sequence.nextval,?,?,?,?,?)";
	private static final String SQL_UPDATE = "update news set title=?, short_text=?, full_text=?, "
			+ "creation_date=?, modification_date=? where news_id=?";
	private static final String SQL_DELETE_FROM_NEWS = "delete from news where news_id=?";
	private static final String SQL_GET_ALL = "select n.TITLE, n.SHORT_TEXT, "
			+ "n.FULL_TEXT, n.CREATION_DATE, n.MODIFICATION_DATE from news";
	private static final String SQL_ADD_TAG = "insert into news_tags (news_tag_id, news_id, tag_id) "
			+ "values (news_tags_sequence.nextval,?,?)";
	private static final String SQL_COUNT_ALL = "select count(*) from news";
	private static final String SQL_GET_ALL_SORTED_BY_COMM = "select distinct n.News_id, n.TITLE, n.SHORT_TEXT, "
			+ "n.FULL_TEXT, n.CREATION_DATE, n.MODIFICATION_DATE, count(c.comment_id)"
			+ "from news n LEFT OUTER JOIN comments c on n.news_id=c.news_id "
			+ "group by n.News_id, n.TITLE, n.SHORT_TEXT, n.FULL_TEXT, n.CREATION_DATE, n.MODIFICATION_DATE"
			+ " order by count(c.comment_id)desc , n.news_id";
	private static final String SQL_ADD_TAGS = "insert into news_tags (news_tag_id, news_id, tag_id) "
			+ "values (news_tags_sequence.nextval,?,?) ";
	private static final String SQL_ADD_AUTHOR = "insert into news_authors (news_author_id, news_id, author_id) "
			+ "values (news_ath_sequence.nextval,?,?) ";
	private static final String SQL_FIND_SOME_BEGINING = "select * from ( select a.*, ROWNUM rnum from ( "
			+ "SELECT * FROM  news n LEFT OUTER JOIN NEWS_AUTHORS na on n.NEWS_ID=na.NEWS_ID "
			+ "LEFT outer join NEWS_TAGS nt on n.NEWS_ID=nt.NEWS_ID ";
	private static final String SQL_COUNT_CRITERIA_BEGINING = "select count(*) from news n LEFT OUTER JOIN NEWS_AUTHORS na on n.NEWS_ID=na.NEWS_ID "
			+ "LEFT outer join NEWS_TAGS nt on n.NEWS_ID=nt.NEWS_ID ";
	private static final String SQL_FIND_ALL_BEGINING = "select n.news_id, n.TITLE, n.SHORT_TEXT, "
			+ "n.FULL_TEXT, n.CREATION_DATE, n.MODIFICATION_DATE from news n LEFT OUTER JOIN NEWS_AUTHORS na on n.NEWS_ID=na.NEWS_ID "
			+ "LEFT outer join NEWS_TAGS nt on n.NEWS_ID=nt.NEWS_ID ";
	private static final String SQL_DELETE_LINK_TO_AUTHOR = "delete from news_authors where news_id=?";
	private static final String SQL_DELETE_LINK_TO_TAGS = "delete from news_tags where news_id=?";
	
	private DataSource dataSource;
	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public News getById(Long entityId) throws DAOException {

		News news = null;
		try (Connection connection = dataSource.getConnection();
				PreparedStatement preparedStatement = connection
						.prepareStatement(SQL_GET_BY_ID);) {

			preparedStatement.setLong(1, entityId);
			try (ResultSet resultSet = preparedStatement.executeQuery();) {
				if (resultSet.next()) {
					news = new News(resultSet.getLong(1),
							resultSet.getString(2), resultSet.getString(3),
							resultSet.getString(4), resultSet.getTimestamp(5),
							resultSet.getDate(6));
				}
			}
		} catch (Exception e) {
			throw new DAOException(e);
		}
		return news;
	}

	public Long add(News entity) throws DAOException {
		String generatedColumns[] = { "news_ID" };
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection
					.prepareStatement(SQL_ADD, generatedColumns);
			preparedStatement.setString(1, entity.getTitle());
			preparedStatement.setString(2, entity.getShortText());
			preparedStatement.setString(3, entity.getFullText());
			preparedStatement.setTimestamp(4, utilDatetoTimestamp(entity.getCreationDate()));
			preparedStatement.setDate(5, utilDatetoSQLDate(entity.getModificationDate()));
			preparedStatement.executeUpdate();
			try (ResultSet resultSet = preparedStatement.getGeneratedKeys();) {
				if (resultSet.next()) {
					return resultSet.getLong(1);
				} else {
					throw new SQLException();
				}
			}
		} catch (Exception e) {
			throw new DAOException(e);
		}finally{
			try {
				UtilsService.closeConnection(connection, preparedStatement, null, dataSource);
			} catch (SQLException e) {
				throw new DAOException(e);
			} 
		}
	}

	public void update(News entity) throws DAOException {
		try (Connection connection = dataSource.getConnection();
				PreparedStatement preparedStatement = connection
						.prepareStatement(SQL_UPDATE);) {
			preparedStatement.setString(1, entity.getTitle());
			preparedStatement.setString(2, entity.getShortText());
			preparedStatement.setString(3, entity.getFullText());
			preparedStatement.setTimestamp(4, utilDatetoTimestamp(entity.getCreationDate()));
			preparedStatement.setDate(5, utilDatetoSQLDate(entity.getModificationDate()));
			preparedStatement.setLong(6, entity.getId());
			preparedStatement.executeUpdate();

		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	
	
	public void delete(Long entityId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_DELETE_FROM_NEWS);
			preparedStatement.setLong(1, entityId);
			preparedStatement.executeUpdate();
		} catch (Exception e) {
			throw new DAOException(e);
		}finally{
			try {
				UtilsService.closeConnection(connection, preparedStatement, null, dataSource);
			} catch (SQLException e) {
				throw new DAOException(e);
			}
		}
	}

	public Collection<News> getAll(Collection<News> all) throws DAOException {
		Collection<News> news = new ArrayList<News>();
		try (Connection connection = dataSource.getConnection();
				Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(SQL_GET_ALL);) {
			while (resultSet.next()) {
				news.add(new News(resultSet.getLong(1), resultSet.getString(2),
						resultSet.getString(3), resultSet.getString(4),
						resultSet.getTimestamp(5), resultSet.getDate(6)));
			}

		} catch (Exception e) {
			throw new DAOException(e);
		}
		return news;
	}

	public void bindTag(Long newsId, Long tagId) throws DAOException {
		try (Connection connection = dataSource.getConnection();
				PreparedStatement preparedStatement = connection
						.prepareStatement(SQL_ADD_TAG);) {
			preparedStatement.setLong(1, newsId);
			preparedStatement.setLong(2, tagId);
			preparedStatement.executeUpdate();

		} catch (Exception e) {
			throw new DAOException(e);
		}

	}

	public int countAll() throws DAOException {
		String query = SQL_COUNT_ALL;
		int result = -1;
		try (Connection connection = dataSource.getConnection();
				Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(query);) {
			if (resultSet.next()) {
				result = resultSet.getInt(1);
			}
		} catch (Exception e) {
			throw new DAOException(e);
		}
		return result;
	}

	public List<News> getAllSortedByComments(List<News> all)
			throws DAOException {

		try (Connection connection = dataSource.getConnection();
				Statement statement = connection.createStatement();
				ResultSet resultSet = statement
						.executeQuery(SQL_GET_ALL_SORTED_BY_COMM);) {
			while (resultSet.next()) {
				all.add(new News(resultSet.getLong(1), resultSet.getString(2),
						resultSet.getString(3), resultSet.getString(4),
						resultSet.getTimestamp(5), resultSet.getDate(6)));
			}

		} catch (Exception e) {
			throw new DAOException(e);
		}
		return all;
	}

	public void bindTags(Long NewsId, Set<Long> tagsIds) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try{
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_ADD_TAGS);
			preparedStatement.setLong(1, NewsId);
			Iterator<Long> itr = tagsIds.iterator();
			while (itr.hasNext()) {
				preparedStatement.setLong(2, itr.next());
				preparedStatement.executeUpdate();
			}

		} catch (Exception e) {
			throw new DAOException(e);
		}finally{
			try {
				UtilsService.closeConnection(connection, preparedStatement, null, dataSource);
			} catch (SQLException e) {
				throw new DAOException(e);
			}
		}
	}

	public List<News> findAllWithCriteria(List<News> all,
			SearchCriteria searchCriteria) throws DAOException {
		String query = SQL_FIND_ALL_BEGINING;
		try (Connection connection = dataSource.getConnection();
				Statement statement = connection.createStatement();) {
			boolean authorExists = false;
			if (searchCriteria.getAuthorId() != null) {
				authorExists = true;
				query += "where author_id="
						+ searchCriteria.getAuthorId() + " ";
			}
			if (!searchCriteria.getTagsIds().isEmpty()) {
				Iterator<Long> itr = searchCriteria.getTagsIds().iterator();
				if (authorExists) {
					query += "AND ( ";
				} else {
					query += " where ";
				}
				int i = 0;
				while (itr.hasNext()) {
					if (i == 0) {
						query += "tag_id=" + itr.next() + " ";
					} else {
						query += "or tag_id=" + itr.next() + " ";
					}
				}
				if (authorExists) {
					query += ")";
				}

			}
			ResultSet resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				all.add(new News(resultSet.getLong(1), resultSet.getString(2),
						resultSet.getString(3), resultSet.getString(4),
						resultSet.getTimestamp(5), resultSet.getDate(6)));
			}

		} catch (Exception e) {
			throw new DAOException(e);
		}
		return all;
	}

	public int countAllWithCriteria(SearchCriteria searchCriteria)
			throws DAOException {

		String query = SQL_COUNT_CRITERIA_BEGINING;
		int result = -1;
		try (Connection connection = dataSource.getConnection();
				Statement statement = connection.createStatement();) {
			boolean authorExists = false;
			if (searchCriteria.getAuthorId() != null) {
				authorExists = true;
				query += "where author_id="
						+ searchCriteria.getAuthorId() + " ";
			}
			if (!searchCriteria.getTagsIds().isEmpty()) {
				Iterator<Long> itr = searchCriteria.getTagsIds().iterator();
				if (authorExists) {
					query += "AND ( ";
				}
				int i = 0;
				while (itr.hasNext()) {
					if (i == 0) {
						query += "tag_id=" + itr.next() + " ";
					} else {
						query += "or tag_id=" + itr.next() + " ";
					}
				}
				if (authorExists) {
					query += ")";
				}

			}
			try (ResultSet resultSet = statement.executeQuery(query);) {
				while (resultSet.next()) {
					result = resultSet.getInt(1);
				}
			}
		} catch (Exception e) {
			throw new DAOException(e);
		}
		return result;
	}

	public List<News> findSomeWithCriteria(List<News> all,
			SearchCriteria searchCriteria, int start, int finish)
			throws DAOException {

		String query = SQL_FIND_SOME_BEGINING;
		try (Connection connection = dataSource.getConnection();
				Statement statement = connection.createStatement();) {
			boolean authorExists = false;
			if (searchCriteria.getAuthorId() != null) {
				authorExists = true;
				query += "where author_id="
						+ searchCriteria.getAuthorId() + " ";
			}
			if (!searchCriteria.getTagsIds().isEmpty()) {
				Iterator<Long> itr = searchCriteria.getTagsIds().iterator();
				if (authorExists) {
					query += "AND ( ";
				} else {
					query += " where ";
				}
				int i = 0;
				while (itr.hasNext()) {
					if (i == 0) {
						query += "tag_id=" + itr.next() + " ";
						i++;
					} else {
						query += "or tag_id=" + itr.next() + " ";
					}
				}
				if (authorExists) {
					query += ")";
				}

			}
			query += " ORDER BY n.NEWS_ID) a where rownum <=  " + finish
					+ ") where rnum >= " + start;
			try (ResultSet resultSet = statement.executeQuery(query);) {
				while (resultSet.next()) {
					all.add(new News(resultSet.getLong(1), resultSet
							.getString(2), resultSet.getString(3), resultSet
							.getString(4), resultSet.getTimestamp(5), resultSet
							.getDate(6)));
				}
			}

		} catch (Exception e) {
			throw new DAOException(e);
		}
		return all;
	}

	@Override
	public void bindAuthor(Long newsId, Long authorId) throws DAOException {
		
		String query = SQL_ADD_AUTHOR;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try{
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(query);
			preparedStatement.setLong(1, newsId);
			preparedStatement.setLong(2, authorId);
			preparedStatement.executeUpdate();

		} catch (Exception e) {
			throw new DAOException(e);
		}finally{
			try {
				UtilsService.closeConnection(connection, preparedStatement, null, dataSource);
			} catch (SQLException e) {
				throw new DAOException(e);
			}
		}

	}
	
	private Timestamp utilDatetoTimestamp(Date date){
		if(date == null){
			return null;
		}else {
			return new Timestamp(date.getTime());
		}
	}
	
	private java.sql.Date utilDatetoSQLDate(Date date){
		if(date == null){
			return null;
		}else {
			return new java.sql.Date(date.getTime());
		}
	}

	@Override
	public void deleteLinkToAuthor(Long newsId) throws DAOException {
		Connection connection= null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_DELETE_LINK_TO_AUTHOR);
			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();
		} catch (Exception e) {
			throw new DAOException(e);
		}finally{
			try {
				UtilsService.closeConnection(connection, preparedStatement, null, dataSource);
			} catch (SQLException e) {
				throw new DAOException(e);
			}
		}
		
	}

	@Override
	public void deleteLinkToTags(Long newsId) throws DAOException {
		Connection connection= null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_DELETE_LINK_TO_TAGS);
			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();
		} catch (Exception e) {
			throw new DAOException(e);
		}finally{
			try {
				UtilsService.closeConnection(connection, preparedStatement, null, dataSource);
			} catch (SQLException e) {
				throw new DAOException(e);
			}
		}
		
	}

}
