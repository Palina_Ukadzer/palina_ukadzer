package com.epam.ukadzer.service.impl;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import com.epam.ukadzer.dao.CommentDAO;
import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.Comment;
import com.epam.ukadzer.service.CommentService;
import com.epam.ukadzer.service.exception.ServiceException;


public class CommentServiceImpl implements CommentService {

	private static final Logger logger = Logger.getLogger(CommentServiceImpl.class);
	
	private CommentDAO commentDAO;

	public void setCommentDAO(CommentDAO commentDAO) {
		this.commentDAO = commentDAO;
	}

	@Transactional
	public Long add(Comment comment) throws ServiceException {
		try {
			return commentDAO.add(comment);
		} catch (DAOException e) {
			logger.error("can't add a comment");
			throw new ServiceException(e);
		}
		
	}

	@Transactional
	public void delete(Long commentId) throws ServiceException {
		try {
			commentDAO.delete(commentId);
		} catch (DAOException e) {
			logger.error("can't delete comment");
			throw new ServiceException(e);
		}
		
	}

	@Transactional
	public void deleteAllForNews(Long newsId) throws ServiceException {
		try {
			commentDAO.deleteAllForNews(newsId);
		} catch (DAOException e) {
			logger.error("can't delete comment");
			throw new ServiceException(e);
		}
		
	}

}
