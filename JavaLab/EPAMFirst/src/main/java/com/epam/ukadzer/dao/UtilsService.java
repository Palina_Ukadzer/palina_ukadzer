package com.epam.ukadzer.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;


public class UtilsService {
	
	/**
     * Method releases connection, its statement and result set
	 * @throws SQLException
     */
	public static void closeConnection (Connection connection, Statement statement, ResultSet resultSet, 
			DataSource dataSource) throws SQLException{
		if(resultSet != null){
			try {
				resultSet.close();
			} catch (SQLException e) {
				throw new SQLException();
			}
		}
		if(statement != null){
			try {
				statement.close();
			} catch (SQLException e) {
				throw new SQLException();
			}
		}
		if(connection != null){
			DataSourceUtils.releaseConnection(connection, dataSource);
		}		
	}
	
	/**
     * Method releases connection, its prepared statement and result set
	 * @throws SQLException
     */
	public static void closeConnection (Connection connection, PreparedStatement statement, ResultSet resultSet, 
			DataSource dataSource) throws SQLException{
		if(resultSet != null){
			try {
				resultSet.close();
			} catch (SQLException e) {
				throw new SQLException();
			}
		}
		if(statement != null){
			try {
				statement.close();
			} catch (SQLException e) {
				throw new SQLException();
			}
		}
		if(connection != null){
			DataSourceUtils.releaseConnection(connection, dataSource);
		}		
	}

}
