package com.epam.ukadzer.service;

import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.Tag;
import com.epam.ukadzer.service.exception.ServiceException;


public interface TagService {
	/**
     * Method creates an new tag (saves it to db)
     * @param tag tag to be saved
     * @return id of saved tag
	 * @throws DAOException
     */
	public Long add(Tag tag) throws ServiceException;
	
	/**
     * Method deletes links of tag with news
     * @param tagId id of tag
	 * @throws DAOException
     */
	public void deleteLinkToNews(Long tagId) throws ServiceException;

}
