package com.epam.ukadzer.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.sql.DataSource;





import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.ukadzer.dao.CommentDAO;
import com.epam.ukadzer.dao.UtilsService;
import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.Comment;


public class SQLCommentDAO implements CommentDAO {

	private static final String SQL_GET_BY_ID = "select c.comment_id, c.news_id, c.comment_text, c.creation_date from comments c where comment_id=?";
	private static final String SQL_ADD = "insert into comments (comment_id,news_id,comment_text, creation_date) "
			+ "values (comm_sequence.nextval,?,?,?)";
	private static final String SQL_UPDATE = "update comments set comment_text=? where comment_id=?";
	private static final String SQL_DELETE = "delete from comments where comment_id=?";
	private static final String SQL_GEL_ALL = "select c.comment_id, c.news_id, c.comment_text, c.creation_date from comments c";
	private static final String SQL_DELETE_FOR_NEWS = "delete from comments where news_id=?";
 
	private DataSource dataSource;
	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public Comment getById(Long entityId) throws DAOException {

		Comment comment = new Comment();
		try (Connection connection = dataSource.getConnection();
				PreparedStatement preparedStatement = connection
						.prepareStatement(SQL_GET_BY_ID);) {
			preparedStatement.setLong(1, entityId);
			try (ResultSet resultSet = preparedStatement.executeQuery();) {
				if (resultSet.next()) {
					comment = new Comment(resultSet.getLong(1),
							resultSet.getLong(2), resultSet.getString(3),
							resultSet.getDate(4));
				}
			}
		} catch (Exception e) {
			throw new DAOException(e);
		}
		return comment;
	}

	public Long add(Comment entity) throws DAOException {
		String generatedColumns[] = { "comment_ID" };
		try (Connection connection = dataSource.getConnection();
				PreparedStatement preparedStatement = connection
						.prepareStatement(SQL_ADD, generatedColumns);) {
			preparedStatement.setLong(1, entity.getNewsId());
			preparedStatement.setString(2, entity.getText());
			preparedStatement.setTimestamp(3, utilDatetoTimestamp(entity.getCreationDate()));
			preparedStatement.executeUpdate();
			try (ResultSet resultSet = preparedStatement.getGeneratedKeys();) {
				if (resultSet.next()) {
					return resultSet.getLong(1);
				}else {
					throw new SQLException();
				}
			}
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	public void update(Comment entity) throws DAOException {
		try (Connection connection = dataSource.getConnection();
				PreparedStatement preparedStatement = connection
						.prepareStatement(SQL_UPDATE);){	
			preparedStatement.setString(1, entity.getText());
			preparedStatement.setLong(2, entity.getId());
			preparedStatement.executeUpdate();

		} catch (Exception e) {
			throw new DAOException(e);
		}

	}

	public void delete(Long entityId) throws DAOException {
		try (Connection connection = dataSource.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE);){
			preparedStatement.setLong(1, entityId);
			preparedStatement.executeUpdate();

		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	public Collection<Comment> getAll(Collection<Comment> all)
			throws DAOException {
		Collection<Comment> comments = new ArrayList<Comment>();
		try (Connection connection = dataSource.getConnection();
				Statement statement = connection.createStatement();){			
			ResultSet resultSet = statement.executeQuery(SQL_GEL_ALL);
			while (resultSet.next()) {
				comments.add(new Comment(resultSet.getLong(1), resultSet
						.getLong(2), resultSet.getString(3), resultSet
						.getTimestamp(4)));
			}

		} catch (Exception e) {
			throw new DAOException(e);
		}
		return comments;
	}
	
	private Timestamp utilDatetoTimestamp(Date date){
		if(date == null){
			return null;
		}else {
			return new Timestamp(date.getTime());
		}
	}

	@Override
	public void deleteAllForNews(Long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try{
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_DELETE_FOR_NEWS);
			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();

		} catch (Exception e) {
			throw new DAOException(e);
		}finally{
			try {
				UtilsService.closeConnection(connection, preparedStatement, null, dataSource);
			} catch (SQLException e) {
				throw new DAOException(e);
			}
		}
		
	}

}
