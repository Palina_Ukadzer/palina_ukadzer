package com.epam.ukadzer.service;

import com.epam.ukadzer.entity.ComplexNews;
import com.epam.ukadzer.service.exception.ServiceException;


public interface ComplexNewsService {
	
	/**
     * Method creates a complex news (saves it to db)
     * @param complex news the complex news to be saved
	 * @throws ServiceException
     */
	public void add(ComplexNews complexNews) throws ServiceException;
	
	/**
     * Method deletes a complex news (saves it to db)
     * @param complex news the complex news to be deleted
	 * @throws ServiceException
     */
	public void delete(ComplexNews complexNews) throws ServiceException;
}
