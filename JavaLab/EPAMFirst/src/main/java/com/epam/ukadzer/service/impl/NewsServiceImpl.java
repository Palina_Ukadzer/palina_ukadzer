package com.epam.ukadzer.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import com.epam.ukadzer.dao.NewsDAO;
import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.News;
import com.epam.ukadzer.entity.SearchCriteria;
import com.epam.ukadzer.entity.Tag;
import com.epam.ukadzer.service.NewsService;
import com.epam.ukadzer.service.exception.ServiceException;

public class NewsServiceImpl implements NewsService {

	private Logger logger = Logger.getLogger(NewsServiceImpl.class);
	
	private NewsDAO newsDAO;
	
	public void setNewsDAO(NewsDAO newsDAO) {
		this.newsDAO = newsDAO;
	}


	@Transactional
	public void edit(News news) throws ServiceException {
		try {
			newsDAO.update(news);
		} catch (DAOException e) {
			logger.error("can't edit news");
			throw new ServiceException(e);
		}
		
	}

	@Transactional
	public Collection<News> getNewsList(List<News> news) throws ServiceException {
		try {
			return newsDAO.getAll(news);
		} catch (DAOException e) {
			logger.error("can't getNewsList");
			throw new ServiceException(e);
		}
	}

	@Transactional
	public void addTagToNews(News news, Tag tag) throws ServiceException {
		try {
			newsDAO.bindTag(news.getId(), tag.getId());
		} catch (DAOException e) {
			logger.error("can't addTagToNews");
			throw new ServiceException(e);
		}
		
	}

	@Transactional
	public void bindTags(Long newsId, Set<Long> tags) throws ServiceException {
		try {
			newsDAO.bindTags(newsId, tags);
		} catch (DAOException e) {
			logger.error("Can't addTagsToNews");
			throw new ServiceException(e);
		}
		
	}

	@Transactional
	public List<News> getCommSortedNewsList(List<News> news) throws ServiceException {
		try {
			return newsDAO.getAllSortedByComments(news);
		} catch (DAOException e) {
			logger.error("Can't getCommSortedNewsList");
			throw new ServiceException(e);
		}
	}

	@Transactional
	public int countAll() throws ServiceException {
		try {
			return newsDAO.countAll();
		} catch (DAOException e) {
			logger.error("Cant count all news");
			throw new ServiceException(e);
		}
	}

	@Transactional
	public int countAllWithCriteria(SearchCriteria searchCriteria) throws ServiceException {
		try {
			return newsDAO.countAllWithCriteria(searchCriteria);
		} catch (DAOException e) {
			logger.error("can't count all news with criteria");
			throw new ServiceException(e);
		}
	}


	@Transactional
	public List<News> gettAllWithCriteria(List<News> news,
			SearchCriteria criteria) throws ServiceException {
		
		try {
			return newsDAO.findAllWithCriteria(news, criteria);
		} catch (DAOException e) {
			logger.error("can't gat all with criteria");
			throw new ServiceException(e);
		}
	}

	@Transactional
	public List<News> getSomeWithCriteria(List<News> news,
			SearchCriteria criteria, int start, int finish) throws ServiceException {
		
		try {
			return newsDAO.findSomeWithCriteria(news, criteria, start, finish);
		} catch (DAOException e) {
			logger.error("cant get some with criteria");
			throw new ServiceException(e);
		}
	}

	@Transactional
	public void deleteLinkToAuthor(Long newsId) throws ServiceException {
		try {
			newsDAO.deleteLinkToAuthor(newsId);
		} catch (DAOException e) {
			logger.error("can't delete comment");
			throw new ServiceException(e);
		}
	}

	@Transactional
	public void deleteLinkToTags(Long newsId) throws ServiceException {
		try {
			newsDAO.deleteLinkToTags(newsId);
		} catch (DAOException e) {
			logger.error("can't delete comment");
			throw new ServiceException(e);
		}
	}

	@Transactional
	public void delete(Long newsId) throws ServiceException {
		try {
			newsDAO.delete(newsId);
		} catch (DAOException e) {
			logger.error("can't delete comment");
			throw new ServiceException(e);
		}
		
	}

	@Override
	public Long addNews(News news) throws ServiceException {
		try {
			return newsDAO.add(news);
		} catch (DAOException e) {
			logger.error("can't delete comment");
			throw new ServiceException(e);
		}
		
	}

	@Override
	public void bindAuthor(Long newsId, Long authorId) throws ServiceException {
		try {
			newsDAO.bindAuthor(newsId, authorId);
		} catch (DAOException e) {
			logger.error("can't delete comment");
			throw new ServiceException(e);
		}
		
	}
	
	
	

	

}
