package com.epam.ukadzer.dao;

import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.Tag;

public interface TagDAO extends EntityDAO<Tag>{
	

	/**
     * Method deletes all links of given tag
     * @param tagId the id of tag
	 * @throws DAOException
     */
	public void deleteLinkToNews(Long tagId) throws DAOException;
}