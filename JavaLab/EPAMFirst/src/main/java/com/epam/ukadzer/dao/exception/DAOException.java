package com.epam.ukadzer.dao.exception;

public class DAOException extends Exception{
	
	private static final long serialVersionUID = 1L;

	public DAOException(){
		super();
	}
	
	public DAOException(Exception e){
		super(e);
	}
	
	public DAOException(String string){
		super(string);
	}
	
	public DAOException(String s,Exception e){
		super(s, e);
	}

}
