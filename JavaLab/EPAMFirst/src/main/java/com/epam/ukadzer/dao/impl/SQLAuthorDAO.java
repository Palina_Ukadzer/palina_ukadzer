package com.epam.ukadzer.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.sql.DataSource;

import com.epam.ukadzer.dao.AuthorDAO;
import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.Author;



public class SQLAuthorDAO implements AuthorDAO {
	
	private static final String SQL_GET_BY_ID = "select a.author_id, a.author_name, a.expired from Authors a where author_id=?";
	private static final String SQL_ADD = "insert into Authors (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (ath_sequence.nextval,?,?)";
	private static final String SQL_UPDATE = "update authors set AUTHOR_NAME=?, EXPIRED=? where author_id=?";
	private static final String SQL_GET_ALL = "select a.author_id, a.author_name, a.expired from authors a";

	private DataSource dataSource; 
	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	
	public Author getById(Long entityId) throws DAOException {
		Author author = null;
		try (Connection connection = dataSource.getConnection();
				PreparedStatement preparedStatement = connection
						.prepareStatement(SQL_GET_BY_ID);) {			
			preparedStatement.setLong(1, entityId);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				author = new Author(resultSet.getLong(1),
						resultSet.getString(2), resultSet.getTimestamp(3));
			}
		} catch (Exception e) {

			throw new DAOException(e);
		}
		return author;
	}

	public Long add(Author entity) throws DAOException {

		String generatedColumns[] = { "author_ID" };
		try (Connection connection = dataSource.getConnection();
				PreparedStatement preparedStatement = connection
						.prepareStatement(SQL_ADD, generatedColumns);) {

			preparedStatement.setString(1, entity.getName());
			preparedStatement.setTimestamp(2, utilDatetoTimestamp(entity.getExpired()));
			preparedStatement.executeUpdate();
			try (ResultSet resultSet = preparedStatement.getGeneratedKeys();) {
				if (resultSet.next()) {
					return resultSet.getLong(1);
				} else {
					throw new SQLException();
				}
			}

		} catch (Exception e) {
			throw new DAOException(e);
		}

	}

	public void update(Author entity) throws DAOException {
		try (Connection connection = dataSource.getConnection();
				PreparedStatement preparedStatement = connection
						.prepareStatement(SQL_UPDATE);) {
			preparedStatement.setString(1, entity.getName());
			preparedStatement.setTimestamp(2, utilDatetoTimestamp(entity.getExpired()));
			preparedStatement.setLong(3, entity.getId());
			preparedStatement.executeUpdate();
		} catch (Exception e) {

			throw new DAOException(e);
		}

	}

	public void delete(Long entityId) throws DAOException {
		java.util.Date date = new java.util.Date();
		Author author = getById(entityId);
		author.setExpired(new Timestamp(date.getTime()));
		update(author);

	}

	public Collection<Author> getAll(Collection<Author> all)
			throws DAOException {

		Collection<Author> authors = new ArrayList<Author>();
		try (Connection connection = dataSource.getConnection();
				Statement statement = connection.createStatement();) {
			try (ResultSet resultSet = statement.executeQuery(SQL_GET_ALL);) {
				while (resultSet.next()) {
					authors.add(new Author(resultSet.getLong(1), resultSet
							.getString(2), resultSet.getTimestamp(3)));
				}
			}

		} catch (Exception e) {

			throw new DAOException(e);
		}
		return authors;
	}
	
	private Timestamp utilDatetoTimestamp(Date date){
		if(date == null){
			return null;
		}else {
			return new Timestamp(date.getTime());
		}
	}

}
