package com.epam.ukadzer.service.impl;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import com.epam.ukadzer.dao.TagDAO;
import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.Tag;
import com.epam.ukadzer.service.TagService;
import com.epam.ukadzer.service.exception.ServiceException;


public class TagServiceImpl implements TagService {

	private static final Logger logger = Logger.getLogger(TagServiceImpl.class);

	private TagDAO tagDAO;
	
	public void setTagDAO(TagDAO tagDAO) {
		this.tagDAO = tagDAO;
	}

	
	@Transactional
	public Long add(Tag tag) throws ServiceException {
		try {
			return tagDAO.add(tag);
		} catch (DAOException e) {
			logger.error("can't add tag");
			 throw new ServiceException(e);
		}		
	}

	@Override
	public void deleteLinkToNews(Long tagId) throws ServiceException {
		try {
			tagDAO.deleteLinkToNews(tagId);
		} catch (DAOException e) {
			logger.error("can't delete comment");
			throw new ServiceException(e);
		}
		
	}

}
