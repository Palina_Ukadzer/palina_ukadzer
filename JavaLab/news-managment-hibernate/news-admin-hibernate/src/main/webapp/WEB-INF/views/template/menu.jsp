
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="btn-group-vertical">

	<a href="/news-admin-hibernate/news/list/1" class="btn btn-default"><button>
			<spring:message code="newsList" />
		</button></a> <a href="/news-admin-hibernate/news/add" class="btn btn-default"><button>
			<spring:message code="addNews" />
		</button></a> <a href="/news-admin-hibernate/author/list" class="btn btn-default"><button>
			<spring:message code="addUpdateAuthors" />
		</button></a> <a href="/news-admin-hibernate/tag/list" class="btn btn-default"><button>
			<spring:message code="addUpdateTags" />
		</button></a>

</div>
