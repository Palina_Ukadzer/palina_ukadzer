<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<html>
<link href="<c:url value="/resources/newsList.css" />" type="text/css"
	rel="stylesheet">

<script src="<c:url value="/resources/validation.js" />"></script>
<body>
	<sf:form action="/news-admin-hibernate/news/filter"
		modelAttribute="searchCriteria">
		<div class="col-sm-9">
			<div class="col-sm-6">
				<c:choose>
					<c:when test="${not empty authors}">

						<sf:select path="authorId" class="selectpicker"
							style="margin-bottom: 0;">
							<option value="0"><spring:message code="anyAuthor" /></option>
							<c:choose>
								<c:when test="${ not empty searchCriteria.authorId}">

									<c:forEach items="${authors}" var="author">
										<c:choose>
											<c:when test="${searchCriteria.authorId == author.id}">
												<option value="${author.id}" selected><c:out
														value="${author.name}" /></option>
											</c:when>
											<c:otherwise>
												<option value="${author.id}"><c:out
														value="${author.name}" /></option>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</c:when>
								<c:otherwise>
									<c:forEach items="${authors}" var="author">
										<option value="${author.id}"><c:out
												value="${author.name}" /></option>
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</sf:select>
					</c:when>
					<c:otherwise>
						<select class="selectpicker">
							<option>No authors found</option>
						</select>
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${not empty tags}">

						<sf:select multiple="true" class="selectpicker" path="tagsIds">
							<sf:options items="${tags}" itemValue="id" itemLabel="name" />
						</sf:select>

					</c:when>

					<c:otherwise>
						<select name="tagsIds" multiple>
							<option>No tags found</option>
						</select>
					</c:otherwise>
				</c:choose>

				<a><button type="submit" class=" btn btn-info ">
						<spring:message code="filter" />
					</button></a> <a href="/news-admin-hibernate/news/reset">
					<div class="buttonLike">
						<spring:message code="reset" />
					</div>
				</a>



			</div>
		</div>
	</sf:form>

	<div class="col-sm-12">
		<c:choose>
			<c:when test="${not empty news}">
				<form action="/news-admin-hibernate/news/delete">
					<c:forEach items="${news}" var="news">

						<div class="col-sm-9">
							<div class="titleInfo">
								<a href="/news-admin-hibernate/news/${news.news.id}"><c:out
										value="${news.news.title}" /> </a>
							</div>
							<div class="authorInfo">(${news.author.name})</div>
							<div class="dateInfo">
								<fmt:message key="datePattern" var="datePattern" />
								<fmt:formatDate pattern="${datePattern}"
									value="${news.news.modificationDate}" />
							</div>

						</div>
						<div class="col-sm-9">
							<div class="shortTextInfo">
								<c:out value="${news.news.shortText}" />
							</div>

							<div class="tagsInfo">
								<c:forEach items="${news.news.tags}" var="tag">
									<c:out value="${tag.name}" />
								</c:forEach>
							</div>

							<div class="commentsInfo">
								<p>
									<spring:message code="comments" />
									(${fn:length(news.news.comments)})
								</p>
							</div>


							<div class="viewButton">
								<a href="/news-admin-hibernate/news/edit/${news.news.id}"> <spring:message
										code="edit" />

								</a>
							</div>
							<input type="checkbox" name="deleteId" value="${news.news.id}">
						</div>
					</c:forEach>
					<div class="col-sm-9 deleteButton">
						<button type="submit" class="btn bth-info">
							<spring:message code="delete" />
						</button>
					</div>

				</form>
			</c:when>
			<c:otherwise>
				<h1>No news found</h1>
			</c:otherwise>
		</c:choose>
	</div>



	<div class="col-sm-9" align="center">
		<ul class="pagination">
			<c:choose>
				<c:when test="${not empty pagesNum}">
					<%-- <c:forEach begin="1" end="${pagesNum}" var="i"> --%>
					<c:choose>
						<c:when test="${page == 1}">
						<a href="/news-admin-hibernate/news/list/1"><input
						class="myCoolPageIndex" type="submit" name="page" value="1"></a>
						<a href="/news-admin-hibernate/news/list/2"><input
						class="myCoolPageIndex" type="submit" name="page" value="2"></a>...
						<a href="/news-admin-hibernate/news/list/${pagesNum}"><input
						class="myCoolPageIndex" type="submit" name="page" value="${pagesNum}"></a>
						</c:when>
						<c:when test="${page == pagesNum}">
						<a href="/news-admin-hibernate/news/list/1"><input
						class="myCoolPageIndex" type="submit" name="page" value="1"></a>...
						<a href="/news-admin-hibernate/news/list/${pagesNum-1}"><input
						class="myCoolPageIndex" type="submit" name="page" value="${pagesNum-1}"></a>
						<a href="/news-admin-hibernate/news/list/${pagesNum}"><input
						class="myCoolPageIndex" type="submit" name="page" value="${pagesNum}"></a>
						</c:when>
						<c:otherwise>
       <a href="/news-admin-hibernate/news/list/1"><input
						class="myCoolPageIndex" type="submit" name="page" value="1"></a>...
						<a href="/news-admin-hibernate/news/list/${page-1}"><input
						class="myCoolPageIndex" type="submit" name="page" value="${page-1}"></a>
						<a href="/news-admin-hibernate/news/list/${page}"><input
						class="myCoolPageIndex" type="submit" name="page" value="${page}"></a>
						<a href="/news-admin-hibernate/news/list/${page+1}"><input
						class="myCoolPageIndex" type="submit" name="page" value="${page+1}"></a>...
						<a href="/news-admin-hibernate/news/list/${pagesNum}"><input
						class="myCoolPageIndex" type="submit" name="page"
						value="${pagesNum}"></a>
    </c:otherwise>
					</c:choose>


					</c:when>
				<c:otherwise>
					<input type="hidden" name="page" value="0">
				</c:otherwise>
			</c:choose>
		</ul>
	</div>


</body>
</html>
