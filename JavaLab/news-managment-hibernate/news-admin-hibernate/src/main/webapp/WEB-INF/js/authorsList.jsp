
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
 <script>	
		badAuthor = '<fmt:message key="badAuthor"/>';
</script> 

<link href="<c:url value="/resources/editUpdate.css" />" type="text/css"
	rel="stylesheet">
	<script src="<c:url value="/resources/validation.js" />"></script>
<body>
	<div class="panel panel-default">
		<div class="panel-body">
			<c:choose>
				<c:when test="${empty authorForEditId}">
					<c:forEach items="${authors}" var="authorCurrent">
						<form action="edit">
							<div class="col-sm-12">
								<div class=" wrapper">
									<c:out value="${authorCurrent.name}" />
								</div>
								<input type="hidden" name="id" value="${authorCurrent.id}">
								<button type="submit" class="editButton">
									<spring:message code="edit" />
								</button>
							</div>
						</form>
					</c:forEach>
				</c:when>
				<c:otherwise>
					<c:forEach items="${authors}" var="authorCurrent">
						<c:choose>
							<c:when test="${authorForEditId == authorCurrent.id}">
								<sf:form action="save" modelAttribute="author" method="post"
									onsubmit="return validate_author ();">
									<sf:input path="name" value="${authorCurrent.name}" id="author"
										class="wrapper" />
									<sf:errors path="name" cssClass="error" />
									<sf:hidden path="id" value="${authorCurrent.id}" />
									<button type="submit" class="editButton">
										<spring:message code="edit" />
									</button>
									<a href="/news-admin-hibernate/author/expire/${authorForEditId}"><div
											class="buttonLike">
											<spring:message code="expire" />
										</div></a>
									<a href="/news-admin-hibernate/author/cancel"><div
											class="buttonLike">
											<spring:message code="cancel" />
										</div></a>
								</sf:form>
							</c:when>
							<c:otherwise>
								<form action="edit" method="post">
									<div class="col-sm-12">
										<div class="wrapper">${authorCurrent.name}</div>
										<input type="hidden" name="id" value="${authorCurrent.id}">
										<button type="submit" class="editButton">
											<spring:message code="edit" />
										</button>
									</div>
								</form>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</c:otherwise>
			</c:choose>
			<sf:form action="add" method="post" modelAttribute="newAuthor"
				onsubmit="return validate_author ();">
				<div class="addField">
					<sf:input class="addWrapper" id="author" path="name"
						value="${newAuthor.name}" />
					<sf:errors path="name" cssClass="error" />
					<button type="submit" class="editButton">
						<spring:message code="add" />
					</button>

				</div>
			</sf:form>
		</div>
	</div>
</body>
</html>