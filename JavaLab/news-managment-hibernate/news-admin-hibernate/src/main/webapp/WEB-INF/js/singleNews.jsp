
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
<link href="<c:url value="/resources/singleNews.css" />" type="text/css"
	rel="stylesheet">
 <script>	
		badComment = '<fmt:message key="badComment"/>';
</script> 

<script src="<c:url value="/resources/validation.js" />"></script>

<body>
	<div class="dotted-border">


		<div class="panel panel-body">

			<div class="backButton">
				<a href="back"><button type="submit" class="btn btn-primary">
						<spring:message code="back" />
					</button></a>
			</div>

			<div class="col-sm-12">
				<div class="col-sm-10">
					<div class="newsTitle">
						<c:out value="${news.news.title}" />
					</div>
					<div class="authorName">
						(
						<c:out value="${news.news.author.name}" />
						)
					</div>
				</div>
				<div class="dateInfoSingle">
					<fmt:message key="datePattern" var="datePattern" />
					<fmt:formatDate pattern="${datePattern}"
						value="${news.news.modificationDate}" />
				</div>
			</div>
			<h4>
				<i><c:out value="${news.news.shortText}" /></i>
			</h4>
			<h4>
				<c:out value="${news.news.fullText}" />
			</h4>

			<c:choose>
				<c:when test="${not empty news.news.comments}">

					<c:forEach items="${news.news.comments}" var="comment">

						<form action="/news-admin-hibernate/comment/delete">
							<div class="commentSection">
								<div class="commentDate">
									<fmt:message key="datePattern" var="datePattern" />
									<fmt:formatDate pattern="${datePattern}"
										value="${comment.creationDate}"  />
								</div>
								<div class="commentText">
									<c:out value="${comment.text}" />
								</div>

								<input type="hidden" value="${comment.news.id}" name="newsId">
								<input type="hidden" value="${comment.id}" name="commentId">

								<div class="closeButton">
									<button type="submit">×</button>
								</div>
							</div>


						</form>

					</c:forEach>
				</c:when>
				<c:otherwise>
					<spring:message code="noCommentsYet" />
				</c:otherwise>
			</c:choose>



			<sf:form modelAttribute="comment"
				action="/news-admin-hibernate/comment/postComment"
				onsubmit="return validate_comment ();">
				<div class="col-sm-12">
					<sf:textarea id="comment_text" rows="2" cols="60" path="text" />
					<sf:hidden path="news.id" value="${news.news.id}" />
					<button type="submit" class="btn btn-primary">
						<spring:message code="postComment" />
					</button>
					<sf:errors path="text" cssClass="error" />
				</div>
			</sf:form> 
			<div class="col-sm-12">

 				<div class="prevNews">

					<c:if test="${previousNewsId != 0}">
						<form action="previous">
							<input type="hidden" name="previousNewsId"
								value="${previousNewsId}">
							<button type="submit" class="btn btn-info">
								<spring:message code="previousNews" />
							</button>
						</form>
					</c:if>
				</div>

				<div class="nextNews">
					<c:if test="${nextNewsId != 0}">
						<form action="next">
							<input type="hidden" name="nextNewsId" value="${nextNewsId}">
							<button type="submit" class="btn btn-info">
								<spring:message code="nextNews" />
							</button>
						</form>
					</c:if>
				</div> 

			</div>
		</div>
		<div class="panel-footer"></div>
	</div>
</body>
</html>