
<%-- <%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
 --%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<c:set var="language"
	value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
	scope="session" />
<fmt:setLocale value="${language}" />
<!DOCTYPE html>

<html>
<head>
<title>Login</title>
<link href="<c:url value="/resources/myClasses.css" />" type="text/css"
	rel="stylesheet">
<link href="<c:url value="/resources/loginStyle.css" />" type="text/css"
	rel="stylesheet">
</head>
<body>
	<div class="panel panel-heading">
		<h1>Mews portal</h1>
		<form>
			<button type="submit" id="language" name="language" value="ru">Ru</button>
			<button type="submit" id="language" name="language" value="en">En</button>
		</form>
	</div>
	<div class="loginForm">
		<div align="center">
			<c:url value="/login" var="loginUrl" />
			<form action="${loginUrl}" method="post"
				class="form-horizontal col-sm-12">
				<fieldset>
					<div class="col-sm-4"></div>
					<div class="panel panel-default form-group col-sm-4">
						<div class="panel-heading">
							<h1>
								<spring:message code="welcome" />
							</h1>
						</div>
						<div class="panel-body">
							<div class="login-field">
								<spring:message code="username" />
								: <input type='text' name='username' class="form-control"
									size="15">
							</div>

							<div>
								<spring:message code="password" />
								: <input class="form-control" type="password" size="15"
									name='password' />
							</div>
							<button type="submit" class="login-button">

								<spring:message code="login" />
							</button>
						</div>
					</div>
				</fieldset>
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
			</form>
		</div>
	</div>
</body>
</html>
