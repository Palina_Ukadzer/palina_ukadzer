var badTitle, badShort, badFull, badAuthor, badComment, badTag;

function validate_news( ) {
	
	if (document.getElementById("news_title").value.length < 1
			|| document.getElementById("news_title").value.length > 30) {
		alert(badTitle);
		return false;
	}
	if (document.getElementById("news_short").value.length < 1
			|| document.getElementById("news_short").value.length > 100) {
		alert(badShort);
		return false;
	}
	if (document.getElementById("news_full").value.length < 1
			|| document.getElementById("news_full").value.length > 2000) {
		alert(badFull);
		return false;
	}
	
	return true;
}


function validate_author() {
	if (document.getElementById("author").value.length < 1
			|| document.getElementById("author").value.length > 30) {
		alert(badAuthor);
		return false;
	}
	return true;
}



function validate_comment() {
	if (document.getElementById("comment_text").value.length < 1
			|| document.getElementById("comment_text").value.length > 100) {
		alert(badComment);
		return false;
	}
	return true;
}

function validate_tag(badTag) {
	if (document.getElementById("tag").value.length < 1
			|| document.getElementById("tag").value.length > 30) {
		alert(badTag);
		return false;
	}
	return true;
}




