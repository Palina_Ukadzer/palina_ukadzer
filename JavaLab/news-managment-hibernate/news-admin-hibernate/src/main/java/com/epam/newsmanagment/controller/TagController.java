package com.epam.newsmanagment.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.orm.hibernate4.HibernateOptimisticLockingFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.epam.ukadzer.entity.Tag;
import com.epam.ukadzer.service.TagService;
import com.epam.ukadzer.service.exception.ServiceException;

@Controller
@RequestMapping("/tag")
@PropertySource("classpath:constants.properties")
public class TagController {

	@Autowired
	private TagService tagService;

	@Value("${tagsList}")
	private String tagsList;

	public void setTagService(TagService tagService) {
		this.tagService = tagService;
	}

	@ExceptionHandler(ServiceException.class)
	public String handleServiceException() {
		return "/errorWithTagsService";
	}

	@ExceptionHandler(Exception.class)
	public String handleException() {
		return "/errorWithTags";
	}

	@RequestMapping("/list")
	public String seeAllTags(Model model) throws ServiceException {

		model.addAttribute("tags", tagService.getAllTags());
		if (!model.containsAttribute("tag")) {
			model.addAttribute("tag", new Tag());
		}
		if (!model.containsAttribute("newTag")) {
			model.addAttribute("newTag", new Tag());
		}
		return tagsList;

	}

	@RequestMapping("/edit")
	public String allowEditingTag(@RequestParam Long id, Model model,
			RedirectAttributes attr) throws ServiceException {
		attr.addFlashAttribute("tagForEditId", id);
		return "redirect:/tag/list";

	}

	@RequestMapping("/cancel")
	public String cancelEditing() throws ServiceException {
		return "redirect:/tag/list";

	}

	@RequestMapping("/save")
	public String saveTag(@Valid @ModelAttribute("tag") Tag tag,
			BindingResult bindingResult, Model model, RedirectAttributes attr)
			throws ServiceException {
		if (bindingResult.hasErrors()) {
			attr.addFlashAttribute(
					"org.springframework.validation.BindingResult.tag",
					bindingResult);
			attr.addFlashAttribute("tag", tag);
			attr.addFlashAttribute("tagForEditId", tag.getId());
			return "redirect:/tag/list";
		}
		try {
			tagService.update(tag);
		} catch (HibernateOptimisticLockingFailureException e) {
			return "redirect:/tag/list";
		}
		attr.addFlashAttribute("tagForEditId", null);
		return "redirect:/tag/list";
	}

	@RequestMapping("/delete/{tagId}")
	public String deleteTag(@PathVariable Long tagId, Model model,
			RedirectAttributes attr) throws ServiceException {
		try {
			tagService.delete(tagId);
		} catch (HibernateOptimisticLockingFailureException e) {
			return "redirect:/tag/list";
		}
		attr.addFlashAttribute("tagForEditId", null);
		return "redirect:/tag/list";

	}

	@RequestMapping("/add")
	public String addTag(@Valid @ModelAttribute("newTag") Tag newTag,
			BindingResult bindingResult, Model model, RedirectAttributes attr)
			throws ServiceException {

		if (bindingResult.hasErrors()) {
			attr.addFlashAttribute(
					"org.springframework.validation.BindingResult.newTag",
					bindingResult);
			attr.addFlashAttribute("newTag", newTag);
			return "redirect:/tag/list";
		}
		tagService.add(newTag);
		return "redirect:/tag/list";
	}

}
