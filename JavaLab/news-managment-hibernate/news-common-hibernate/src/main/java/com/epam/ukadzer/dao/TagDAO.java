package com.epam.ukadzer.dao;

import java.util.List;

import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.Tag;

public interface TagDAO extends CRUD<Tag>{
	
	
	/**
     * Method gets all the tags from db
     * @param all collection to get all object
     * @return collection with all objects
     */
    List<Tag> getAllTags() throws DAOException;
    
    
}