package com.epam.ukadzer.service;


import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.Comment;
import com.epam.ukadzer.service.exception.ServiceException;


public interface CommentService {
	
	/**
     * Method creates a new comment (saves it to db)
     * @param comment the comment to be saved
     * @returns id of created (saved) comment 
	 * @throws DAOException
     */
	public Long add(Comment comment, Long newsId) throws ServiceException;
	
	/**
     * Method deletes given comment
     * @param commentId id of comment to be deleted
	 * @throws ServiceException
     */	
	public void delete(Long commentId) throws ServiceException;
	
	/**
     * Method deletes all comments for news with given Id
     * @param newsId id of news which comments are to be deleted
	 * @throws ServiceException
     */
	/*public void deleteAllForNews(Long newsId) throws ServiceException;*/
	
	/**
     * Method gets all comments for news with given Id
     * @param newsId id of news which comments are to be got
	 * @throws ServiceException
     */
	/*public Set<Comment> getAllForNews(Long newsId) throws ServiceException;
	
	
	public void deleteAllForAllNews(List<Long> newsIds) throws ServiceException;*/

}
