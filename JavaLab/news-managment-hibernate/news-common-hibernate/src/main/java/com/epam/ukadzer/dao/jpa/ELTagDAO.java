package com.epam.ukadzer.dao.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.transaction.annotation.Transactional;

import com.epam.ukadzer.dao.TagDAO;
import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.Tag;

@Transactional(rollbackFor = Exception.class)
public class ELTagDAO implements TagDAO {

	@PersistenceContext
	private EntityManager em;

	@Override
	public Tag getById(Long entityId) throws DAOException {
		Tag tag = em.find(Tag.class, entityId);
		return tag;
	}

	@Override
	public Long add(Tag entity) throws DAOException {
		try {
			em.persist(entity);
			return entity.getId();
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public void update(Tag entity) throws DAOException {
		try {
			em.merge(entity);
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public void delete(Long entityId) throws DAOException {
		try {
			Tag tag = em.find(Tag.class, entityId);
			em.remove(tag);
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public List<Tag> getAllTags() throws DAOException {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Tag> cq = cb.createQuery(Tag.class);
		Root<Tag> rootEntry = cq.from(Tag.class);
		CriteriaQuery<Tag> all = cq.select(rootEntry);
		TypedQuery<Tag> allQuery = em.createQuery(all);
		return allQuery.getResultList();
	}

}
