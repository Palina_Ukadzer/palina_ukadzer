package com.epam.ukadzer.dao.jpa;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.transaction.annotation.Transactional;

import com.epam.ukadzer.dao.NewsDAO;
import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.News;
import com.epam.ukadzer.entity.SearchCriteria;

@Transactional(rollbackFor = Exception.class)
public class ELNewsDAO implements NewsDAO {

	@PersistenceContext
	private EntityManager em;

	private static final String SQL_GET_NEXT_NEWS = "select next_news_id from (select News_id, MODIFICATION_DATE, Lead(news_id, 1) over(order by modification_date desc) as next_news_id from ( select distinct n.news_id, n.MODIFICATION_DATE, count(c.comment_id) as comm_num from news n  LEFT OUTER JOIN NEWS_AUTHORS na on n.NEWS_ID=na.NEWS_ID  LEFT outer join NEWS_TAGS nt on n.NEWS_ID=nt.NEWS_ID  left OUTER JOIN comments c ON n.news_id = c.cm_news_id ";
	private static final String SQL_GET_PREVIOUS_NEWS = "select prev_news_id from (select News_id, MODIFICATION_DATE, Lag(news_id, 1) over(order by modification_date desc) as prev_news_id from ( select distinct n.news_id, n.MODIFICATION_DATE, count(c.comment_id) as comm_num from news n  LEFT OUTER JOIN NEWS_AUTHORS na on n.NEWS_ID=na.NEWS_ID  LEFT outer join NEWS_TAGS nt on n.NEWS_ID=nt.NEWS_ID  left OUTER JOIN comments c ON n.news_id = c.cm_news_id ";
	private static final String SQL_GET_PREV_NEXT_LAST_PART = " group by n.news_id, n.MODIFICATION_DATE)) where news_id=";
	private static final String SQL_WHERE_AUTHOR_ID = "where author_id=";
	private static final String SQL_AND_OPENIONG_BRACKET = "AND ( ";
	private static final String SQL_WHERE = " where ";
	private static final String SQL_TAG_ID_EQUALS = "tag_id=";
	private static final String SQL_OR_TAG_ID = "or tag_id=";
	private static final String SQL_CLOSING_BRACKET = ")";
	private static final String HQL_DELETE = "DELETE FROM News WHERE id IN :newsIds";

	@Override
	public News getById(Long entityId) throws DAOException {
		try {
			em.clear();
			News news = em.find(News.class, entityId);
			return news;
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public Long add(News entity) throws DAOException {
		SimpleDateFormat dateFormatGmt = new SimpleDateFormat(
				"yyyy-MMM-dd HH:mm:ss");
		dateFormatGmt.setTimeZone(TimeZone.getTimeZone("UTC"));
		SimpleDateFormat dateFormatLocal = new SimpleDateFormat(
				"yyyy-MMM-dd HH:mm:ss");
		News newNews = null;
		try {
			entity.setModificationDate(dateFormatLocal.parse(dateFormatGmt
					.format(new Date())));

			newNews = em.merge(entity);
		} catch (Exception e) {
			throw new DAOException(e);
		}
		return newNews.getId();
	}

	@Override
	public void update(News entity) throws DAOException {
		SimpleDateFormat dateFormatGmt = new SimpleDateFormat(
				"yyyy-MMM-dd HH:mm:ss");
		dateFormatGmt.setTimeZone(TimeZone.getTimeZone("UTC"));
		SimpleDateFormat dateFormatLocal = new SimpleDateFormat(
				"yyyy-MMM-dd HH:mm:ss");
		try {
			entity.setModificationDate(dateFormatLocal.parse(dateFormatGmt
					.format(new Date())));
			em.merge(entity);
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public void delete(Long entityId) throws DAOException {
		try {
			News news = em.find(News.class, entityId);
			em.remove(news);
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public int countAll() throws DAOException {
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<News> cq = cb.createQuery(News.class);
			Root<News> rootEntry = cq.from(News.class);
			CriteriaQuery<News> all = cq.select(rootEntry);
			TypedQuery<News> allQuery = em.createQuery(all);
			return allQuery.getResultList().size();
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public int countAllWithCriteria(SearchCriteria searchCriteria)
			throws DAOException {
		try {
			CriteriaQuery<Long> cq = getCountQuery(searchCriteria);
			long result_long = em.createQuery(cq).getSingleResult();
			return (int) result_long;
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public List<News> findAllWithCriteria(List<News> all,
			SearchCriteria searchCriteria) throws DAOException {
		try {
			CriteriaQuery<News> cq = getSearchQuery(searchCriteria);
			return em.createQuery(cq).getResultList();
		} catch (Exception e) {
			throw new DAOException(e);
		}

	}

	@Override
	public List<News> findSomeWithCriteria(List<News> all,
			SearchCriteria searchCriteria, int start, int finish)
			throws DAOException {
		List<News> news = null;
		try {
			CriteriaQuery<News> cq = getSearchQuery(searchCriteria);
			news = em.createQuery(cq).setFirstResult(start - 1)
					.setMaxResults(finish - start + 1).getResultList();
			em.clear();
		} catch (Exception e) {
			throw new DAOException(e);
		}
		return news;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Long getNextNewsId(Long newsId, SearchCriteria searchCriteria)
			throws DAOException {
		String queryString = SQL_GET_NEXT_NEWS;
		queryString += sqlSearchCriteriaLina(searchCriteria);
		queryString = queryString + SQL_GET_PREV_NEXT_LAST_PART + newsId;
		Query q = em.createNativeQuery(queryString);
		List results = q.getResultList();
		if (results.get(0) == null) {
			return 0L;
		}
		return ((BigDecimal) results.get(0)).longValue();

	}

	@SuppressWarnings("rawtypes")
	@Override
	public Long getPreviousNewsId(Long newsId, SearchCriteria searchCriteria)
			throws DAOException {
		String queryString = SQL_GET_PREVIOUS_NEWS;
		queryString += sqlSearchCriteriaLina(searchCriteria);
		queryString = queryString + SQL_GET_PREV_NEXT_LAST_PART + newsId;
		Query q = em.createNativeQuery(queryString);
		List results = q.getResultList();
		if (results.get(0) == null) {
			return 0L;
		}
		return ((BigDecimal) results.get(0)).longValue();

	}

	@Override
	public List<News> getAllNews() throws DAOException {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<News> cq = cb.createQuery(News.class);
		Root<News> rootEntry = cq.from(News.class);
		CriteriaQuery<News> all = cq.select(rootEntry);
		TypedQuery<News> allQuery = em.createQuery(all);
		return allQuery.getResultList();
	}

	@Override
	public void deleteNewsList(List<Long> newsIds) throws DAOException {
		try {
			Query query = em.createQuery(HQL_DELETE);
			query.setParameter("newsIds", newsIds);
			query.executeUpdate();
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	private CriteriaQuery<News> getSearchQuery(SearchCriteria searchCriteria) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<News> cq = cb.createQuery(News.class);
		Root<News> rootEntry = cq.from(News.class);
		List<Predicate> andPredicates = new ArrayList<Predicate>();
		List<Predicate> orPredicates = new ArrayList<Predicate>();
		if (searchCriteria.getAuthorId() != null) {
			andPredicates.add(cb.equal(rootEntry.get("author").get("id"),
					searchCriteria.getAuthorId()));
		}
		if (searchCriteria.getTagsIds() != null
				&& searchCriteria.getTagsIds().size() != 0) {
			for (Long id : searchCriteria.getTagsIds()) {
				orPredicates.add(cb.equal(rootEntry.get("tags").get("id"), id));
			}
		}
		if (andPredicates.size() > 0 && orPredicates.size() == 0) {
			cq.orderBy(cb.desc(rootEntry.get("modificationDate")),
					cb.asc(rootEntry.get("id")))
					.where(andPredicates.toArray(new Predicate[andPredicates
							.size()])).distinct(true);
		} else if (andPredicates.size() == 0 && orPredicates.size() > 0) {
			Predicate p = cb.conjunction();
			p = cb.or(orPredicates.toArray(new Predicate[orPredicates.size()]));
			cq.orderBy(cb.desc(rootEntry.get("modificationDate")),
					cb.asc(rootEntry.get("id"))).where(p).distinct(true);
		} else {
			Predicate o = cb.and(andPredicates
					.toArray(new Predicate[andPredicates.size()]));
			Predicate p = cb.or(orPredicates.toArray(new Predicate[orPredicates
					.size()]));
			cq.where(o, p)
					.orderBy(cb.desc(rootEntry.get("modificationDate")),
							cb.asc(rootEntry.get("id"))).distinct(true);

		}
		return cq;
	}

	private CriteriaQuery<Long> getCountQuery(SearchCriteria searchCriteria) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<News> rootEntry = cq.from(News.class);
		cq.select(cb.count(rootEntry));
		List<Predicate> andPredicates = new ArrayList<Predicate>();
		List<Predicate> orPredicates = new ArrayList<Predicate>();
		if (searchCriteria.getAuthorId() != null) {
			andPredicates.add(cb.equal(rootEntry.get("author").get("id"),
					searchCriteria.getAuthorId()));
		}
		if (searchCriteria.getTagsIds() != null
				&& searchCriteria.getTagsIds().size() != 0) {
			for (Long id : searchCriteria.getTagsIds()) {
				orPredicates.add(cb.equal(rootEntry.get("tags").get("id"), id));
			}
		}
		if (andPredicates.size() > 0 && orPredicates.size() == 0) {
			cq.where(andPredicates.toArray(new Predicate[andPredicates.size()]))
					.distinct(true);
		} else if (andPredicates.size() == 0 && orPredicates.size() > 0) {
			Predicate p = cb.conjunction();
			p = cb.or(orPredicates.toArray(new Predicate[orPredicates.size()]));
			cq.where(p).distinct(true);
		} else {
			Predicate o = cb.and(andPredicates
					.toArray(new Predicate[andPredicates.size()]));
			Predicate p = cb.or(orPredicates.toArray(new Predicate[orPredicates
					.size()]));
			cq.where(o, p).distinct(true);
		}
		return cq;
	}

	private String sqlSearchCriteriaLina(SearchCriteria searchCriteria) {
		String query = " ";
		boolean authorExists = false;
		if (searchCriteria.getAuthorId() != null) {
			authorExists = true;
			query += SQL_WHERE_AUTHOR_ID + searchCriteria.getAuthorId() + " ";
		}
		if (searchCriteria.getTagsIds() != null
				&& searchCriteria.getTagsIds().size() != 0) {
			Iterator<Long> itr = searchCriteria.getTagsIds().iterator();
			if (authorExists) {
				query += SQL_AND_OPENIONG_BRACKET;
			} else {
				query += SQL_WHERE;
			}
			int i = 0;
			while (itr.hasNext()) {
				if (i == 0) {
					query += SQL_TAG_ID_EQUALS + itr.next() + " ";
					i++;
				} else {
					query += SQL_OR_TAG_ID + itr.next() + " ";
				}
			}
			if (authorExists) {
				query += SQL_CLOSING_BRACKET;
			}
		}
		return query;
	}

}
