package com.epam.ukadzer.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import com.epam.ukadzer.dao.AuthorDAO;
import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.Author;
import com.epam.ukadzer.service.AuthorService;
import com.epam.ukadzer.service.exception.ServiceException;


@Transactional(rollbackFor=Exception.class)
public class AuthorServiceImpl implements AuthorService{


	private static final Logger logger = Logger.getLogger(AuthorServiceImpl.class);
	
	private AuthorDAO authorDAO; 
	
	
	public void setAuthorDAO(AuthorDAO authorDAO) {
		this.authorDAO = authorDAO;
	}
	
	
	public Long add(Author author) throws ServiceException {
		 try {
			return authorDAO.add(author);
		} catch (DAOException e) {
			logger.error("can't add an author");
			throw new ServiceException(e);
		}
	}

	

	
	public void expire(Author author) throws ServiceException {
		try {
			authorDAO.delete(author.getId());
		} catch (DAOException e) {
			logger.error("can't expire an author");
			throw new ServiceException(e);
		} 
		
	}
	
	
	public Author getById(Long id) throws ServiceException {
		 try {
			return authorDAO.getById(id);
		} catch (DAOException e) {
			logger.error("can't add an author");
			throw new ServiceException(e);
		}
	}
	
	
	public List<Author> getAllAuthors() throws ServiceException{
		try {
			return authorDAO.getAllAuthors();
		} catch (DAOException e) {
			logger.error("can't get all authors");
			throw new ServiceException(e);
		}
	}
	
	
	@Override
	public void update(Author author) throws ServiceException{
		try{
			authorDAO.update(author);;
		}catch (DAOException e) {
			logger.error("can't update author for news");
			throw new ServiceException(e);
		}
	}

}
