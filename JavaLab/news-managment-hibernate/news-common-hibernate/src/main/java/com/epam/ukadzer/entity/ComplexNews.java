package com.epam.ukadzer.entity;


import java.io.Serializable;
import java.util.Set;

import javax.validation.Valid;


public class ComplexNews implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Valid
	private News news;
	
	private Author author;
	
	private Set<Tag> tags;
	
	private Set<Comment> comments;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((news == null) ? 0 : news.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ComplexNews other = (ComplexNews) obj;
		if (news == null) {
			if (other.news != null)
				return false;
		} else if (!news.equals(other.news))
			return false;
		return true;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public Set<Tag> getTags() {
		return tags;
	}

	public void setTags(Set<Tag> tags) {
		this.tags = tags;
	}

	public Set<Comment> getComments() {
		return comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	public ComplexNews() {
		super();
	}

	public ComplexNews(News news) {
		super();
		this.news = news;
	}

	@Override
	public String toString() {
		return "ComplexNews [news=" + news + "]";
	}
	

}
