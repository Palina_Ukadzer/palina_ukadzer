package com.epam.ukadzer.service.impl;

import java.util.ArrayList;
import java.util.List;




import org.springframework.transaction.annotation.Transactional;

import com.epam.ukadzer.entity.ComplexNews;
import com.epam.ukadzer.entity.News;
import com.epam.ukadzer.entity.SearchCriteria;
import com.epam.ukadzer.service.AuthorService;
import com.epam.ukadzer.service.CommentService;
import com.epam.ukadzer.service.ComplexNewsService;
import com.epam.ukadzer.service.NewsService;
import com.epam.ukadzer.service.TagService;
import com.epam.ukadzer.service.exception.ServiceException;

@Transactional(rollbackFor=Exception.class)
public class ComplexNewsServiceImpl implements ComplexNewsService {

	NewsService newsService;

	TagService tagService;

	CommentService commentService;

	AuthorService authorService;

	public void setNewsService(NewsService newsService) {
		this.newsService = newsService;
	}

	public void setTagService(TagService tagService) {
		this.tagService = tagService;
	}

	public void setAuthorService(AuthorService authorService) {
		this.authorService = authorService;
	}

	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}

	@Override
	public Long add(ComplexNews complexNews) throws ServiceException {
		Long newsId = newsService.addNews(complexNews.getNews());		
		return newsId;
	}

	@Override
	public void delete(Long newsId) throws ServiceException {
		newsService.delete(newsId);
	}

	@Override
	public ComplexNews get(Long newsId) throws ServiceException {
		ComplexNews complexNews = new ComplexNews();
		News news = newsService.get(newsId);
		complexNews.setNews(news);
		complexNews.setAuthor(news.getAuthor());
		complexNews.setComments(news.getComments());
		complexNews.setTags(news.getTags());
		return complexNews;
	}

	@Override
	public void update(ComplexNews complexNews) throws ServiceException {
		News news = complexNews.getNews();
		newsService.edit(news);
	}

	@Override
	public List<ComplexNews> getSomeWithCriteria(SearchCriteria searchCriteria,
			int first, int last) throws ServiceException {
		List<ComplexNews> complexNews = new ArrayList<ComplexNews>();
		List<News> news = new ArrayList<News>();
		news = newsService.getSomeWithCriteria(news, searchCriteria, first,
				last);
		for (int i = 0; i < news.size(); i++) {
			complexNews.add(get(news.get(i).getId()));
		}
		return complexNews;
	}

	
	public void deleteNewsList(List<Long> newsIds) throws ServiceException {
		newsService.deleteNewsList(newsIds);
	}

}
