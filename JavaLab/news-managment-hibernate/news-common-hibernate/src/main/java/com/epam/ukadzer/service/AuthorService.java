package com.epam.ukadzer.service;


import java.util.List;

import com.epam.ukadzer.entity.Author;
import com.epam.ukadzer.service.exception.ServiceException;


public interface AuthorService {

	/**
     * Method creates a news author (saves it to db)
     * @param author the author to be saved
     * @returns id of created (saved) author 
	 * @throws ServiceException
     */
	public Long add(Author author) throws ServiceException;
	
	/**
     * Method updates a news author
     * @param author the author to be updated 
	 * @throws ServiceException
     */
	public void update(Author author) throws ServiceException;
	
	/**
     * Method expires given author
     * @param author the author to become expired
	 * @throws ServiceException
     */
	public void expire(Author author) throws ServiceException;

	/**
     * Method get an Author with given id
     * @param id id of author to be found
     * @returns found author 
	 * @throws ServiceException
     */
	public Author getById(Long id) throws ServiceException;
	
	
	/**
     * Method gets all authors
     * @param authors collection of authors where new will be put
     * @returns collection of found authors 
	 * @throws ServiceException
     */
	public List<Author> getAllAuthors() throws ServiceException;
}
