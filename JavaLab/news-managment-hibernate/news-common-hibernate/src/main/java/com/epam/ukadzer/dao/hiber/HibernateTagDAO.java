package com.epam.ukadzer.dao.hiber;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.ukadzer.dao.TagDAO;
import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.Tag;

@Transactional(rollbackFor = Exception.class)
public class HibernateTagDAO implements TagDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public Tag getById(Long entityId) throws DAOException {
		Session session = null;
		Tag tag = null;
		try {
			session = sessionFactory.getCurrentSession();
			tag = (Tag) session.get(Tag.class, entityId);
		} catch (Exception e) {
			throw new DAOException(e);
		}
		return tag;
	}

	public Long add(Tag entity) throws DAOException {
		Session session = null;
		Long tagId = null;
		try {
			session = sessionFactory.getCurrentSession();
			session.save(entity);
			tagId = entity.getId();
		} catch (Exception e) {
			throw new DAOException(e);
		}
		return tagId;
	}

	public void update(Tag entity) throws DAOException {
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			session.update(entity);
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	public void delete(Long entityId) throws DAOException {
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			Tag tag = (Tag) session.get(Tag.class, entityId);
			if (tag != null) {
				session.delete(tag);
			}
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@SuppressWarnings("unchecked")
	public List<Tag> getAllTags() throws DAOException {
		List<Tag> tags = new ArrayList<>();
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			tags = session.createCriteria(Tag.class).list();
		} catch (Exception e) {
			throw new DAOException(e);
		}
		return tags;
	}

}
