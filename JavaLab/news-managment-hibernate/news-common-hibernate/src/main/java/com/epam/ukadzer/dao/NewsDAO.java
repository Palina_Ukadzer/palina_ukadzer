package com.epam.ukadzer.dao;

import java.util.List;

import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.News;
import com.epam.ukadzer.entity.SearchCriteria;

public interface NewsDAO extends CRUD<News>{
	
	/**
    * Method counts all existing news
    * @returns number of news
	 * @throws DAOException
    */
	public int countAll() throws DAOException;
	
	/**
    * Method counts all existing news which characteristics match given criteria
    * @param searchCriteria the search criteria
    * @returns number of news
	 * @throws DAOException
    */
	public int countAllWithCriteria(SearchCriteria searchCriteria) throws DAOException;
	
	/**
     * Method finds all existing news which characteristics match given criteria
     * @param all list where news should be put
     * @param searchCriteria the search criteria
     * @returns list of found news
	 * @throws DAOException
     */
	public List<News> findAllWithCriteria(List<News> all,SearchCriteria searchCriteria) throws DAOException;
	
	
	/**
     * Method finds a number of existing news which characteristics match given criteria from start to finish
     * @param all list where news should be put
     * @param searchCriteria the search criteria
     * @param start the first row to get
     * @param finish the last row to get
     * @returns list of found news
	 * @throws DAOException
     */
	public List<News> findSomeWithCriteria(List<News> all,SearchCriteria searchCriteria, int start, int finish) throws DAOException;
	
	/**
     * Method gets a next news
     * @param newsId current news id
     * @returns next news
	 * @throws DAOException
     */
	public Long getNextNewsId(Long newsId, SearchCriteria searchCriteria) throws DAOException;
	
	/**
     * Method gets a previous news id
     * @param newsId current news id
     * @returns next news
	 * @throws DAOException
     */
	public Long getPreviousNewsId(Long newsId, SearchCriteria searchCriteria) throws DAOException;
	
	/**
     * Method gets all the news from db
     * @param all collection to get all object
     * @return collection with all objects
     */
    public List<News> getAllNews() throws DAOException;
    
    /**
     * Method deletes all news with given id's from db
     * @param entityId id of entity to be deleted
     */   
    public void deleteNewsList(List<Long> newsIds) throws DAOException;
	

}
