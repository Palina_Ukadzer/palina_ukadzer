package com.epam.ukadzer.dao.hiber;

import java.util.ArrayList;
import java.util.List;


import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.ukadzer.dao.CommentDAO;
import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.Comment;

@Transactional(rollbackFor=Exception.class)
public class HibernateCommentDAO implements CommentDAO {

	
	@Autowired
	private SessionFactory sessionFactory;

	public Comment getById(Long entityId) throws DAOException {
		Session session = null;
		Comment comment = null;
		try {
			session = sessionFactory.getCurrentSession();
			comment = (Comment) session.get(Comment.class, entityId);
		} catch (Exception e) {
			throw new DAOException(e);
		}
		return comment;
	}

	public Long add(Comment entity) throws DAOException {
		Session session = null;
		Long commentId = null;
		try {
			session = sessionFactory.getCurrentSession();
			session.save(entity);
			commentId = entity.getId();
		} catch (Exception e) {
			throw new DAOException(e);
		}
		return commentId;
	}

	public void update(Comment entity) throws DAOException {
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			session.update(entity);
		}catch (Exception e) {
			throw new DAOException(e);
		}

	}

	public void delete(Long entityId) throws DAOException {
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			Comment comment = (Comment) session.get(Comment.class, entityId);
			comment.getNews().getComments().remove(comment);
			if (comment != null) {
				session.delete(comment);
			}
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@SuppressWarnings("unchecked")
	public List<Comment> getAllComments() throws DAOException {
		List<Comment> comments = new ArrayList<>();
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			Query query = session.createQuery("from Comments");
			comments = query.list();
		} catch (Exception e) {
			throw new DAOException(e);
		}
		return comments;
	}



}
