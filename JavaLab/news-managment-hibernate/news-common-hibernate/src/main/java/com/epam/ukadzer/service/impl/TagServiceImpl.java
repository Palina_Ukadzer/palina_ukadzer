package com.epam.ukadzer.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import com.epam.ukadzer.dao.TagDAO;
import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.Tag;
import com.epam.ukadzer.service.TagService;
import com.epam.ukadzer.service.exception.ServiceException;

@Transactional(rollbackFor=Exception.class)
public class TagServiceImpl implements TagService {

	private static final Logger logger = Logger.getLogger(TagServiceImpl.class);

	private TagDAO tagDAO;
	
	public void setTagDAO(TagDAO tagDAO) {
		this.tagDAO = tagDAO;
	}

	
	
	public Long add(Tag tag) throws ServiceException {
		try {
			return tagDAO.add(tag);
		} catch (DAOException e) {
			logger.error("can't add tag");
			 throw new ServiceException(e);
		}		
	}



	
	@Override
	public List<Tag> getAllTags()
			throws ServiceException {
		try {
			return tagDAO.getAllTags();
		} catch (DAOException e) {
			logger.error("can't get tags");
			throw new ServiceException(e);
		}
	}
	
	
	@Override
	public Tag getById(Long id) throws ServiceException{
		try {
			return tagDAO.getById(id);
		} catch (DAOException e) {
			logger.error("can't get tag");
			throw new ServiceException(e);
		}
	}
	
	
	@Override
	public void update(Tag tag) throws ServiceException{
		try {
			tagDAO.update(tag);
		} catch (DAOException e) {
			logger.error("can't update tag");
			throw new ServiceException(e);
		}
	}
	
	
	@Override
	public void delete(Long id) throws ServiceException{
		try {
			tagDAO.delete(id);
		} catch (DAOException e) {
			logger.error("can't update tag");
			throw new ServiceException(e);
		}
	}

}
