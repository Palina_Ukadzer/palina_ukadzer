package com.epam.ukadzer.dao;

import java.util.List;

import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.Comment;

public interface CommentDAO extends CRUD<Comment>{
	

	
	/**
     * Method gets all the comments from db
     * @param all collection to get all object
     * @return collection with all objects
     */
    List<Comment> getAllComments() throws DAOException;
	
    
	
}
