package com.epam.ukadzer.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "TAGS")
public class Tag implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TAG_SEQUENCE")
	@SequenceGenerator(name="TAG_SEQUENCE", sequenceName="DRAGON.TAG_SEQUENCE", allocationSize = 1)	
	@Column(name = "TAG_ID", nullable = false, unique = true)
	private Long id;
	
	@Size(min=1, max=30, message="")
	@Column(name = "TAG_NAME", nullable = false)
	private String name;
	
	
	
	public Tag() {}
	
	public Tag(Long id, String name) {
		this.id=id;
		this.name=name;
	}

	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tag other = (Tag) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	public String getName() {
		return name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
    public String toString() {
        return "Tag whith id=" + id + "; name=" + name;
    }
}

    
