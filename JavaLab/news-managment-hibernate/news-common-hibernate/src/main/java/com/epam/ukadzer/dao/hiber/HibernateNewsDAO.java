package com.epam.ukadzer.dao.hiber;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;


import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.ukadzer.dao.NewsDAO;
import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.News;
import com.epam.ukadzer.entity.SearchCriteria;

@Transactional(rollbackFor=Exception.class)
public class HibernateNewsDAO implements NewsDAO {

	private static final String HQL_DELETE = "delete News where id in :newsIds";

	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public News getById(Long entityId) throws DAOException {
		Session session = null;
		News news = null;
		try {
			session = sessionFactory.getCurrentSession();
			List<News> newsList = (List<News>) session.createCriteria(News.class).add(Restrictions.eq("id", entityId)).list();
			if(newsList.size()!=0){
				return newsList.get(0);
			}
		} catch (Exception e) {
			throw new DAOException(e);
		}
		return news;
	}

	public Long add(News entity) throws DAOException {
		Session session = null;
		Long authorId = null;
		SimpleDateFormat dateFormatGmt = new SimpleDateFormat(
				"yyyy-MMM-dd HH:mm:ss");
		dateFormatGmt.setTimeZone(TimeZone.getTimeZone("UTC"));
		SimpleDateFormat dateFormatLocal = new SimpleDateFormat(
				"yyyy-MMM-dd HH:mm:ss");
		try {
			entity.setModificationDate(dateFormatLocal.parse(dateFormatGmt.format(new Date())));
			session = sessionFactory.getCurrentSession();
			session.save(entity);
			authorId = entity.getId();
		} catch (Exception e) {
			throw new DAOException(e);
		}
		return authorId;		
	}

	public void update(News entity) throws DAOException {
		Session session = null;
		SimpleDateFormat dateFormatGmt = new SimpleDateFormat(
				"yyyy-MMM-dd HH:mm:ss");
		dateFormatGmt.setTimeZone(TimeZone.getTimeZone("UTC"));
		SimpleDateFormat dateFormatLocal = new SimpleDateFormat(
				"yyyy-MMM-dd HH:mm:ss");
		try {
			entity.setModificationDate(dateFormatLocal.parse(dateFormatGmt.format(new Date())));
			session = sessionFactory.getCurrentSession();
			session.update(entity);
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	public void delete(Long entityId) throws DAOException {
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			News news = (News) session.get(News.class, entityId);
			if (news != null) {
				session.delete(news);
			}
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@SuppressWarnings("unchecked")
	public List<News> getAllNews() throws DAOException {
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			return session.createCriteria(News.class).list();
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	
	public int countAll() throws DAOException {
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			return (int) session.createCriteria(News.class).setProjection(Projections.rowCount()).uniqueResult();
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}


	@SuppressWarnings("unchecked")
	public List<News> findAllWithCriteria(List<News> all,
			SearchCriteria searchCriteria) throws DAOException {
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			DetachedCriteria detachedCriteria = setCriteriaParameters(searchCriteria);
			Criteria criteria = session.createCriteria(News.class);
			all = (List<News>)criteria.add(Subqueries.propertyIn("id", detachedCriteria)).list();
			return all;
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	public int countAllWithCriteria(SearchCriteria searchCriteria)
			throws DAOException {
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			DetachedCriteria detachedCriteria = setCriteriaParameters(searchCriteria);
			Criteria criteria = session.createCriteria(News.class);
			return (int)(long)criteria.add(Subqueries.propertyIn("id", detachedCriteria)).setProjection(Projections.rowCount()).uniqueResult();
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@SuppressWarnings("unchecked")
	public List<News> findSomeWithCriteria(List<News> all,
			SearchCriteria searchCriteria, int start, int finish)
			throws DAOException {
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			DetachedCriteria detachedCriteria = setCriteriaParameters(searchCriteria);
			Criteria criteria = session.createCriteria(News.class);
			all = (List<News>)criteria.addOrder(Order.desc("modificationDate")).setFirstResult(start).setMaxResults(finish-start+1).add(Subqueries.propertyIn("id", detachedCriteria)).list();
			return all;
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@SuppressWarnings("unchecked")
	public Long getNextNewsId(Long newsId, SearchCriteria searchCriteria)
			throws DAOException {
		Session session = null;
		Long foundId = 0L;
		try {
			session = sessionFactory.getCurrentSession();
			News currentNews =  (News) session.createCriteria(News.class).add(Restrictions.eq("id", newsId)).list().get(0);			
			DetachedCriteria detachedCriteria = setCriteriaParameters(searchCriteria);
			Criteria criteria = session.createCriteria(News.class);
			List<Long> all = (List<Long>) criteria.addOrder(Order.desc("modificationDate"))
					.add(Restrictions.eq("modificationDate", currentNews.getModificationDate())).
					add(Subqueries.propertyIn("id", detachedCriteria)).setProjection(Projections.property("id")).list();
			for(int i=0; i<all.size()-1; i++){
				if(all.get(i) == newsId){
					foundId = all.get(i+1);
					return foundId;
				}
			}
			criteria = session.createCriteria(News.class);
			List<Long> foundNews  = (List<Long>) criteria.addOrder(Order.desc("modificationDate")).
					add(Subqueries.propertyIn("id", detachedCriteria))
					.add(Restrictions.lt("modificationDate", currentNews.getModificationDate())).setProjection(Projections.property("id")).setMaxResults(1)
					.list();
			if(foundNews.size()!=0){
				return foundNews.get(0);
			}
		} catch (Exception e) {
			throw new DAOException(e);
		}
		return foundId;
	}

	@SuppressWarnings("unchecked")
	public Long getPreviousNewsId(Long newsId, SearchCriteria searchCriteria)
			throws DAOException {
		Session session = null;
		Long foundId = 0L;
		try {
			session = sessionFactory.getCurrentSession();
			News currentNews =  (News) session.createCriteria(News.class).add(Restrictions.eq("id", newsId)).list().get(0);			
			DetachedCriteria detachedCriteria = setCriteriaParameters(searchCriteria);
			Criteria criteria = session.createCriteria(News.class);
			List<Long> all = (List<Long>) criteria.addOrder(Order.asc("modificationDate"))
					.add(Restrictions.eq("modificationDate", currentNews.getModificationDate())).
					add(Subqueries.propertyIn("id", detachedCriteria)).setProjection(Projections.property("id")).list();
			for(int i=all.size(); i<0; i--){
				if(all.get(i) == newsId){
					foundId = all.get(i-1);
					return foundId;
				}
			}
			criteria = session.createCriteria(News.class);
			List<Long> foundNews  = (List<Long>) criteria.addOrder(Order.desc("modificationDate")).
					add(Subqueries.propertyIn("id", detachedCriteria))
					.add(Restrictions.gt("modificationDate", currentNews.getModificationDate())).setProjection(Projections.property("id")).setMaxResults(1)
					.list();
			if(foundNews.size()!=0){
				return foundNews.get(0);
			}
		} catch (Exception e) {
			throw new DAOException(e);
		}
		return foundId;
	}

	public void deleteNewsList(List<Long> newsIds) throws DAOException {
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			String hql = HQL_DELETE;
			Query query= session.createQuery(hql).setParameterList("newsIds", newsIds);
			query.executeUpdate();
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	

	
	private DetachedCriteria setCriteriaParameters(SearchCriteria searchCriteria){
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(News.class);
		if (searchCriteria.getAuthorId() != null) {
			detachedCriteria.add(Restrictions.like("author.id",searchCriteria.getAuthorId() ));
		}
		if (searchCriteria.getTagsIds() != null
				&& searchCriteria.getTagsIds().size() != 0) {
			detachedCriteria.createAlias("tags", "tagsAlias");
			detachedCriteria.add(Restrictions.in("tagsAlias.id", searchCriteria.getTagsIds()));
		}
		detachedCriteria.setProjection(Projections.distinct(Projections.property("id"))); 
		return detachedCriteria;
	}


}
