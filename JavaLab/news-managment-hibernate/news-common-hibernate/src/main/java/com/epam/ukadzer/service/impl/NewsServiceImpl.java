package com.epam.ukadzer.service.impl;

import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import com.epam.ukadzer.dao.NewsDAO;
import com.epam.ukadzer.dao.TagDAO;
import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.News;
import com.epam.ukadzer.entity.SearchCriteria;
import com.epam.ukadzer.entity.Tag;
import com.epam.ukadzer.service.NewsService;
import com.epam.ukadzer.service.exception.ServiceException;

@Transactional(rollbackFor=Exception.class)
public class NewsServiceImpl implements NewsService {

	private Logger logger = Logger.getLogger(NewsServiceImpl.class);
	
	private NewsDAO newsDAO;
	
	private TagDAO tagDAO;
	
	public void setNewsDAO(NewsDAO newsDAO) {
		this.newsDAO = newsDAO;
	}


	
	public void edit(News news) throws ServiceException {
		try {
			newsDAO.update(news);
		} catch (DAOException e) {
			logger.error("can't edit news");
			throw new ServiceException(e);
		}
		
	}
	
		public void addTagToNews(News news, Tag tag) throws ServiceException {
		try {
			Set<Tag> tags = news.getTags();
			tags.add(tag);
			news.setTags(tags);
			newsDAO.update(news);
		} catch (DAOException e) {
			logger.error("can't addTagToNews");
			throw new ServiceException(e);
		}
		
	}

	
	public void bindTags(Long newsId, Set<Long> tags) throws ServiceException {
		try {
			News news = newsDAO.getById(newsId);
			Set<Tag> fullTags = news.getTags();
			for (Long tagId: tags){
				fullTags.add(tagDAO.getById(tagId));
			}
			news.setTags(fullTags);
			newsDAO.update(news);
		} catch (DAOException e) {
			logger.error("Can't addTagsToNews");
			throw new ServiceException(e);
		}
		
	}


	
	public int countAll() throws ServiceException {
		try {
			return newsDAO.countAll();
		} catch (DAOException e) {
			logger.error("Cant count all news");
			throw new ServiceException(e);
		}
	}

	
	public int countAllWithCriteria(SearchCriteria searchCriteria) throws ServiceException {
		try {
			return newsDAO.countAllWithCriteria(searchCriteria);
		} catch (DAOException e) {
			logger.error("can't count all news with criteria");
			throw new ServiceException(e);
		}
	}


	
	public List<News> gettAllWithCriteria(List<News> news,
			SearchCriteria criteria) throws ServiceException {		
		try {
			return newsDAO.findAllWithCriteria(news, criteria);
		} catch (DAOException e) {
			logger.error("can't gat all with criteria");
			throw new ServiceException(e);
		}
	}

	
	public List<News> getSomeWithCriteria(List<News> news,
			SearchCriteria criteria, int start, int finish) throws ServiceException {		
		try {
			return newsDAO.findSomeWithCriteria(news, criteria, start, finish);
		} catch (DAOException e) {
			logger.error("cant get some with criteria");
			throw new ServiceException(e);
		}
	}


	
	public void delete(Long newsId) throws ServiceException {
		try {
			newsDAO.delete(newsId);
		} catch (DAOException e) {
			logger.error("can't delete news");
			throw new ServiceException(e);
		}
		
	}

	
	@Override
	public Long addNews(News news) throws ServiceException {
		try {
			return newsDAO.add(news);
		} catch (DAOException e) {
			logger.error("can't add news");
			throw new ServiceException(e);
		}
		
	}


	
	@Override
	public News get(Long newsId) throws ServiceException {
		try {
			return newsDAO.getById(newsId);
		} catch (DAOException e) {
			logger.error("can't delete news");
			throw new ServiceException(e);
		}
		
	}


	
	@Override
	public Long getNextNewsId(Long newsId, SearchCriteria searchCriteria) throws ServiceException {
		try {
			return newsDAO.getNextNewsId(newsId, searchCriteria);
		} catch (DAOException e) {
			logger.error("can't delete news");
			throw new ServiceException(e);
		}
	}
	
	
	
	@Override
	public Long getPreviousNewsId(Long newsId, SearchCriteria searchCriteria) throws ServiceException {
		try {
			return newsDAO.getPreviousNewsId(newsId, searchCriteria);
		} catch (DAOException e) {
			logger.error("can't delete news");
			throw new ServiceException(e);
		}
	}
	
	
	
	public void deleteNewsList(List<Long> newsIds) throws ServiceException{
		try {
			newsDAO.deleteNewsList(newsIds);
		} catch (DAOException e) {
			logger.error("can't delete news");
			throw new ServiceException(e);
		}
		
	}

	

}
