package com.epam.ukadzer.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

@Entity
@Table(name = "COMMENTS")
public class Comment implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COMM_SEQUENCE")
	@SequenceGenerator(name="COMM_SEQUENCE", sequenceName="DRAGON.COMM_SEQUENCE", allocationSize = 1)	
	@Column(name = "COMMENT_ID", nullable = false, unique = true)
	private Long id;
	
	
	@Size(min = 1 , max=100)
	@Column(name = "COMMENT_TEXT", nullable = false)
	private String text;	
	
	

	@Column(name = "CREATION_DATE", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date creationDate;
	
	
	
	@ManyToOne
	@JoinColumn(name = "CM_NEWS_ID", nullable=false)
	private News news ;
	
	

	public Comment() {}
	
	
	public Comment(Long id, Long newsId, String text, Date creationDate) {
		this.id = id;
		
		this.text = text;
		this.creationDate = creationDate;
	}
	
	public News getNews() {
		return news;
	}


	public void setNews(News news) {
		this.news = news;
	}


	
	
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public Date getCreationDate() {
		return creationDate;
	}
	
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}


	@Override
	public String toString() {
		return "Comment [id=" + id + ", text=" + text + ", creationDate="
				+ creationDate + ", news=" + news + "]";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((news == null) ? 0 : news.hashCode());
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comment other = (Comment) obj;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (news == null) {
			if (other.news != null)
				return false;
		} else if (!news.equals(other.news))
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		return true;
	}
	
	

    
}
