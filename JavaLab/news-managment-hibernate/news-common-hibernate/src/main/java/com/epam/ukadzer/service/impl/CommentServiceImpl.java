package com.epam.ukadzer.service.impl;


import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import com.epam.ukadzer.dao.CommentDAO;
import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.Comment;
import com.epam.ukadzer.service.CommentService;
import com.epam.ukadzer.service.NewsService;
import com.epam.ukadzer.service.exception.ServiceException;

@Transactional(rollbackFor=Exception.class)
public class CommentServiceImpl implements CommentService {

	private static final Logger logger = Logger.getLogger(CommentServiceImpl.class);
	
	private CommentDAO commentDAO;
	
	NewsService newsService;

	public void setCommentDAO(CommentDAO commentDAO) {
		this.commentDAO = commentDAO;
	}

	
	public Long add(Comment comment, Long newsId) throws ServiceException {
		try {			
			return commentDAO.add(comment);
		} catch (DAOException e) {
			logger.error("can't add a comment");
			throw new ServiceException(e);
		}
		
	}

	
	public void delete(Long commentId) throws ServiceException {
		try {
			commentDAO.delete(commentId);
		} catch (DAOException e) {
			logger.error("can't delete comment");
			throw new ServiceException(e);
		}
		
	}


}
