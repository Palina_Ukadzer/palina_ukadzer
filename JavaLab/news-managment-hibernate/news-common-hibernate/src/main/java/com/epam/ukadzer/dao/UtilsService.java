package com.epam.ukadzer.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.ukadzer.dao.exception.DAOException;


public class UtilsService {
	
	private static final Logger logger = Logger.getLogger(UtilsService.class);
	
	/**
     * Method releases connection, its statement and result set
	 * @throws SQLException
	 * @throws DAOException 
     */
	public static void closeConnection (Connection connection, Statement statement, ResultSet resultSet, 
			DataSource dataSource) throws DAOException{
		if(resultSet != null){
			try {
				resultSet.close();
			} catch (SQLException e) {
				throw new DAOException(e);
			}
		}
		if(statement != null){
			try {
				statement.close();
			} catch (SQLException e) {
				throw new DAOException(e);
			}
		}
		if(connection != null){
			try {
				DataSourceUtils.doReleaseConnection(connection, dataSource);
			}
			catch (SQLException ex) {
				logger.error("can't close connection", ex);
			}
		}		
	}
	
	/**
     * Method releases connection, its prepared statement and result set
	 * @throws SQLException
	 * @throws DAOException 
     */
	public static void closeConnection (Connection connection, PreparedStatement statement, ResultSet resultSet, 
			DataSource dataSource) throws DAOException{
		if(resultSet != null){
			try {
				resultSet.close();
			} catch (SQLException e) {
				throw new DAOException(e);
			}
		}
		if(statement != null){
			try {
				statement.close();
			} catch (SQLException e) {
				throw new DAOException(e);
			}
		}
		if(connection != null){
			try {
				DataSourceUtils.doReleaseConnection(connection, dataSource);
			}
			catch (SQLException ex) {
				logger.error("can't close connection", ex);
			}
		}		
	}

}
