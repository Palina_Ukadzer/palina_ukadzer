package com.epam.ukadzer.entity;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.UniqueConstraint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;


@Entity
@Table(name = "AUTHORS")
@NamedQueries({
	@NamedQuery(
	name = "expireAuthor",
	query = "update Author set expired = :newExpired where author_id = :oldId"
	)
})
public class Author implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ATH_SEQUENCE")
	@SequenceGenerator(name="ATH_SEQUENCE", sequenceName="DRAGON.ATH_SEQUENCE", allocationSize = 1)	
	@Column(name = "AUTHOR_ID", nullable = false, unique = true)
	private Long id;

	@Size(min=1, max=30)
	@Column(name = "AUTHOR_NAME", nullable = false)
	private String name;
	
	@Column(name = "EXPIRED", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date expired;
	
	public Author() {}
	
	public Author(Long id, String name, Date expired) {
		this.id = id;
		this.name=name;
		this.expired=expired;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Date getExpired() {
		return expired;
	}
	
	public void setExpired(Date expired) {
		this.expired = expired;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
    public String toString() {
        return "Author whith id=" + id + "; name=" + name +
                "; expired=" + expired;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((expired == null) ? 0 : expired.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Author other = (Author) obj;
		if (expired == null) {
			if (other.expired != null)
				return false;
		} else if (!expired.equals(other.expired))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

   
	
}
