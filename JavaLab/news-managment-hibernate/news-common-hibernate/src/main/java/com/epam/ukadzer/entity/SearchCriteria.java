package com.epam.ukadzer.entity;

import java.io.Serializable;
import java.util.Set;

public class SearchCriteria implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private Long authorId;
	
	private Set<Long> tagsIds; 
	
	public SearchCriteria(){}

	public SearchCriteria(Long authorId, Set<Long> tagsIds) {
		super();
		this.authorId = authorId;
		this.tagsIds = tagsIds;
	}


	@Override
	public String toString() {
		return "SearchCriteria [authorId=" + authorId + ", tagsIds=" + tagsIds
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((authorId == null) ? 0 : authorId.hashCode());
		result = prime * result + ((tagsIds == null) ? 0 : tagsIds.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SearchCriteria other = (SearchCriteria) obj;
		if (authorId == null) {
			if (other.authorId != null)
				return false;
		} else if (!authorId.equals(other.authorId))
			return false;
		if (tagsIds == null) {
			if (other.tagsIds != null)
				return false;
		} else if (!tagsIds.equals(other.tagsIds))
			return false;
		return true;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public Set<Long> getTagsIds() {
		return tagsIds;
	}

	public void setTagsIds(Set<Long> tagsIds) {
		this.tagsIds = tagsIds;
	}

	

}
