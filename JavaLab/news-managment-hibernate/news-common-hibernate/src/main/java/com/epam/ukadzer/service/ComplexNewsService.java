package com.epam.ukadzer.service;


import java.util.List;

import com.epam.ukadzer.entity.ComplexNews;
import com.epam.ukadzer.entity.SearchCriteria;
import com.epam.ukadzer.service.exception.ServiceException;

public interface ComplexNewsService {

	/**
	 * Method creates a complex news (saves it to db)
	 * 
	 * @param complex
	 *            news the complex news to be saved
	 * @throws ServiceException
	 */
	public Long add(ComplexNews complexNews) throws ServiceException;

	/**
	 * Method deletes a complex news
	 * 
	 * @param complex
	 *            news the complex news to be deleted
	 * @throws ServiceException
	 */
	public void delete(Long id) throws ServiceException;

	/**
	 * Method gets a complex news
	 * 
	 * @param newsId
	 *            id of news
	 * @return found complex news
	 * @throws ServiceException
	 */
	public ComplexNews get(Long newsId) throws ServiceException;

	/**
	 * Method updates a complex news
	 * 
	 * @param complex
	 *            news the complex news to be updated
	 * @throws ServiceException
	 */
	public void update(ComplexNews complexNews) throws ServiceException;

	/**
	 * Method gets a complex news list
	 * 
	 * @param newsId
	 *            id of news
	 * @return found complex news
	 * @throws ServiceException
	 */
	public List<ComplexNews> getSomeWithCriteria(SearchCriteria searchCriteria,
			int first, int last) throws ServiceException;
	
	
	/**
	 * Method deletes all complex news with given ids from db
	 * 
	 * @param complex
	 *            news the complex news to be deleted
	 * @throws ServiceException
	 */
	public void deleteNewsList(List<Long> newsIds) throws ServiceException;

}
