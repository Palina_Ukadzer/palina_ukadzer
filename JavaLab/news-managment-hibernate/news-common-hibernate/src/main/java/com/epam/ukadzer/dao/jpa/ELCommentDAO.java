package com.epam.ukadzer.dao.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.transaction.annotation.Transactional;

import com.epam.ukadzer.dao.CommentDAO;
import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.Comment;

@Transactional(rollbackFor = Exception.class)
public class ELCommentDAO implements CommentDAO {

	@PersistenceContext
	private EntityManager em;

	@Override
	public Comment getById(Long entityId) throws DAOException {
		try {
			Comment comment = em.find(Comment.class, entityId);
			return comment;
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public Long add(Comment entity) throws DAOException {
		try {
			em.persist(entity);
			return entity.getId();
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public void update(Comment entity) throws DAOException {
		try {
			em.merge(entity);
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public void delete(Long entityId) throws DAOException {
		try {
			Comment comment = em.find(Comment.class, entityId);
			em.remove(comment);
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public List<Comment> getAllComments() throws DAOException {
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<Comment> cq = cb.createQuery(Comment.class);
			Root<Comment> rootEntry = cq.from(Comment.class);
			CriteriaQuery<Comment> all = cq.select(rootEntry);
			TypedQuery<Comment> allQuery = em.createQuery(all);
			return allQuery.getResultList();
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

}
