package com.epam.ukadzer.dao.hiber;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.ukadzer.dao.AuthorDAO;
import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.Author;

@Transactional(rollbackFor = Exception.class)
public class HibernateAuthorDAO implements AuthorDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public Author getById(Long entityId) throws DAOException {
		Session session = null;
		Author author = null;
		try {
			session = sessionFactory.getCurrentSession();
			author = (Author) session.get(Author.class, entityId);
		} catch (Exception e) {
			throw new DAOException(e);
		}
		return author;
	}

	public void update(Author entity) throws DAOException {
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			session.update(entity);
		}catch (Exception e) {
			throw new DAOException(e);
		}
	}

	public void delete(Long entityId) throws DAOException {
		Session session = null;
		try {
			SimpleDateFormat dateFormatGmt = new SimpleDateFormat(
					"yyyy-MMM-dd HH:mm:ss");
			dateFormatGmt.setTimeZone(TimeZone.getTimeZone("UTC"));
			SimpleDateFormat dateFormatLocal = new SimpleDateFormat(
					"yyyy-MMM-dd HH:mm:ss");
			session = sessionFactory.getCurrentSession();
			session.getNamedQuery("expireAuthor")
					.setTimestamp(
							"newExpired",
							(dateFormatLocal.parse(dateFormatGmt
									.format(new Date()))))
					.setLong("oldId", entityId).executeUpdate();
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@SuppressWarnings("unchecked")
	public List<Author> getAllAuthors() throws DAOException {
		List<Author> authors = new ArrayList<Author>();
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			authors = session.createCriteria(Author.class).list();
		} catch (Exception e) {
			throw new DAOException(e);
		}
		return authors;
	}

	public Long add(Author entity) throws DAOException {
		Session session = null;
		Long authorId = null;
		try {
			session = sessionFactory.getCurrentSession();
			session.save(entity);
			authorId = entity.getId();
		} catch (Exception e) {
			throw new DAOException(e);
		}
		return authorId;
	}

}
