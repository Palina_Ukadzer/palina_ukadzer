package com.epam.ukadzer.dao.jpa;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.transaction.annotation.Transactional;

import com.epam.ukadzer.dao.AuthorDAO;
import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.Author;

@Transactional(rollbackFor = Exception.class)
public class ELAuthorDAO implements AuthorDAO {

	@PersistenceContext
	private EntityManager em;

	@Override
	public Author getById(Long entityId) throws DAOException {
		try {
			Author author = em.find(Author.class, entityId);
			return author;
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public Long add(Author entity) throws DAOException {
		try {
			em.persist(entity);
			return entity.getId();
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public void update(Author entity) throws DAOException {
		try {
			em.merge(entity);
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public void delete(Long entityId) throws DAOException {
		SimpleDateFormat dateFormatGmt = new SimpleDateFormat(
				"yyyy-MMM-dd HH:mm:ss");
		dateFormatGmt.setTimeZone(TimeZone.getTimeZone("UTC"));
		SimpleDateFormat dateFormatLocal = new SimpleDateFormat(
				"yyyy-MMM-dd HH:mm:ss");
		try {
			em.createNamedQuery("expireAuthor")
					.setParameter(
							"newExpired",
							(dateFormatLocal.parse(dateFormatGmt
									.format(new Date()))))
					.setParameter("oldId", entityId).executeUpdate();
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public List<Author> getAllAuthors() throws DAOException {
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<Author> cq = cb.createQuery(Author.class);
			Root<Author> rootEntry = cq.from(Author.class);
			CriteriaQuery<Author> all = cq.select(rootEntry);
			TypedQuery<Author> allQuery = em.createQuery(all);
			return allQuery.getResultList();
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

}
