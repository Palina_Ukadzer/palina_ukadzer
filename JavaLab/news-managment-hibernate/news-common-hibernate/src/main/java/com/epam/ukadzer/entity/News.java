package com.epam.ukadzer.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name = "NEWS")
public class News implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "NEWS_SEQUENCE")
	@SequenceGenerator(name="NEWS_SEQUENCE", sequenceName="DRAGON.NEWS_SEQUENCE", allocationSize = 1)
	@Column(name = "NEWS_ID", nullable = false, unique = true)
	private Long id;
	
	@Size(min=1, max=30)
	@Column(name = "TITLE", nullable = false)
	private String title;
	
	@Size(min=1, max=100)
	@Column(name = "SHORT_TEXT", nullable = false)
	private String shortText;
	
	@Size(min=1, max=2000)
	@Column(name = "FULL_TEXT", nullable = false)
	private String fullText;
	
	@NotNull
	@Column(name = "CREATION_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;
	
	@Column(name = "MODIFICATION_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date modificationDate;
	
	
	@ManyToOne
	@JoinTable(name = "NEWS_AUTHORS", joinColumns = {
	        @JoinColumn(name = "NEWS_ID")}, inverseJoinColumns = {
	        @JoinColumn(name = "AUTHOR_ID")})
	private Author author;
	
	@LazyCollection(value=LazyCollectionOption.FALSE)
	@OneToMany(targetEntity=Comment.class,orphanRemoval = true)
	@JoinColumn(name = "cm_NEWS_ID", referencedColumnName="news_id")
	private Set<Comment> comments = new HashSet<Comment>(0);

	@ManyToMany(targetEntity = Tag.class)
	@LazyCollection(LazyCollectionOption.FALSE)
	@JoinTable(name = "NEWS_TAGS", joinColumns={
			@JoinColumn(name = "NEWS_ID")}, inverseJoinColumns = {
	        @JoinColumn(name = "TAG_ID")})
	private Set<Tag> tags = new HashSet<Tag>(0);
	
	@Version
	private Long version;
	

	public News(Long id, String title, String shortText, String fullText,
			Date creationDate, Date modificationDate, Author author,
			Set<Comment> comments, Set<Tag> tags) {
		super();
		this.id = id;
		this.title = title;
		this.shortText = shortText;
		this.fullText = fullText;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
		this.author = author;
		this.comments = comments;
		this.tags = tags;
	}



	public News() {}

	
	
	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}
	
	public Set<Comment> getComments() {
		return comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	public Set<Tag> getTags() {
		return tags;
	}

	public void setTags(Set<Tag> tags) {
		this.tags = tags;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getFullText() {
		return fullText;
	}
	
	public void setFullText(String fullText) {
		this.fullText = fullText;
	}
	
	public String getShortText() {
		return shortText;
	}
	
	public void setShortText(String shortText) {
		this.shortText = shortText;
	}
	
	public Date getCreationDate() {
		return creationDate;
	}
	
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}
	
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result
				+ ((fullText == null) ? 0 : fullText.hashCode());
		result = prime
				* result
				+ ((modificationDate == null) ? 0 : modificationDate.hashCode());
		result = prime * result
				+ ((shortText == null) ? 0 : shortText.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		News other = (News) obj;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (fullText == null) {
			if (other.fullText != null)
				return false;
		} else if (!fullText.equals(other.fullText))
			return false;
		if (modificationDate == null) {
			if (other.modificationDate != null)
				return false;
		} else if (!modificationDate.equals(other.modificationDate))
			return false;
		if (shortText == null) {
			if (other.shortText != null)
				return false;
		} else if (!shortText.equals(other.shortText))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	@Override
    public String toString() {
        return "News with [id=" + this.getId() + "; title=" + title +
                "; short text=" + shortText + "; full text=" + fullText + 
                "; ceation date=" + creationDate + "; modification date=" + modificationDate+"], ["+author+
                "], comments "+comments+", tags "+tags;
    }

   

}
