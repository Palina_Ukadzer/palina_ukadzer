package com.epam.ukadzer.dao.hiber;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.ukadzer.dao.UserDAO;
import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.entity.User;

@Transactional
public class HibernateUserDAO implements UserDAO {


	@Autowired
	private SessionFactory sessionFactory;


	public User getById(Long entityId) throws DAOException {
		Session session = null;
		User user = null;
		try {
			session = sessionFactory.getCurrentSession();
			user = (User) session.get(User.class, entityId);
		} catch (Exception e) {
			throw new DAOException(e);
		}
		return user;
	}

	public void update(User entity) throws DAOException {
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			session.update(entity);
		} catch (Exception e) {
			throw new DAOException(e);
		}

	}

	public void delete(Long entityId) throws DAOException {
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			User user = (User) session.get(User.class, entityId);
			if (user != null) {
				session.delete(user);
			}
		} catch (Exception e) {
			throw new DAOException(e);
		}

	}

	public Long add(User entity) throws DAOException {
		Session session = null;
		Long userId = null;
		try {
			session = sessionFactory.getCurrentSession();
			session.save(entity);
			userId = entity.getId();
		} catch (Exception e) {
			throw new DAOException(e);
		}
		return userId;
	}


}
