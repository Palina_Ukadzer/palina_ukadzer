package com.epam.ukadzer.service;

import com.epam.ukadzer.entity.User;
import com.epam.ukadzer.service.exception.ServiceException;

public interface UserService {
	
	/**
     * Method creates an user (saves it to db)
     * @param user the user to be saved
     * @returns id of created (saved) user 
	 * @throws ServiceException
     */
	public Long add(User user) throws ServiceException;
	
	/**
     * Method get an User with given id
     * @param id id of user to be found
     * @returns found author 
	 * @throws ServiceException
     */
	public User getById(Long id) throws ServiceException;
	
	/**
     * Method finds if user with given name and password exists
     * @param name user name
     * @param password user password
     * @returns true if user exists, false- otherwise
	 * @throws ServiceException
     */
	public boolean findIfExists(String name, String password) throws ServiceException;

}
