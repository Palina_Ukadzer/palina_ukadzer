package com.epam.ukadzer.dao.impl;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.dao.jpa.ELAuthorDAO;
import com.epam.ukadzer.entity.Author;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring-test.xml" })
@ActiveProfiles("el")
public class AuthorDAOTest {

	@Autowired
	ELAuthorDAO authorDAO;
	
	
	@Test
	@DatabaseSetup(value = "verycooldataset.xml")
	public void testGetById() {
		Author author = null;
		try {
			author = authorDAO.getById(1L);
		} catch (DAOException e) {
			Assert.fail();
		}
		Assert.assertEquals("Palina Ukadzer", author.getName());
	}

	@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/author/dataset.xml")
	@Test
	public void testGetByIdNull() {
		Author author = null;
		try {
			author = authorDAO.getById(6L);
		} catch (DAOException e) {
			Assert.fail();
		}
		Assert.assertNull(author);
	}

	@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/author/dataset.xml")
	@ExpectedDatabase(value = "/com/epam/ukadzer/dao/impl/author/expected-database-save.xml")
	@Test
	public void testSave() throws DAOException {
		try {
			Author author = new Author(null, "bbb", null);
			authorDAO.add(author);
		} catch (DAOException e) {
			Assert.fail();
		}
		
	}

	@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/author/dataset.xml")
	@ExpectedDatabase(value = "/com/epam/ukadzer/dao/impl/author/expected-database-update.xml")
	@Test
	public void testUpdate() throws DAOException {
		Author author = new Author(1L, "Palina Ukadzer", null);
		authorDAO.update(author);

	}
	
	@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/author/dataset.xml")
	@ExpectedDatabase(value = "/com/epam/ukadzer/dao/impl/author/expected-database-save.xml")
	@Test
	public void testAdd() throws DAOException {
		Author author = new Author(1L, "new_author", null);
		authorDAO.add(author);

	}
	
	@Test(expected=DAOException.class)
	public void testExceptionOnTooLongText() throws DAOException {
		Author author = new Author(1L, "1234567890123456789012345678901234567890", null);
		authorDAO.add(author);

	}
	
	@Test
	@DatabaseSetup(value = "verycooldataset.xml")
	public void testGetAll() {
		List<Author> authors = null;
		try {
			authors = authorDAO.getAllAuthors();
		} catch (DAOException e) {
			Assert.fail();
		}
		Assert.assertEquals("Palina Ukadzer", null);
	}

}
