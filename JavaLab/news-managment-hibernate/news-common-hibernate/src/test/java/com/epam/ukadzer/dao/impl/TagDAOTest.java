package com.epam.ukadzer.dao.impl;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.dao.hiber.HibernateTagDAO;
import com.epam.ukadzer.entity.Tag;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring-test.xml" })
public class TagDAOTest {
	@Autowired
	HibernateTagDAO tagDAO;

	@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/tag/dataset.xml")
	@Test
	public void testGetById() throws DAOException {
		Tag tag = null;
		tag = tagDAO.getById(4L);
		Assert.assertEquals("Tag4", tag.getName());
	}

	@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/tag/dataset.xml")
	@Test
	public void testGetByIdNull() throws DAOException {
		Tag tag = null;
		tag = tagDAO.getById(6L);
		Assert.assertNull(tag);
	}

	@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/tag/dataset.xml")
	@ExpectedDatabase(value = "/com/epam/ukadzer/dao/impl/tag/expected-database-save.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	@Test
	public void testSave() throws DAOException {
		Tag tag = new Tag(1L, "tag_new");
		tagDAO.add(tag);
	}

	@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/tag/dataset.xml")
	@ExpectedDatabase(value = "/com/epam/ukadzer/dao/impl/tag/expected-database-update.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	@Test
	public void testUpdate() throws DAOException {
		Tag tag = new Tag(1L, "tag_update");
		tagDAO.update(tag);

	}

	@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/tag/dataset.xml")
	@ExpectedDatabase(value = "/com/epam/ukadzer/dao/impl/tag/expected-database-delete.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	@Test
	public void testDelete() throws DAOException {
		tagDAO.delete(1L);
	}


	@Test(expected = DAOException.class)
	public void testExceptionOnTooLongText() throws DAOException {
		Tag tag = new Tag(1L, "1234567890123456789012345678901234567890");
		tagDAO.add(tag);

	}

}
