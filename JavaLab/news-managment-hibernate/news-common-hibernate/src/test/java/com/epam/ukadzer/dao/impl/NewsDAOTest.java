package com.epam.ukadzer.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.epam.ukadzer.dao.exception.DAOException;
import com.epam.ukadzer.dao.hiber.HibernateAuthorDAO;
import com.epam.ukadzer.dao.hiber.HibernateNewsDAO;
import com.epam.ukadzer.dao.hiber.HibernateTagDAO;
import com.epam.ukadzer.dao.jpa.ELNewsDAO;
import com.epam.ukadzer.entity.Author;
import com.epam.ukadzer.entity.News;
import com.epam.ukadzer.entity.SearchCriteria;
import com.epam.ukadzer.entity.Tag;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring-test.xml" })
@ActiveProfiles("hiber")
public class NewsDAOTest {

	@Autowired
	HibernateNewsDAO newsDAO;
	
	@Autowired
	HibernateAuthorDAO authorDAO;
	
	@Autowired
	HibernateTagDAO tagDAO;

	@Test
	public void testGetById() {
		News news = null;
		try {
			news = newsDAO.getById(1L);
		} catch (DAOException e) {
			Assert.fail();
		}
		Assert.assertEquals("title1", news.getTitle());
		Assert.assertEquals("Palina Ukadzer", news.getAuthor().getName());
		Assert.assertEquals(4, news.getTags().size());
		Assert.assertEquals(0, news.getComments().size());
	}
	
	@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/news/dataset.xml")
	@ExpectedDatabase(value = "/com/epam/ukadzer/dao/impl/news/expected-database-save.xml")
	@Test
	public void testSave() throws DAOException {
		Set<Long> tags = new HashSet<>();
		tags.add(2L);
		SearchCriteria searchCriteria = new SearchCriteria(1L, tags);
		newsDAO.getNextNewsId(1L, searchCriteria);

	}
	

	/*@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/news/dataset.xml")
	@Test
	public void testGetById() throws DAOException {
		News news = null;
		news = newsDAO.getById(1L);
		Assert.assertEquals(news, null);
	}

	@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/news/dataset.xml")
	@Test
	public void testGetByIdNull() throws DAOException {
		News news = new News();
		news = newsDAO.getById(17L);
		Assert.assertNull(news);

	}

	@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/news/dataset.xml")
	@ExpectedDatabase(value = "/com/epam/ukadzer/dao/impl/news/expected-database-save.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	@Test
	public void testSave() throws DAOException {
		News news = new News(1L, "title_test", "shortText_test",
				"fullText_test", new Timestamp(myMagicalDate), new Date(
						myMagicalDate));
		newsDAO.add(news);

	}*/

	/*@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/news/dataset.xml")
	@ExpectedDatabase(value = "/com/epam/ukadzer/dao/impl/news/expected-database-update.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	@Test
	public void testUpdate() throws DAOException {
		News news = new News(1L, "title_update", "shortText_update",
				"fullText_update", new Timestamp(myMagicalDate), new Date(
						myMagicalDate));
		newsDAO.update(news);

	}

	@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/news/dataset.xml")
	@ExpectedDatabase(value = "/com/epam/ukadzer/dao/impl/news/expected-database-delete.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	@Test
	public void testDelete() throws DAOException {
		newsDAO.delete(1L);
	}*/
	
	
	@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/news/dataset.xml")
	@Test
	public void testTimeDifference() throws DAOException {
		/*long start = System.nanoTime();    
		newsDAO.getNextNewsId(13945L, new SearchCriteria());
		long elapsedTime = System.nanoTime() - start;
		System.out.println("Time for method with modificationDate in the begining: "+elapsedTime);
		
		start = System.nanoTime();    
		newsDAO.getPreviousNewsId(13945L, new SearchCriteria());
		elapsedTime = System.nanoTime() - start;
		System.out.println("Time for method with id's in the begining: "+elapsedTime);
		
		start = System.nanoTime();    
		newsDAO.getNextNewsId(23940L, new SearchCriteria());
		elapsedTime = System.nanoTime() - start;
		System.out.println("Time for method with modificationDate in the end: "+elapsedTime);
		
		start = System.nanoTime();    
		newsDAO.getPreviousNewsId(23940L, new SearchCriteria());
		elapsedTime = System.nanoTime() - start;
		System.out.println("Time for method with ids in the end: "+elapsedTime);
		
		start = System.nanoTime();    
		newsDAO.getNextNewsId(18942L, new SearchCriteria());
		elapsedTime = System.nanoTime() - start;
		System.out.println("Time for method with modificationDate in the middle: "+elapsedTime);
		
		start = System.nanoTime();    
		newsDAO.getPreviousNewsId(18942L, new SearchCriteria());
		elapsedTime = System.nanoTime() - start;
		System.out.println("Time for method with ids in the middle: "+elapsedTime);
		int t=0;
		for(int i=13945; i<14545; i++){
			start = System.nanoTime();    
			newsDAO.getNextNewsId((long)i, new SearchCriteria());
			elapsedTime = System.nanoTime() - start;
			long timeOne = elapsedTime;
			
			start = System.nanoTime();    
			newsDAO.getPreviousNewsId((long)i, new SearchCriteria());
			elapsedTime = System.nanoTime() - start;
			long timeTwo = elapsedTime;
			if(timeOne<timeTwo){
				t++;
			}else if (timeTwo<timeOne) {
				t--;
			}
		}
		if(t>0){
			System.out.println("Use modification  (t="+t+")");
		}else {
			System.out.println("Use ids  (t="+t+")");
		}*/
		
	}

	@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/news/dataset.xml")
	@Test
	public void testFindAllWithCriteria() throws DAOException {
		List<News> list = new ArrayList<News>();
		Set<Long> tags = new HashSet<>();
		tags.add(2L);
		SearchCriteria searchCriteria = new SearchCriteria(1L, tags);
		list = newsDAO.findAllWithCriteria(list, searchCriteria);
		Assert.assertEquals(0, list.size());
	}

	/*@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/news/dataset.xml")
	@Test
	public void testCountAllWithCriteria() throws DAOException {
		Set<Long> tags = new HashSet<>();
		tags.add(2L);
		SearchCriteria searchCriteria = new SearchCriteria(1L, tags);
		int result = 0;
		result = newsDAO.countAllWithCriteria(searchCriteria);
		Assert.assertEquals(0, result);
	}*/

	@Test
	public void testgetSomeWithCriteria() throws DAOException {
		List<News> list = new ArrayList<News>();
		SearchCriteria searchCriteria = new SearchCriteria(1L, null);
		list = newsDAO.findSomeWithCriteria(list, searchCriteria, 1, 3);
		System.out.print(list.get(1).getAuthor());
		Assert.assertEquals(0, list.size());
	}
	
	@Test
	public void testgetPreoiousId() throws DAOException {
		SearchCriteria searchCriteria = new SearchCriteria(1L, null);
		Long nextId = newsDAO.getPreviousNewsId(2L, searchCriteria);
		System.out.print(nextId);
		Assert.assertNotNull(nextId);
	}
	
	@Test
	public void testdelete() throws DAOException {
		List<Long> ids = new ArrayList<Long>(); 
		ids.add(2L);
		newsDAO.deleteNewsList(ids);
	}

	/*@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/news/dataset.xml")
	@ExpectedDatabase(value = "/com/epam/ukadzer/dao/impl/news/expected-database-delete-link-author.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	@Test
	public void testDeleteLinkToAuthor() throws DAOException {
		newsDAO.deleteLinkToAuthor(1L);
	}

	@DatabaseSetup(value = "/com/epam/ukadzer/dao/impl/news/dataset.xml")
	@ExpectedDatabase(value = "/com/epam/ukadzer/dao/impl/news/expected-database-delete-link-tags.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
	@Test
	public void testDeleteLinkToTags() throws DAOException {
		newsDAO.deleteLinkToTags(1L);
	}
	
	@Test(expected = DAOException.class)
	public void testExceptionOnTooLongTitle() throws DAOException {
		News news = new News(1L, "11111111111111111111111111111111", "shortText_test",
				"fullText_test", new Timestamp(myMagicalDate), new Date(
						myMagicalDate));
		newsDAO.add(news);
	}
	
	@Test(expected = DAOException.class)
	public void testExceptionOnTooLongShort() throws DAOException {
		News news = new News(1L, "1", "111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111",
				"fullText_test", new Timestamp(myMagicalDate), new Date(
						myMagicalDate));
		newsDAO.add(news);
	}*/
	
	/*@Test
	public void newsGenerator(){
		 String randomString = new String();
		 Random random = new Random();
		 Date date = new Date();
		 for(int i = 0; i < 10000; i++)
		 {
			 News news = new News();
		     char[] word = new char[random.nextInt(12)+3]; 
		     char[] word2 = new char[random.nextInt(12)+3]; 
		     char[] short1 = new char[random.nextInt(30)+3]; 
		     char[] short2 = new char[random.nextInt(30)+3]; 
		     char[] short3 = new char[random.nextInt(30)+3];		     
		     char[] long1 = new char[random.nextInt(100)+3]; 
		     char[] long2 = new char[random.nextInt(100)+3]; 
		     char[] long3 = new char[random.nextInt(100)+3];
		     
		     for(int j = 0; j < word.length; j++)
		     {
		         word[j] = (char)('a' + random.nextInt(26));
		     }
		     for(int j = 0; j < word2.length; j++)
		     {
		         word2[j] = (char)('a' + random.nextInt(26));
		     }
		     randomString = new String(word) + " " + new String(word2);
		     news.setTitle(randomString);
		     
		     for(int j = 0; j < short1.length; j++)
		     {
		         short1[j] = (char)('a' + random.nextInt(26));
		     }for(int j = 0; j < short2.length; j++)
		     {
		    	 short2[j] = (char)('a' + random.nextInt(26));
		     }for(int j = 0; j < short3.length; j++)
		     {
		    	 short3[j] = (char)('a' + random.nextInt(26));
		     }
		     randomString = new String(short1) + " " + new String(short2) + " " + new String(short3);
		     news.setShortText(randomString);
		     
		     for(int j = 0; j < long1.length; j++)
		     {
		    	 long1[j] = (char)('a' + random.nextInt(26));
		     }for(int j = 0; j < long2.length; j++)
		     {
		    	 long2[j] = (char)('a' + random.nextInt(26));
		     }for(int j = 0; j < long3.length; j++)
		     {
		    	 long3[j] = (char)('a' + random.nextInt(26));
		     }
		     randomString = new String(long1) + " " + new String(long2) + " " + new String(long3);
		     news.setFullText(randomString);
		     
		     long author_id_int = random.nextInt(9)+1;
		     Long author_id = (Long)author_id_int;
		     Author author = null;
		     try {
				author = authorDAO.getById(author_id);
			} catch (DAOException e) {
				e.printStackTrace();
			}
		     news.setAuthor(author);
		     
		     int tags_number = random.nextInt(15)+1;
		     Set<Tag> tags = new HashSet<Tag>();
		     for(int j=0; j<tags_number; j++){
		    	 long tag_id_int = random.nextInt(9)+1;
			     Long tag_id = (Long)tag_id_int;
			     try {
					tags.add(tagDAO.getById(tag_id));
				} catch (DAOException e) {
					e.printStackTrace();
				}
		     }
		     news.setTags(tags);
		     news.setCreationDate(new Date());
		     try {
				newsDAO.add(news);
			} catch (DAOException e) {
				e.printStackTrace();
			}
		 }
		 
		
	}*/
	
}
