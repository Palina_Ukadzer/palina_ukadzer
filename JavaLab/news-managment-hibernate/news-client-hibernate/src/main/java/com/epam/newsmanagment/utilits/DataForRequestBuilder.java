package com.epam.newsmanagment.utilits;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;

import com.epam.ukadzer.entity.SearchCriteria;
import com.epam.ukadzer.service.NewsService;
import com.epam.ukadzer.service.exception.ServiceException;

public class DataForRequestBuilder {

	public static Map<Long, Boolean> mapFromSet(Set<Long> longs) {
		Map<Long, Boolean> map = new HashMap<Long, Boolean>();
		for (Long id : longs) {
			map.put(id, true);
		}
		return map;
	}

	public static int pageNum(NewsService newsService,
			SearchCriteria searchCriteria) throws ServiceException {

		int newsNum;
		newsNum = newsService.countAllWithCriteria(searchCriteria);
		int pagesNum = (newsNum) / 3;
		if (newsNum % 3 != 0) {
			pagesNum++;
		}
		return pagesNum;

	}

	@SuppressWarnings("unchecked")
	public static Long setNextNewsId(Long newsId, NewsService newsService,
			HttpSession session) throws ServiceException {
		SearchCriteria searchCriteria = new SearchCriteria();
		Set<Long> searchedTags = new HashSet<>();
		if (session.getAttribute("authorId") != null
				&& !session.getAttribute("authorId").equals("0")) {
			searchCriteria.setAuthorId((Long) session.getAttribute("authorId"));
		}
		searchedTags = (Set<Long>) session
				.getAttribute("searchedTagsForCriteria");
		searchCriteria.setTagsIds(searchedTags);

		return newsService.getNextNewsId(newsId, searchCriteria);

	}

	@SuppressWarnings("unchecked")
	public static Long setPrevNewsId(Long newsId, NewsService newsService,
			HttpSession session) throws ServiceException {
		SearchCriteria searchCriteria = new SearchCriteria();
		Set<Long> searchedTags = new HashSet<>();
		if (session.getAttribute("authorId") != null
				&& !session.getAttribute("authorId").equals("0")) {
			searchCriteria.setAuthorId((Long) session.getAttribute("authorId"));
		}
		searchedTags = (Set<Long>) session
				.getAttribute("searchedTagsForCriteria");
		searchCriteria.setTagsIds(searchedTags);
		return newsService.getPreviousNewsId(newsId, searchCriteria);
	}
}
