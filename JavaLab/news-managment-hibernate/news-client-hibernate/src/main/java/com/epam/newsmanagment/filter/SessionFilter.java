package com.epam.newsmanagment.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate4.SessionHolder;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import com.epam.newsmanagment.utilits.ServiceManager;

public class SessionFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		ServiceManager.init();
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		Session sess = null;
		SessionFactory sessionFactory = null;

		if (ServiceManager.getApplicationContext() != null) {
			sessionFactory = (SessionFactory) ServiceManager
					.getApplicationContext().getBean("sessionFactory");
			sess = sessionFactory.openSession();
			sess.beginTransaction();

			TransactionSynchronizationManager.bindResource(sessionFactory,
					new SessionHolder(sess));
			TransactionSynchronizationManager.initSynchronization();

		}
		chain.doFilter(request, response);
		if (ServiceManager.getApplicationContext() != null) {

			try {

				sess.getTransaction().commit();

			} catch (Exception e) {
				throw e;
			} finally {
				TransactionSynchronizationManager
						.unbindResource(sessionFactory);
				TransactionSynchronizationManager.clearSynchronization();
				try {
					if (sess.isOpen()) {
						sess.close();
					}
				} catch (Exception e) {
					throw e;
				}

			}
		}

	}

	@Override
	public void destroy() {

	}

}
