package com.epam.newsmanagment.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.epam.newsmanagment.controller.command.Command;
import com.epam.newsmanagment.controller.exception.ControllerException;
import com.epam.newsmanagment.utilits.ResourceManager;
import com.epam.newsmanagment.utilits.ServiceManager;

@WebServlet(urlPatterns = { "/hi", "/Controller" })
public class Servlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private RequestHelper requestHelper = new RequestHelper();
	private ResourceManager resourceManager = ResourceManager.INSTANCE;

	/**
	 * Initialization
	 */
	@Override
	public void init() throws ServletException {
		super.init();
	}

	/**
	 * doGet method
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		processRequest(req, resp);
	}

	/**
	 * doPost method
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		processRequest(req, resp);
	}

	/**
	 * Processes doGet and doPost requests
	 */
	protected void processRequest(HttpServletRequest req,
			HttpServletResponse resp) throws ServletException, IOException {

		String commandName;
		try {
			HttpSession session = req.getSession(true);
			commandName = req.getParameter("command");
			if (commandName == null) {
				if (req.getRequestURI().equals(
						resourceManager.getString("welcomePage"))) {
					commandName = "SeeNewsList";
				}
			}
			Command command = requestHelper.getCommand(commandName, session);
			String where = null;
			try {
				where = command.execute(req, session);
			} catch (ControllerException e) {
				RequestDispatcher dispatcher = req
						.getRequestDispatcher(resourceManager
								.getString("unknownError"));
				dispatcher.forward(req, resp);
			}

			if (where != null) {
				if (command.isRedirected()) {
					resp.sendRedirect(where);
					return;
				}
				RequestDispatcher dispatcher = req.getRequestDispatcher(where);
				dispatcher.forward(req, resp);
			} else {
				RequestDispatcher dispatcher = req
						.getRequestDispatcher(resourceManager
								.getString("unknownError"));
				dispatcher.forward(req, resp);
			}
		} catch (Exception e) {
			RequestDispatcher dispatcher = req
					.getRequestDispatcher(resourceManager
							.getString("unknownError"));
			dispatcher.forward(req, resp);
		}
	}

}
