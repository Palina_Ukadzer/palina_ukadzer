package com.epam.newsmanagment.controller.exception;

public class ControllerException extends Exception{
	
	private static final long serialVersionUID = 1L;

	public ControllerException(){
		super();
	}
	
	public ControllerException(Exception e){
		super(e);
	}
	
	public ControllerException(String string){
		super(string);
	}
	
	public ControllerException(String s,Exception e){
		super(s, e);
	}

}
