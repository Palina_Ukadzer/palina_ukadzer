package com.epam.newsmanagment.controller.command;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;



import com.epam.newsmanagment.controller.exception.ControllerException;
import com.epam.newsmanagment.utilits.ResourceManager;
import com.epam.newsmanagment.utilits.ServiceManager;
import com.epam.newsmanagment.utilits.DataForRequestBuilder;
import com.epam.ukadzer.entity.ComplexNews;
import com.epam.ukadzer.entity.SearchCriteria;
import com.epam.ukadzer.service.AuthorService;
import com.epam.ukadzer.service.ComplexNewsService;
import com.epam.ukadzer.service.NewsService;
import com.epam.ukadzer.service.TagService;
import com.epam.ukadzer.service.exception.ServiceException;

public class SeeNewsListCommand implements Command {
	
	private ResourceManager resourceManager=ResourceManager.INSTANCE; 

	public String execute(HttpServletRequest request, HttpSession session)
			throws ControllerException {
		AuthorService authorService = (AuthorService) ServiceManager.getApplicationContext().getBean("authorService");
		TagService tagService = (TagService) ServiceManager.getApplicationContext().getBean("tagService");
		NewsService newsService = (NewsService) ServiceManager.getApplicationContext().getBean("newsService");
		ComplexNewsService complexNewsService = (ComplexNewsService) ServiceManager.getApplicationContext().getBean("complexNewsService");
		try {
			int page = 1;
			if (request.getParameter("page") != null) {
				page = Integer.parseInt(request.getParameter("page"));
			}
			request.setAttribute("authors", authorService.getAllAuthors());
			request.setAttribute("tags", tagService.getAllTags());
			List<ComplexNews> news = new ArrayList<>();
			SearchCriteria searchCriteria = new SearchCriteria();
			Set<Long> searchedTagsForCriteria = new HashSet<Long>();			
			getAuthorAndPutWhereNeeded(request, searchCriteria, session);			
			String[] params = request.getParameterValues("tagsIds");
			if (params != null) {
				for (int i = 0; i < params.length; i++) {
					searchedTagsForCriteria.add(Long.parseLong(params[i]));
				}
			}
			request.setAttribute("searchedTags", DataForRequestBuilder.mapFromSet(searchedTagsForCriteria));
			request.setAttribute("searchedTagsForCriteria", searchedTagsForCriteria);
			session.setAttribute("searchedTagsForCriteria", searchedTagsForCriteria);
			searchCriteria.setTagsIds(searchedTagsForCriteria);
			news = complexNewsService.getSomeWithCriteria( searchCriteria,
					page * 3 - 2, page * 3);
			request.setAttribute("news", news);
			request.setAttribute("pagesNum", DataForRequestBuilder.pageNum(newsService, searchCriteria));

		} catch (ServiceException e) {
			throw new ControllerException();
		}
		return resourceManager.getString("newsList");
	}
	
	
	
	private void getAuthorAndPutWhereNeeded(HttpServletRequest request, SearchCriteria searchCriteria, HttpSession session){
		if (request.getParameter("authorId") != null
				&& !request.getParameter("authorId").equals("0")) {
			searchCriteria.setAuthorId(Long.parseLong(request
					.getParameter("authorId")));
			request.setAttribute("authorId",
					Integer.parseInt(request.getParameter("authorId")));
			session.setAttribute("authorId",
					Long.parseLong(request.getParameter("authorId")));
		} else {
			if (session.getAttribute("authorId") != null && !session.getAttribute("authorId").equals("0")) {
				searchCriteria.setAuthorId((Long)session.getAttribute("authorId"));
				request.setAttribute("authorId",(Long)session.getAttribute("authorId"));
			}
		}
		if(request.getParameter("authorId") != null
				&& request.getParameter("authorId").equals("0")){
			searchCriteria.setAuthorId(null);
			request.removeAttribute("authorId");
			session.setAttribute("authorId", null);
		}
	}
	
	public boolean isRedirected(){
		return false;
	}

}
