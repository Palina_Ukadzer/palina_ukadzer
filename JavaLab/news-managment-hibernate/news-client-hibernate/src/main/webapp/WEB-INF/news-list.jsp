<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<c:set var="language"
	value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
	scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="message" />
<head>

<link href="<c:url value="/resources/myClasses.css" />" type="text/css"
	rel="stylesheet">

<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="<c:url value="/resources/jquery.sumoselect.min.js" />"></script>
<link href="<c:url value="/resources/sumoselect.css" />"
	rel="stylesheet" />

<script>
	$(document).ready(function() {

		$('.selectpicker').SumoSelect({
			placeholder : '<fmt:message key="selectHere" />',
			captionFormat : '<fmt:message key="selected" />',
			csvDispCount : 3
		});

		$('#date').html(Globalize.format(today, 'D', 'de'));

	});
</script>


<title>Mews List</title>


</head>
<body>


	<div class="panel panel-default">

		<div class="panel panel-heading">
			<h1>Mews portal</h1>
			<form action="/news-client-hibernate">
				<button type="submit" id="language" name="language" value="ru">Рус</button>
				<button type="submit" id="language" name="language" value="en">En</button>
			</form>
		</div>
		<div class="panel panel-body">


			<div class="col-sm-9">
				<form action="Controller">
					<div class="col-sm-6">
						<input type="hidden" name="command" value="SeeNewsList">

						<c:choose>
							<c:when test="${not empty authors}">
								<select name="authorId" class="selectpicker">
									<option value="0"><fmt:message key="anyAuthor" /></option>
									<c:choose>
										<c:when test="${ not empty authorId}">
											<c:forEach items="${authors}" var="author">
												<c:choose>
													<c:when test="${authorId == author.id}">
														<option value="${author.id}" selected><c:out
																value="${author.name}" /></option>
													</c:when>
													<c:otherwise>
														<option value="${author.id}"><c:out
																value="${author.name}" /></option>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</c:when>
										<c:otherwise>
											<c:forEach items="${authors}" var="author">
												<option value="${author.id}"><c:out
														value="${author.name}" /></option>
											</c:forEach>
										</c:otherwise>
									</c:choose>
								</select>
							</c:when>
							<c:otherwise>
								<select class="selectpicker">
									<option>No authors found</option>
								</select>
							</c:otherwise>
						</c:choose>

						<c:choose>
							<c:when test="${not empty tags}">
								<select id="tagsIds" name="tagsIds" class="selectpicker"
									multiple>
									<c:choose>
										<c:when test="${empty searchedTags}">
											<c:forEach items="${tags}" var="tag">
												<option value="${tag.id}"><c:out
														value="${tag.name}" /></option>
											</c:forEach>
										</c:when>
										<c:otherwise>
											<c:forEach items="${tags}" var="thisTag">
												<c:choose>
													<c:when test="${not empty searchedTags[thisTag.id]}">
														<option selected value="${thisTag.id}"><c:out
																value="${thisTag.name}" /></option>
													</c:when>
													<c:otherwise>
														<option value="${thisTag.id}"><c:out
																value="${thisTag.name}" /></option>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</c:otherwise>
									</c:choose>
								</select>

							</c:when>

							<c:otherwise>
								<select name="tagsIds" multiple>
									<option>No tags found</option>
								</select>
							</c:otherwise>
						</c:choose>


						<button type="submit" class="btn-info form-control">
							<fmt:message key="filter" />
						</button>

						<a href="/news-client/Controller?command=ResetSearchCriteria">
							<div class="buttonLike">
								<fmt:message key="reset" />
							</div>
						</a>
					</div>
				</form>



			</div>


			<c:choose>
				<c:when test="${not empty news}">

					<c:forEach items="${news}" var="news">
						<form action="Controller">
							<div class="col-sm-9">
								<div class="titleInfo">
									<b>${news.news.title}</b>
								</div>
								<div class="authorInfo">(${news.author.name})</div>
								<div class="dateInfo">
									<ins>
									<fmt:message key="datePattern" var="datePattern"/>
										<fmt:formatDate pattern="${datePattern}"
											value="${news.news.modificationDate}" />
									</ins>
								</div>

							</div>
							<div class="col-sm-9">

								<div class="shortTextInfo">${news.news.shortText}</div>


								<div class="tagsInfo">
									<c:forEach items="${news.tags}" var="tag">
									${tag.name}
								</c:forEach>
								</div>


								<div class="commentsInfo">
									<p>
										<fmt:message key="comments" />
										(${fn:length(news.comments)})
									</p>
								</div>
								<div class="viewButton">
									<button type="submit" class="btn btn-primary">
										<fmt:message key="view" />
									</button>
								</div>
							</div>

							<input type="hidden" name="command" value="SeeSingleNews">
							<input type="hidden" name="newsId" value="${news.news.id}">
						</form>
					</c:forEach>


				</c:when>
				<c:otherwise>
					<h1>No news found</h1>
				</c:otherwise>
			</c:choose>
		</div>
	</div>


	<form action="Controller">

		<input type="hidden" name="command" value="SeeNewsList">
		<c:if test="${not empty authorId}">
			<input type="hidden" name="authorId" value="${authorId}">
		</c:if>
		<c:if test="${not empty searchedTags}">
			<c:forEach items="${searchedTags}" var="searchedTag">
				<input type="hidden" name="tagsIds" value="${searchedTag.key}">
			</c:forEach>
		</c:if>
		<div class="col-sm-9" align="center">
			<ul class="pagination">
				<c:forEach begin="1" end="${pagesNum}" var="i">
					<a><input class="myCoolPageIndex" type="submit" name="page"
						value="${i}"></a>
				</c:forEach>
			</ul>
		</div>
	</form>

</body>
</html>