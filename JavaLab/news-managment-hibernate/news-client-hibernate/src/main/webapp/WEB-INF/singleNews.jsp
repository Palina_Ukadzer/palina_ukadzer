<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<c:set var="language"
	value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
	scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="message" />
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>News</title>
<script src="<c:url value="/resources/validation.js" />"></script>
<link href="<c:url value="/resources/myClasses.css" />" type="text/css"
	rel="stylesheet">
<script>	
badComment = '<fmt:message key="badComment"/>';
</script> 
</head>
<body>
	<div class="panel panel-default">

		<div class="panel panel-heading">
			<h1>Mews portal</h1>
			<form action="/news-client-hibernate">
				<input type="hidden" name="command" value="SeeSingleNews"> <input
					type="hidden" name="newsId" value="${news.news.id}">

				<button type="submit" id="language" name="language" value="ru">Рус</button>
				<button type="submit" id="language" name="language" value="en">En</button>
			</form>
		</div>
		<div class="dotted-border">
			<div class="col-sm-9">
				<form action="Controller">
					<input type="hidden" name="from" value="SingleNews"> <input
						type="hidden" name="command" value="GoBackToList">
					<div class="backButton">
						<button type="submit" class="btn btn-primary">
							<fmt:message key="back" />
						</button>
					</div>
				</form>
				<div class="col-sm-12">
					<div class="col-sm-8">
						<div class="col-sm-4">
							<div class="newsTitle">
								<b><c:out value="${news.news.title}" /></b>
							</div>
						</div>
						<div class="authorName">
							<c:out value="(${news.news.author.name})" />
						</div>
					</div>
					<div class="dateInfoSingle">
						<fmt:message key="datePattern" var="datePattern" />
						<fmt:formatDate pattern="${datePattern}"
							value="${news.news.modificationDate}" />
					</div>
				</div>
				<h4>
					<i><c:out value="${news.news.shortText}" /></i>
				</h4>
				<h4>
					<c:out value="${news.news.fullText}" />
				</h4>

				<c:choose>
					<c:when test="${not empty news.news.comments}">

						<c:forEach items="${news.news.comments}" var="comment">
							<div class="col-sm-9">
								<div class="commentDate">
									<ins>
									<fmt:message key="datePattern" var="datePattern"/>
										<fmt:formatDate pattern="${datePattern}"
											value="${comment.creationDate}" />
									</ins>
								</div>

								<div class="commentText">
									<c:out value="${comment.text}" />
								</div>
							</div>
						</c:forEach>
					</c:when>
					<c:otherwise>
						<fmt:message key="noCommentsYet" />
					</c:otherwise>
				</c:choose>

				<form action="Controller" method="post" onsubmit="return validate_form ();">
					<textarea rows="2" cols="60" id="text" name="commentText"></textarea>
					<input type="hidden" name="command" value="PostComment"> <input
						type="hidden" name="newsId" value="${news.news.id}">
					<button type="submit" class="btn btn-primary">
						<fmt:message key="postComment" />
					</button>
				</form>
				<div class="col-sm-9">


					<div class="prevNews">
						<c:if test="${previousNewsId != 0}">
							<form action="Controller">
								<input type="hidden" name="command" value="SeePreviousNews">
								<input type="hidden" name="previousNewsId"
									value="${previousNewsId}">
								<button type="submit" class="btn btn-info">
									<fmt:message key="previousNews" />
								</button>
							</form>
						</c:if>
					</div>

					<div class="nextNews">
						<c:if test="${nextNewsId != 0}">
							<form action="Controller">
								<input type="hidden" name="command" value="SeeNextNews">
								<input type="hidden" name="nextNewsId" value="${nextNewsId}">
								<button type="submit" class="btn btn-info">
									<fmt:message key="nextNews" />
								</button>
							</form>
						</c:if>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>